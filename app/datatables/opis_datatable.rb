  class OpisDatatable
  delegate :params, :h, :link_to, :number_to_currency, to: :@view

  def initialize(view, periodo, est)
    @view = view
    @periodo = periodo
    @est = est
  end

  def as_json(options = {})
    {
      "sEcho" => params[:sEcho].to_i,
      "iTotalRecords" => Orden.all(:es_nula => false).count,
      "iTotalDisplayRecords" => cantidad,
      "aaData" => data
    }
  end

private

  def ubicacion id
    orden = Orden.get id
    case orden.estado 
      when "Conducto Regular"
        ubicacion = orden.rutas(:activa => true).first.bandeja.nombre
      when "En consulta"
        if orden.consulta.blank?
          ubicacion = "#{orden.ubicacion.departamento.nombre}"
        else
          ubicacion = "#{orden.consulta.usuario.nombre} #{orden.consulta.usuario.apaterno}"
        end
      when "En proceso"
        ubicacion = "#{orden.ubicacion.departamento.nombre}"
      when "Digitada"
        ubicacion = "#{orden.departamento.establecimiento.nombre}"
      when "Rechazada"
        ubicacion = "Direccion - #{orden.departamento.establecimiento.nombre}"
      else
        ubicacion = "#{orden.departamento.nombre} - #{orden.departamento.establecimiento.nombre}"
      end
      ubicacion

  end

  def data
  
    #estado = ""
    

    opis.map do |p|  
      recepciones = ""
      if not p.recepcion.blank?
        p.recepcion.each do |o|
          recepciones += "<a target='_blank' href='/recepcionar/#{o.id}/ver'>#{o.id}</a> </br>"
        end
      end

      case p.estado
      when "Conducto Regular" 
       estado = "<span class='label label-primary'> #{p.estado} </span>"
      when "En consulta" 
       estado = "<span class='label label-info'>#{p.estado} </span>"
       when "Recepcionada" 
       estado = "<span class='label label-success'>#{p.estado} </span>"
       when "Comprada" 
       estado = "<span class='label label-success'>#{p.estado} </span>"
     when "Aprobada" 
       estado = "<span class='label label-success'>#{p.estado} </span>"
       when "Rechazada" 
       estado = "<span class='label label-danger'>#{p.estado} </span>"
       when "Terminada" 
        estado = "<span class='label label-warning'>#{p.estado} </span>"
       else 
          estado = "<span class='label label-default'>#{p.estado} </span>"
       end 
       ubi = ubicacion(p.id)
       puts "ID OPI #{p.id}".yellow
       puts "ID OPI #{p.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.nombre}".yellow
      [
        p.id,
        recepciones,
        p.titulo,
        "#{p.departamento.nombre} - #{p.departamento.establecimiento.nombre}",
        p.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.nombre,
        estado,
        p.created_at.strftime('%d/%m/%Y'),
        ubi,
        "<a href='/opi/#{p.id}/ver'><i class='icon-search'></i>Ver</a>"
      ]
    end
  end

  def opis
    @opis ||= fetch_opis
  end

  def fetch_opis
   
   if @est == 1
    opis = Orden.all(:periodo_id => @periodo, :es_nula => false, :order => [ params[:sSortDir_0] == 'desc' ? :"#{sort_column}".desc : :"#{sort_column}".asc ])
   else
    opis = Orden.all(:periodo_id => @periodo, :es_nula => false, :departamento => {:establecimiento_id => @est}, :order => [ params[:sSortDir_0] == 'desc' ? :"#{sort_column}".desc : :"#{sort_column}".asc ])
   end
    opis = opis.page(page).per(per_page)
    
    if params[:sSearch].present?
      buscar = params[:sSearch]
      opis = opis.all(:titulo.like =>  "%#{buscar}%") | opis.all(:ubicacion => {:departamento => {:nombre.like => "%#{buscar}%"}}) | opis.all(:departamento => {:establecimiento => {:nombre.like => "%#{buscar}%"}}) | opis.all(:estado.like => "%#{buscar}%") | opis.all(:id => buscar.to_i) | opis.all(:subcategoria_gasto => {:categoria_gasto => {:tipo_gasto => {:subvencion => {:nombre.like => "%#{buscar}%"}}}})
    end
    opis
  end

  def cantidad

  if @est == 1
    cantidad = Orden.all(:periodo_id => @periodo, :es_nula => false)
  else
    cantidad = Orden.all(:periodo_id => @periodo, :es_nula => false, :departamento => {:establecimiento_id => @est})
  end

    if params[:sSearch].present?
      buscar = params[:sSearch]
      cantidad = cantidad.all(:titulo.like =>  "%#{buscar}%") | cantidad.all(:ubicacion => {:departamento => {:nombre.like => "%#{buscar}%"}}) | cantidad.all(:departamento => {:establecimiento => {:nombre.like => "%#{buscar}%"}}) | cantidad.all(:estado.like => "%#{buscar}%") | cantidad.all(:id => buscar.to_i) | cantidad.all(:subcategoria_gasto => {:categoria_gasto => {:tipo_gasto => {:subvencion => {:nombre.like => "%#{buscar}%"}}}})
    end
    cantidad.size
  end

  def page
    params[:iDisplayStart].to_i/per_page + 1
  end

  def per_page
    params[:iDisplayLength].to_i > 0 ? params[:iDisplayLength].to_i : 10
  end

  def sort_column
    columns = %w[id titulo departamento_id id estado created_at id id]
    columns[params[:iSortCol_0].to_i]
  end

end
