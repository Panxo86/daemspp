require 'dm-rails/middleware/identity_map'
class ApplicationController < ActionController::Base
  use Rails::DataMapper::Middleware::IdentityMap
  protect_from_forgery

   before_filter :verificar_autentificacion, :periodo, :entrantes_count


  def obtener_subvencion_subcategoria id
    #puts "#{id}".green
    subcategoria = SubcategoriaGasto.get id

    subvencion = Subvencion.get subcategoria.categoria_gasto.tipo_gasto.subvencion_id
    #puts "#{subvencion.inspect}".green
    subvencion
  end



  def pagado_subvencion est, sub
    pagado = 0
    compradas = Adquisicion.all(
      :es_nula => false,
      :eliminada => false,
      :orden => {
        :es_nula => false,
        :periodo_id => session[:periodo].id,
        :departamento => {
          :establecimiento_id => est
        },
        :subcategoria_gasto => {
          :categoria_gasto => {
            :tipo_gasto => {
              :subvencion_id => sub
            }
          }
        }
      }
    )
    compradas.each do |c|
      pagado += c.monto.to_i
    end
    pagado
  end

  def saldo_subvencion est, sub
    #metodo igual en PresupuestoSuvencion 
    pagado = pagado_subvencion(est, sub)
    presupuesto = presupuesto_sub(est, sub)
    saldo = presupuesto.to_i - pagado
    saldo
  end

  def disponible_subvencion est, sub
    saldo = saldo_subvencion(est, sub)
    en_gestion = en_gestion_subvencion(est, sub)
    disponible = saldo.to_i - en_gestion.to_i
  end

  def presupuesto_sub est, sub
    presupuesto = PresupuestoSubvencion.last(
      :periodo_id => session[:periodo].id, 
      :establecimiento_id => est, 
      :subvencion_id => sub)
    presupuesto.monto
  end

  def en_gestion_subvencion est, sub
    en_gestion = 0
    en_proceso = Orden.all(
        :es_nula => false,
        :en_proceso => true,
        :periodo_id => session[:periodo].id,
        :departamento => {
          :establecimiento_id => est
        },
        :subcategoria_gasto => {
            :categoria_gasto => {
              :tipo_gasto => {
                :subvencion_id => sub
              }
            }
          }
      )
     # en_proceso = en_proceso.all(:estado => "Conducto Regular") | en_proceso.all(:estado => "Espera decreto") | en_proceso.all(:estado => "En proceso") | en_proceso.all(:estado => "En consulta") |en_proceso.all(:estado => "En digitacion") | en_proceso.all(:estado => "En Licitacion") | en_proceso.all(:estado => "Rechazada")

      en_proceso.each do |o|
        o.bien_servicios.each do |b|
          en_gestion += (b.total.to_i * 1.19)
        end
        
      end
      en_gestion

  end


  def verificar_autentificacion

    #puts "#{session[:periodo].inspect}".red

  	if not esta_logueado?
      if not se_recuerda?
    		redirect_to new_login_path
      else
        session[:usuario_id] = cookies[:usuario_id]
        @usuario_actual = Usuario.get session[:usuario_id]
        session[:nombre] = "#{@usuario_actual.nombre} #{@usuario_actual.apaterno}"
        @estado_sistema = EstadoSistema.get 1

      end
  	else      
  		@usuario_actual = Usuario.get session[:usuario_id]
      session[:nombre] = "#{@usuario_actual.nombre} #{@usuario_actual.apaterno}"
      @estado_sistema = EstadoSistema.get 1
  	end
  end

  def periodo
    if session[:periodo].blank?
      session[:periodo] = Periodo.first(:es_vigente => true)
    end
  end

  def entrantes_count
    if esta_logueado?


      if @usuario_actual.sistema.dotacion
          if @usuario_actual.sistema.dotacion_admin 
            @solicitudes_count = DocenteSolicitud.count(
              :estado => "Enviada", 
              :es_nulo => false
            ) | DocenteSolicitud.count(
                :establecimiento_id => @usuario_actual.departamento.establecimiento_id,
                :estado => "Rechazada", 
                :es_nulo => false
            ) | DocenteSolicitud.count(
                :usuario_id => @usuario_actual.departamento.establecimiento_id, 
                :estado => "Devuelta", 
                :es_nulo => false)
          else
            @solicitudes = DocenteSolicitud.count(
                :establecimiento_id => @usuario_actual.departamento.establecimiento_id,
                :estado => "Rechazada", 
                :es_nulo => false
            ) | DocenteSolicitud.count(
                :usuario_id => @usuario_actual.departamento.establecimiento_id, 
                :estado => "Devuelta", 
                :es_nulo => false
            )
          end
      end

      if @usuario_actual.departamento.es_direccion
        @firma_recepcion = Recepcion.all(
            :pagar => true, 
            :orden => {
              :periodo_id => session[:periodo].id, 
              :es_nula => false
            }, 
            :firmado => false,
            :orden => {
              :periodo_id => session[:periodo].id, 
              :es_nula => false, 
              :departamento => {
                :establecimiento_id => @usuario_actual.departamento.establecimiento_id
              }
            }
          ).count
      end
      @entrantes = Orden.count(:periodo_id => session[:periodo].id, :es_nula => false, :es_entrante => true, :ubicacion => {:departamento_id => @usuario_actual.departamento_id})
      @recepcion_c = Adquisicion.count(
      :eliminada => false,
      :orden => {
        :lista_recepcion => true, 
        :periodo_id => session[:periodo].id, 
        :es_nula => false, 
        :departamento => {
          :establecimiento_id => @usuario_actual.departamento.establecimiento_id}
        }
      )
      if @usuario_actual.departamento_id == 1
        @licitacion = Licitacion.count(:estado => "Publicada", :orden => {:periodo_id => session[:periodo].id})
        @decreto = Orden.count(:periodo_id => session[:periodo].id, :es_nula => false, :estado => "Espera decreto")
      end

      if @usuario_actual.departamento.es_direccion 
        @validar_count = Orden.count(
          :estado => "Digitada", 
          :es_nula => false,
          :periodo_id => session[:periodo].id,
          :en_proceso => true,
          :departamento => {
            :establecimiento_id => @usuario_actual.departamento.establecimiento_id
            })
      end

      if @usuario_actual.asesor_subvencions.count > 0 
        @asesor_count = Orden.count(
            :es_nula => false,
            :en_proceso => true, 
            :estado => "Conducto Regular",
            :periodo_id => session[:periodo].id,
            :rutas =>{
              :bandeja_id => 2,
              :activa => true
              },
            :subcategoria_gasto => {
              :categoria_gasto => {
                :tipo_gasto => {
                  :subvencion => {
                    :asesor_subvencions => {
                      :usuario_id => @usuario_actual.id,
                      :es_vigente => true
                    }
                  }
                }
              }
            }
            )
      end
      if @usuario_actual.encargado_subvencions.count > 0
          @encargado_count = Orden.count(
          :es_nula => false,
          :en_proceso => true, 
          :estado => "Conducto Regular",
          :periodo_id => session[:periodo].id,
          :rutas =>{
            :bandeja_id => 1,
            :activa => true
            },
          :subcategoria_gasto => {
            :categoria_gasto => {
              :tipo_gasto => {
                :subvencion => {
                  :encargado_subvencions => {
                    :usuario_id => @usuario_actual.id,
                    :es_vigente => true
                  }
                }
              }
            }
          }
          )

      end 

          direccion = Departamento.first(:establecimiento_id => @usuario_actual.departamento.establecimiento_id, :es_direccion => true)
          director = Usuario.all(:departamento_id => direccion.id).first

        if @usuario_actual.id == director.id
              #es director
          @mibandeja_count = Orden.count(
            :es_nula => false,
            :en_proceso => true,
            :periodo_id => session[:periodo].id,
            :consulta => {
              :usuario_id => @usuario_actual.id
            }
          ) | Orden.count(
            :es_nula => false,
            :en_proceso => true,
            :periodo_id => session[:periodo].id,
            :en_emisor => true,
            :usuario_id => @usuario_actual.id
          ) | Orden.count(
            :es_nula => false,
            :en_proceso => true,
            :periodo_id => session[:periodo].id,
            :en_direccion => true,
            :departamento => {
              :establecimiento_id => direccion.establecimiento_id
            }
          )
        else
          @mibandeja_count = Orden.count(
            :es_nula => false,
            :en_proceso => true,
            :periodo_id => session[:periodo].id,
            :consulta => {
              :usuario_id => @usuario_actual.id
            }
          ) | Orden.count(
            :es_nula => false,
            :en_proceso => true,
            :periodo_id => session[:periodo].id,
            :en_emisor => true,
            :usuario_id => @usuario_actual.id
          )

        end
    end 
end



  def esta_logueado?
  	(not session[:usuario_id].nil?)
  end

  def se_recuerda?
    (not cookies[:usuario_id].nil?)
  end

  def handle_unverified_request
    true
  end
end
