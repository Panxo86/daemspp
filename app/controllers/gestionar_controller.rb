#encoding: utf-8
class GestionarController < ApplicationController

	def opi

		@orden = Orden.get params[:id]

		@emisor = Usuario.get @orden.usuario_id

      	direccion = Departamento.first(:establecimiento_id => @orden.departamento.establecimiento_id, :es_direccion => true)

		@director = Usuario.all(:departamento_id => direccion.id).first

		@option = "<option>asd</option>"
		
	end

	def termina_decreto

		@orden = Orden.get params[:id]
		if params[:comentario].blank?
        	raise "Ingrese numero decreto en comentario"
        end
        Orden.transaction do 
		movimiento = Movimiento.new(
		          :descripcion => "Espera decreto terminado",
		          :estado => "Aprobada",
		          :comentario => params[:comentario],
		          :departamento_id => @usuario_actual.departamento_id,
		          :orden_id => @orden.id,
		          :usuario_id => @usuario_actual.id
		        )
		        movimiento.save
		        @orden.update(:estado => "Aprobada", :en_proceso => false, :es_editable => false)
		end
		flash[:notificacion] = "OPI Terminada y aprobada exitosamente"
		redirect_to espera_decreto_path
	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to :back
	end

	def validar
		@orden = Orden.get params[:id]

		if params[:comentario].blank?
        	comentario = "-"
        else
        	comentario = params[:comentario]
        end

		if params[:accion] == "aprobar"
			Orden.transaction do 
				mover_opi = Ruta.all(:orden_id => @orden.id).first
		      	mover_opi.update(:activa => true)
		        
		        movimiento = Movimiento.new(
		          :descripcion => "OPI Validada por director",
		          :estado => "Conducto Regular",
		          :comentario => comentario,
		          :departamento_id => @usuario_actual.departamento_id,
		          :orden_id => @orden.id,
		          :usuario_id => @usuario_actual.id
		        )
		        movimiento.save
		        @orden.update(:estado => "Conducto Regular", :en_proceso => true, :es_editable => false, :director => @usuario_actual.id)
        		etapa_actual = Ruta.all(:orden_id => @orden.id, :activa => true).first
			    EducaMailer.entrante(etapa_actual).deliver

		        firma = Firma.all(:orden_id => @orden.id).first
		        firma.update :director_est => true
	    	end
	    	
	        flash[:notificacion] = "OPI Aprobada con exito"
	        redirect_to validar_opi_path
	    elsif params[:accion] == "Rechazar"

	    	movimiento = Movimiento.new(
		          :descripcion => "OPI Rechazada por director",
		          :estado => "Rechazada",
		          :comentario => comentario,
		          :departamento_id => @usuario_actual.departamento_id,
		          :orden_id => @orden.id,
		          :usuario_id => @usuario_actual.id
		        )
		    movimiento.save
		    @orden.update(:estado => "Rechazada", :en_proceso => false, :director => @usuario_actual.id)
		    EducaMailer.rechazada(@orden).deliver
	    	flash[:notificacion] = "OPI Rechazada"
	        redirect_to validar_opi_path
	    else
	    	raise "Debe seleccionar una accion"
	    end

    rescue Exception => e
    	flash[:error] = "#{e.message}"
        redirect_to :back

	end

	def sigue_conducto
		
	    @orden = Orden.get params[:id]

		if params[:comentario].blank?
        	comentario = "-"
        else
        	comentario = params[:comentario]
        end
        etapa_anterior = Ruta.all(:orden_id => @orden.id, :aprobada => true, :activa => false).last
        etapa_actual = Ruta.all(:orden_id => @orden.id, :activa => true).first
		siguiente_etapa = Ruta.all(:orden_id => @orden.id, :aprobada => false, :activa => false).first
		Orden.transaction do 

			if params[:accion] == "Aprobar"
			
		      	if not siguiente_etapa.blank?
		      		@orden.update(:estado => "Conducto Regular")
			        etapa_actual.update(:activa => false, :aprobada => true, :fecha_aprobada => Time.now, :usuario_id => @usuario_actual.id)
			       	siguiente_etapa.update(:activa => true)
			       	
			        movimiento = Movimiento.new(
			          :descripcion => "OPI Validada por #{etapa_actual.bandeja.nombre}",
			          :estado => "Conducto Regular",
			          :comentario => comentario,
			          :departamento_id => @usuario_actual.departamento_id,
			          :orden_id => @orden.id,
			          :usuario_id => @usuario_actual.id
			        )
			        movimiento.save

			        #firma
			        firma = Firma.all(:orden_id => @orden.id).first
			        firma.update :encargado_subvencion => true

			        if etapa_actual.bandeja.nombre == "Asesor Subvención"

				        if not params[:cuenta].blank?
				        	if params[:cuenta][:cuenta].blank?
				        		raise "El numero de cuenta no debe ir en blanco"
				        	end
				        	if params[:cuenta][:subtitulo].blank?
				        		raise "El subtitulo de la cuenta no debe ir en blanco"
				        	end
				        	if params[:cuenta][:item].blank?
				        		raise "El item de cuenta no debe ir en blanco"
				        	end
				        	if params[:cuenta][:asignacion].blank?
				        		raise "La asignacion de la cuenta no debe ir en blanco"
				        	end
				            #puts "#{params[:cuenta].inspect}".yellow
				              if @orden.cuenta_gasto_orden.blank?
				                cuenta_gasto = CuentaGastoOrden.new params[:cuenta]
				                cuenta_gasto.orden_id = @orden.id
				                cuenta_gasto.cuenta_gasto_id = 1
				               # puts "#{cuenta_gasto.inspect}".yellow
				                cuenta_gasto.save
				                mov_cuenta = "Cuenta gasto: #{cuenta_gasto.cuenta}"
				              else

				                cuenta_gasto = CuentaGastoOrden.all(:orden_id => @orden.id).last
				                if params[:cuenta][:cuenta] != cuenta_gasto.cuenta_gasto_id
				                  cuenta_gasto.update params[:cuenta]
				                  mov_cuenta = "Cuenta gasto Modificada a: #{cuenta_gasto.cuenta}"  
				                end
				              end
				              movimiento_c = Movimiento.new(
				                  :descripcion => mov_cuenta,
				                  :estado => "Conducto Regular",
				                  :comentario => "Cuenta asignada ",
				                  :departamento_id => @usuario_actual.departamento_id,
				                  :orden_id => @orden.id,
				                  :usuario_id => @usuario_actual.id
				                )
				                movimiento_c.save
				        else
				        	raise "Debe ingresar datos de la cuenta de gasto"

				        end
				        EducaMailer.entrante(siguiente_etapa).deliver
				        
			    	end

			    else
			    	@orden.update(:estado => "Aprobada", :en_proceso => false)

			    	movimiento = Movimiento.new(
			          :descripcion => "OPI Aprobada y Finalizada por #{etapa_actual.bandeja.nombre}",
			          :estado => "Aprobada",
			          :comentario => comentario,
			          :departamento_id => @usuario_actual.departamento_id,
			          :orden_id => @orden.id,
			          :usuario_id => @usuario_actual.id
			        )
			        movimiento.save
			        #Enviar correo de opi Aprobada y esperar recepcion
			    end
				#EducaMailer.entrante(etapa_anterior).deliver

			    flash[:notificacion] = "OPI Aprobada con exito"
	        	
		elsif params[:accion] == "Suministro"
	    
			@orden.update(:estado => "Enviada Suministro", :en_proceso => false, :es_suministro => true)
			    	movimiento = Movimiento.new(
			          :descripcion => "OPI Enviada a contrato de suminstro",
			          :estado => "Aprobada",
			          :comentario => comentario,
			          :departamento_id => @usuario_actual.departamento_id,
			          :orden_id => @orden.id,
			          :usuario_id => @usuario_actual.id
			        )
			        movimiento.save
		elsif params[:accion] == "Rechazar"
			@orden.update(:estado => "Rechazada", :en_proceso => false)
			etapa_actual.update(:rechazada => true)
			if params[:comentario].blank?
				raise "Debe ingresar el motivo del rechazo en el comentario"
			end
			movimiento = Movimiento.new(
		          :descripcion => "OPI Rechazada por #{etapa_actual.bandeja.nombre}",
		          :estado => "Rechazada",
		          :comentario => comentario,
		          :departamento_id => @usuario_actual.departamento_id,
		          :orden_id => @orden.id,
		          :usuario_id => @usuario_actual.id
		        )
		     movimiento.save
		     EducaMailer.rechazada(@orden).deliver
		     flash[:notificacion] = "OPI Rechazada y devuelta"

		elsif params[:accion] == "Consulta"

			if params[:consulta].blank?
				raise "No selecciono funcionario consulta"
			end
			if params[:comentario].blank?
				raise "Debe escribir la consulta en el comentario"
			end
			@orden.update(:estado => "En consulta", :en_consulta => true)
			consulta = Consulta.create(:orden_id => @orden.id, :usuario_id => params[:consulta])
			#editar si es del establecimiento
			if consulta.usuario.departamento.establecimiento_id == @orden.departamento.establecimiento_id
				@orden.update :es_editable => true
			end
			movimiento = Movimiento.new(
		          :descripcion => "OPI enviada a Consulta por #{etapa_actual.bandeja.nombre}",
		          :estado => "En consulta",
		          :comentario => comentario,
		          :departamento_id => @usuario_actual.departamento_id,
		          :orden_id => @orden.id,
		          :usuario_id => @usuario_actual.id
		        )
		     movimiento.save
		     EducaMailer.consulta(@orden).deliver
			flash[:notificacion] = "OPI enviada a consulta exitosamente"

		elsif params[:accion] == "Devolver"
			if params[:comentario].blank?
				raise "Debe escribir motivo devolucion en el comentario"
			end
			if etapa_anterior.blank?
				raise "No existe una etapa anterior donde devolver la OPI"
			end
			etapa_actual.update(:activa => false)
			etapa_anterior.update(:activa => true)
			EducaMailer.entrante(etapa_anterior).deliver

			movimiento = Movimiento.new(
		          :descripcion => "OPI Devuelta a #{etapa_actual.bandeja.nombre} por #{etapa_actual.bandeja.nombre}",
		          :estado => "Conducto Regular",
		          :comentario => comentario,
		          :departamento_id => @usuario_actual.departamento_id,
		          :orden_id => @orden.id,
		          :usuario_id => @usuario_actual.id
		        )
		     movimiento.save
			flash[:notificacion] = "OPI devuelta exitosamente"

		elsif params[:accion] == "Licitar"
         # puts "Licitar".blue
          movimiento = Movimiento.new(
            :descripcion => "Enviada a licitacion",
            :estado => "En Licitacion",
            :comentario => comentario,
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          @orden.update(:estado => "En Licitacion")
          params[:licitacion].each do |l|
            #puts "#{l}".yellow
            lic = Licitacion.new l
            lic.orden_id = @orden.id
            lic.estado = "Publicada"
            lic.save
          end

        elsif params[:accion] == "Comprar"
         # puts "Comprar".yellow
          
            movimiento = Movimiento.new(
              :descripcion => "Aprobada compra directa",
              :estado => "Comprada",
              :comentario => comentario,
              :departamento_id => @usuario_actual.departamento_id,
              :orden_id => @orden.id,
              :usuario_id => @usuario_actual.id
            )
            movimiento.save

            ruta_actual = Ruta.all(:orden_id => @orden.id, :activa => true).first
            if not ruta_actual.blank?
	            ruta_actual.update(:aprobada => true, :fecha_aprobada => Time.now, :activa => false) 
				siguiente_etapa = Ruta.all(:orden_id => @orden.id, :aprobada => false, :activa => false).first
				if siguiente_etapa.blank?
					#no hay mas etapas, se acabo
					@orden.update(:estado => "Comprada", :lista_recepcion => true, :en_proceso => false)
				else
					@orden.update(:estado => "Conducto Regular", :lista_recepcion => true)
					siguiente_etapa.update(:activa => true)
				end
			else
				@orden.update(:estado => "Comprada", :lista_recepcion => true, :en_proceso => false)
			end
           busca_oc = Adquisicion.all(:orden_id => params[:id], :orden_compra => params[:compra][:orden_compra], :eliminada => false, :es_nula => false)
		      if not busca_oc.blank?
		      	puts "#{busca_oc.inspect}".blue
		        raise "La OC ya ha sido ingresada"
		      end
            ad = Adquisicion.new params[:compra]
            ad.orden_id = @orden.id
            ad.save   
            subvencion_orden = @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id
            est_orden = @orden.departamento.establecimiento_id
            saldo = saldo_subvencion(est_orden, subvencion_orden)
            historial = HistorialPresupuesto.create(
              :descripcion => "Adquisicion OPI #{@orden.id}, OC: #{ad.orden_compra}",
              :entrada => "0",
              :salida => "#{ad.monto}",
              :total => saldo,
              :subvencion_id => subvencion_orden,
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => est_orden,
              :usuario_id => @usuario_actual.id
            )          

            documento = OrdenCompraDoc.new params[:doc]
            documento.adquisicion_id = ad.id
            documento.save

        elsif params[:accion] == "oc"
            movimiento = Movimiento.new(
              :descripcion => "Agregada Orden de Compra",
              :estado => @orden.estado,
              :comentario => comentario,
              :departamento_id => @usuario_actual.departamento_id,
              :orden_id => @orden.id,
              :usuario_id => @usuario_actual.id
            )
            movimiento.save
            busca_oc = Adquisicion.all(:orden_id => params[:id], :orden_compra => params[:compra][:orden_compra], :eliminada => false, :es_nula => false)
		      if not busca_oc.blank?
		      	puts "#{busca_oc.inspect}".blue
		        raise "La OC ya ha sido ingresada"
		      end
            ad = Adquisicion.new params[:compra]
            ad.orden_id = @orden.id
            ad.save             
            documento = OrdenCompraDoc.new params[:doc]
            documento.adquisicion_id = ad.id
            documento.save
            subvencion_orden = @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id
            est_orden = @orden.departamento.establecimiento_id
            saldo = saldo_subvencion(est_orden, subvencion_orden)
            historial = HistorialPresupuesto.create(
              :descripcion => "Adquisicion OPI #{@orden.id}, OC: #{ad.orden_compra}",
              :entrada => "0",
              :salida => "#{ad.monto}",
              :total => saldo,
              :subvencion_id => subvencion_orden,
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => est_orden,
              :usuario_id => @usuario_actual.id
            )  
			@orden.update(:lista_recepcion => true)

            flash[:notificacion] = "Se ha Guardado la orden Exitosamente."
            #redirect_to :back

      elsif params[:accion] == "Espera decreto"
      	@orden.update(:estado => "Espera decreto")
      	movimiento = Movimiento.new(
              :descripcion => "Esperando Decreto",
              :estado => "Espera decreto",
              :comentario => comentario,
              :departamento_id => @usuario_actual.departamento_id,
              :orden_id => @orden.id,
              :usuario_id => @usuario_actual.id
            )
            movimiento.save
		end #fin elsif acciones
			
	    	
	end
	if params[:accion] == "oc"
		redirect_to gestionar_opi_path params[:id]
	else
		if not etapa_actual.blank?
			if etapa_actual.bandeja.nombre == "Encargado Subvención"
				redirect_to entrantes_encargado_subvencion_path
			elsif etapa_actual.bandeja.nombre == "Asesor Subvención"
				redirect_to entrantes_asesor_subvencion_path
			else
				redirect_to bandeja_entrantes_path etapa_actual.bandeja_id
			end
		else
			redirect_to mi_bandeja_path
		end
	end
	

    rescue Exception => e
    	flash[:error] = "#{e.message}"
    	puts "#{e.backtrace}".red

        redirect_to gestionar_opi_path params[:id]

	end


	def responde_consulta
		@orden = Orden.get params[:id]

		if params[:comentario].blank?
        	raise "Debe ingresar una respuesta"
        end
        Orden.transaction do
	        movimiento = Movimiento.new(
			          :descripcion => "Consulta respondida",
			          :estado => "Conducto Regular",
			          :comentario => params[:comentario],
			          :departamento_id => @usuario_actual.departamento_id,
			          :orden_id => @orden.id,
			          :usuario_id => @usuario_actual.id
			        )
			movimiento.save
			consulta = Consulta.all(:orden_id => @orden.id)
			consulta.destroy
			@orden.update(:estado => "Conducto Regular", :en_consulta => false)
			etapa_actual = Ruta.all(:orden_id => @orden.id, :activa => true).first
			EducaMailer.entrante(etapa_actual).deliver


		end

		flash[:notificacion] = "Consulta responida con exito"
		redirect_to mi_bandeja_path

	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to :back
	end

	def rechazada

		@orden = Orden.get params[:id]

		if params[:comentario].blank?
        	comentario = "-"
        else
        	comentario = params[:comentario]
        end

        if params[:btn] == "reenviar"
        	if params[:comentario].blank?
        		raise "Debe ingresar un comentario"
        	end
        	@orden.update(:es_editable => false, :en_direccion => false, :en_emisor => false, :estado => "Conducto Regular")
        	movimiento = Movimiento.new(
			          :descripcion => "OPI Reenviada",
			          :estado => "Conducto Regular",
			          :comentario => params[:comentario],
			          :departamento_id => @usuario_actual.departamento_id,
			          :orden_id => @orden.id,
			          :usuario_id => @usuario_actual.id
			        )
			movimiento.save
        	flash[:notificacion] = "Se ha reenviado la OPI con exito"

        elsif params[:btn] == "terminar"
        	@orden.update(:en_proceso => false, :es_editable => false, :en_direccion => false, :en_emisor => false, :estado => "Terminada")
        	movimiento = Movimiento.new(
			          :descripcion => "OPI Terminada",
			          :estado => "Terminada",
			          :comentario => params[:comentario],
			          :departamento_id => @usuario_actual.departamento_id,
			          :orden_id => @orden.id,
			          :usuario_id => @usuario_actual.id
			        )
			movimiento.save

        	flash[:notificacion] = "Se ha terminado la OPI con exito"

        elsif params[:btn] == "emisor"
        	if params[:comentario].blank?
        		raise "Debe ingresar un comentario"
        	end

        	@orden.update(:en_direccion => false, :en_emisor => true)
        	movimiento = Movimiento.new(
			          :descripcion => "OPI Enviada a emisor",
			          :estado => "Rechazada",
			          :comentario => params[:comentario],
			          :departamento_id => @usuario_actual.departamento_id,
			          :orden_id => @orden.id,
			          :usuario_id => @usuario_actual.id
			        )
			movimiento.save

        	flash[:notificacion] = "Se ha enviado a emisor con exito"
        end
		
		
		redirect_to mi_bandeja_path

	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to :back
	end


end
