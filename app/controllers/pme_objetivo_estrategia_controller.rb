class PmeObjetivoEstrategiaController < ApplicationController

	def get_pme_objetivo
    
		  id = params[:id]
	      dimencion = PmeDimension.get id
	      datos = PmeObjetivo.all(:pme_dimension_id => id, :periodo_id => session[:periodo].id, :establecimiento_id => @usuario_actual.departamento.establecimiento_id, :order => :nombre)
	      if datos.blank?
	        mensaje = "No existen objetivos creados para Dimension <b>'#{dimencion.nombre}'</b> para el perido <b>#{session[:periodo].anio}</b>"
	      else
	        mensaje = ""
	      end
	      array = "<option value=''>Seleccione Objetivo</option>"
	      datos.each do |p|
		      array += "<option value='#{p.id}'>#{p.nombre}</option>"
		    end
		    render :json => { 
		      :opciones => array,
		      :mensaje => mensaje
		    }
	end

	def get_pme_estrategia
    
		id = params[:id]
		datos = PmeEstrategia.all(:pme_objetivo_id => id, :es_vigente => true, :order => :nombre)
		if datos.blank?
	        mensaje = "No existen Estrategias creados para el Objetivo <b>'#{pme_objetivo.nombre}'</b> para el perido <b>#{session[:periodo].anio}</b>"
	      else
	        mensaje = ""
	      end
	      array = "<option value=''>Seleccione Estrategia</option>"
	      datos.each do |p|
		      array += "<option value='#{p.id}'>#{p.nombre}</option>"
		    end
		    render :json => { 
		      :opciones => array,
		      :mensaje => mensaje
		    }
	end

	def objetivos
		est = Establecimiento.all(:hex_url => params[:est]).first
		@objetivos = PmeObjetivo.all(:establecimiento_id => est.id, :periodo_id => session[:periodo].id, :es_vigente => true)
	end

	def crear_objetivo

		if not params[:objetivo_sub].blank?
			objetivo = PmeObjetivo.new params[:objetivo]
			objetivo.establecimiento_id = params[:est]
			objetivo.pme_objetivo_sub_dimensions = params[:objetivo_sub]
			objetivo.periodo_id = session[:periodo].id
			puts "#{objetivo.inspect}".yellow
			objetivo.save
		else
			raise "Debe ingresar Dimension y su Subdimension"
		end

		flash[:notificacion] = "Se ha guardado con exito"
		redirect_to :back
	rescue Exception => e
		flash[:error] = "Ha ocurrido un error, #{e.message}"
		redirect_to :back
	end

	def jeditable_nombre_pme_objetivo
		nuevo = params[:newvalue]
		id = params[:elementid].to_i
		obj = PmeObjetivo.get id
		obj.update :nombre => nuevo
		render :json => nuevo
	end

	def destroy_pme_objetivo
		@obj = PmeObjetivo.get(params[:id])
		@obj.update(es_vigente: false)
		

		flash[:notificacion] = "Se ha borrado existosamente"
		redirect_to :back

	rescue
		flash[:error] = "Error"
		redirect_to :back
	end 

	def estrategia_objetivo
		@est = Establecimiento.all(:hex_url => params[:est]).first
		@objetivo = PmeObjetivo.get params[:id]
		@estrategias = PmeEstrategia.all(:pme_objetivo_id => @objetivo.id, :es_vigente => true)
	end

	def crear_estrategia

	estrategia = PmeEstrategia.new params[:estrategia]
	estrategia.pme_objetivo_id = params[:id]
	estrategia.save
		
	flash[:notificacion] = "Se ha guardado con exito"
		redirect_to :back
	rescue Exception => e
		flash[:error] = "Ha ocurrido un error, #{e.message}"
		redirect_to :back
	end

	def jeditable_nombre_pme_estrategia
		nuevo = params[:newvalue]
		id = params[:elementid].to_i
		obj = PmeEstrategia.get id
		obj.update :nombre => nuevo
		render :json => nuevo
	end

	def destroy_pme_estrategia
		@est = PmeEstrategia.get(params[:id])
		@est.update(es_vigente: false)
		

		flash[:notificacion] = "Se ha borrado existosamente"
		redirect_to :back

	rescue
		flash[:error] = "Error"
		redirect_to :back
	end 

end
