class EstablecimientosController < ApplicationController

  def get_dpto

      id = params[:id].to_i
      dptos = Departamento.all(:establecimiento_id => id)
      array = "<option value=''>Seleccione departamento</option>"
      dptos.each do |p|
      array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => array
 
    
  end

  def get_dpto_usuario

      id = params[:id].to_i
      usuario = Usuario.get id
      dptos = Departamento.all(:establecimiento_id => usuario.departamento.establecimiento_id)
      array = "<option value=''>Seleccione departamento</option>"
      dptos.each do |p|
        if usuario.departamento_id == p.id
          array += "<option selected value='#{p.id}'>#{p.nombre}</option>"
        else
          array += "<option value='#{p.id}'>#{p.nombre}</option>"
        end
      end
    render :json => array
 
    
  end
  # GET /establecimientos
  # GET /establecimientos.json
  def index
    @establecimientos = Establecimiento.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @establecimientos }
    end
  end

  # GET /establecimientos/1
  # GET /establecimientos/1.json
  def show
    @establecimiento = Establecimiento.get(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @establecimiento }
    end
  end

  # GET /establecimientos/new
  # GET /establecimientos/new.json
  def new
    @establecimiento = Establecimiento.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @establecimiento }
    end
  end

  # GET /establecimientos/1/edit
  def edit
    @establecimiento = Establecimiento.get(params[:id])
  end

  # POST /establecimientos
  # POST /establecimientos.json
  def create
    @establecimiento = Establecimiento.new(params[:est])
    @establecimiento.es_dificil = false
    hex = ""
    busca_hex = 1
    while busca_hex > 0
      hex = SecureRandom.hex(3)
      busca_hex = Establecimiento.all(hex_url: hex).count
    end
    @establecimiento.hex_url = hex
    @establecimiento.save
    
    Subvencion.todos.each do |s|
      presupuesto = PresupuestoSubvencion.create(
        :monto => "0",
        :establecimiento_id => @establecimiento.id,
        :subvencion_id => s.id,
        :periodo_id => session[:periodo].id
      )
    end
    
    

    
    flash[:notificacion] = "Guardado con exito"
    redirect_to establecimientos_path

    rescue Exception => e
      puts "#{e.message}".red
    puts "#{e.backtrace}".red
      flash[:error] = "Error: #{e.message}"
      redirect_to nuevo_establecimiento_path
  end

  def departamento

    @d = Departamento.get params[:id]
    
  end

  def actualizar_departamento
    @d = Departamento.get params[:id]
    @d.update params[:dpto]
    flash[:notificacion] = "Guardado con exito"
    redirect_to ver_departamento_path params[:id]

    rescue Exception => e
      puts "#{e.message}".red
      puts "#{e.backtrace}".red
      flash[:error] = "Error: #{e.message}"
      redirect_to ver_departamento_path params[:id]
  end

  def guardar_departamento
    @dpto = Departamento.new(params[:dpto])
    @dpto.establecimiento_id = params[:id]
    

    @dpto.save
    flash[:notificacion] = "Guardado con exito"
    redirect_to ver_establecimiento_path params[:id]

    rescue Exception => e
      puts "#{e.message}".red
      puts "#{e.backtrace}".red
      flash[:error] = "Error: #{e.message}"
      redirect_to ver_establecimiento_path params[:id]
  end

  def eliminar_departamento
    @dpto = Departamento.get(params[:id])
    if @dpto.destroy
     flash[:notificacion] = "Eliminado con exito"
        redirect_to ver_establecimiento_path params[:establecimiento_id]
    else
      flash[:error] = "No se puede eliminar el departamento"
        redirect_to ver_establecimiento_path params[:establecimiento_id]
    end 

    rescue Exception => e
      puts "#{e.message}".red
      puts "#{e.backtrace}".red
      flash[:error] = "Error: #{e.message}"
      redirect_to ver_departamento_path params[:id]
  end

  def eliminar_establecimiento
    @establecimiento = Establecimiento.get(params[:id])
    if @establecimiento.destroy
      flash[:notificacion] = "Eliminado con exito"
       redirect_to establecimientos_path
    else
      flash[:error] = "No se puede eliminar el establecimiento"
       redirect_to establecimientos_path
    end
   
    rescue Exception => e
      puts "#{e.message}".red
      puts "#{e.backtrace}".red
      flash[:error] = "Error: #{e.message}"
      redirect_to ver_establecimiento_path params[:id]
  end

  # PUT /establecimientos/1
  # PUT /establecimientos/1.json
  def update
    @establecimiento = Establecimiento.get(params[:id])

    @establecimiento.update(params[:est])

    flash[:notificacion] = "Guardado con exito"
    redirect_to ver_establecimiento_path params[:id]

    rescue Exception => e
      puts "#{e.message}".red
    puts "#{e.backtrace}".red
      flash[:error] = "Error: #{e.message}"
      redirect_to ver_establecimiento_path params[:id]
  
  end

  # DELETE /establecimientos/1
  # DELETE /establecimientos/1.json
  def destroy
    @establecimiento = Establecimiento.get(params[:id])
    @establecimiento.destroy

    respond_to do |format|
      format.html { redirect_to establecimientos_url }
      format.json { head :no_content }
    end
  end
end
