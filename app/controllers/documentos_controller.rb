class DocumentosController < ApplicationController

	def crear_documento

		documento = Documento.new params[:doc]
		documento.orden_id = 1
		documento.departamento_id = @usuario_actual.departamento_id
		documento.usuario_id = @usuario_actual.id
		documento.save
		
		respond_to do |format|
       		format.html { 		
       			flash[:notificacion] = "exito."
				redirect_to alta_path 
			}
       		format.json {
       			render :json => {
       				"estado" => "Exito",
       				"url" => documento.archivo.url,
       				"id" => documento.id,
       				"name" => documento.name
       			}
       		}
     	end
		rescue Exception => e
			puts "ERROR::::::: #{e.message} #{e.message.inspect}".red
			respond_to do |format|
		      format.html { 
		      	flash[:error] = "Verifique los datos ingresados"
		      	redirect_to alta_path
		      }
		      format.json {
       			render :json => {
       				"estado" => "Error #{e.message}"
       			}
       		}
		    end
		
	end

	def adjuntar_documento

		documento = Documento.new params[:doc]
		documento.departamento_id = @usuario_actual.departamento_id
		documento.usuario_id = @usuario_actual.id
		documento.save
		
		respond_to do |format|
       		format.html { 		
       			flash[:notificacion] = "exito."
				redirect_to alta_path 
			}
       		format.json {
       			render :json => {
       				"estado" => "Exito",
       				"url" => documento.archivo.url,
       				"id" => documento.id,
       				"name" => documento.name,
       				"dpto" => @usuario_actual.departamento.nombre,
       				"por" => "#{@usuario_actual.nombre} #{@usuario_actual.apaterno}",
       				"fecha" => documento.created_at.strftime("%d/%m/%Y %H:%M")
       			}
       		}
     	end
		rescue Exception => e
			puts "ERROR::::::: #{e.message} #{e.message.inspect}".red
			respond_to do |format|
		      format.html { 
		      	flash[:error] = "Verifique los datos ingresados"
		      	redirect_to alta_path
		      }
		      format.json {
       			render :json => {
       				"estado" => "Error #{e.message}"
       			}
       		}
		    end
		
	end

	def borrar_documento
		documento = Documento.get params[:id]
		documento.destroy
		flash[:notificacion] = "exito."
		redirect_to :back
	rescue Exception => e
		puts "ERROR::::::: #{e.message} #{e.message.inspect}".red
		redirect_to :back
	end

end