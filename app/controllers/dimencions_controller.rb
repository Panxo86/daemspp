class DimencionsController < ApplicationController
  
  def index
    @var = "Dimencion" 

    @dimencion = Dimencion.all(:es_vigente => true)

    
  end

  
  

 
  def new
   
    @area = Area.all

    @a = Area.get params[:id]

    
  end

  
  def edit

    @d = Dimencion.get(params[:id])

    @area = Area.all

    @a = Area.get params[:id]

   

    @dimencion = Dimencion.all(:area_id => params[:id])


  end

  def create

    @a = Area.get params[:id]

    @dimencion = Dimencion.new(
      :nombre => params[:tx_nombre],
      :area_id => params[:area] 
      )
    @dimencion.save
    flash[:notificacion] = "Se ha guardado con exito"
      redirect_to ver_area_dim_path @a.id
  
    rescue 
            flash[:error] = "Error"
            redirect_to nueva_dimencion_path 
 
  end

 
  def update
    
    dimencion = Dimencion.get params[:id]
        dimencion.update( 
            :nombre => params[:tx_nombre],
            :es_vigente => params[:vigente]
        )

        flash[:notificacion] = "Se ha guardado con exito"
      
        redirect_to ver_area_dim_path dimencion.area_id



    rescue
        flash[:error] = "Error"
        redirect_to :back
 
    end


    def borrar
      
        @d = Dimencion.get params[:id]
        @area = Area.all 

    end




    
    
  

 
  def destroy
    dimencion = Dimencion.get params[:id]
    dimencion.destroy


    flash[:notificacion] = "Se ha borrado existosamente"
        redirect_to dimencions_path 

        rescue
        flash[:error] = "Error"
        redirect_to borrar_dimencion_path params[:id]

  end

  def show

    @d = Dimencion.get params[:id]

    @dimencion = Dimencion.all
    

    @objetivo = Objetivo.all(:dimencion_id => params[:id])

   

   
  end


  def sl_dimencion
    
      id = params[:id]
      datos = Dimencion.all(:area_id => id, :es_vigente => true, :order => :nombre)
      array = "<option value=''>Dimensiones</option>"
      datos.each do |p|
      array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => { :opciones => array }
  end
  
end
