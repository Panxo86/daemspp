class ObjetivoController < ApplicationController



  


  def new
  
    @objetivos = Objetivo.all(:periodo_id => session[:periodo].id, :establecimiento_id => @usuario_actual.departamento.establecimiento_id, :order => :dimencion_id)

   
  end



  def create

    puts "#{session[:periodo].id}"
    puts "#{@usuario_actual.departamento.establecimiento_id}"
    @objetivo = Objetivo.new(
      :nombre => params[:tx_nombre],
      :dimencion_id => params[:dimencion].to_i
      )
    @objetivo.periodo_id = session[:periodo].id
    @objetivo.establecimiento_id = @usuario_actual.departamento.establecimiento_id
    @objetivo.save

    flash[:notificacion] = "Se ha guardado con exito"
      redirect_to nuevo_objetivo_path
  
    rescue Exception => e
          puts "ERROR:::: #{e.message.inspect}"
            flash[:error] = "Error"
            redirect_to nuevo_objetivo_path 

   
    
  end

  def get_objetivo
    id = params[:id]
      dimencion = PmeDimension.get id
      datos = PmeObjetivo.all(:pme_dimencion_id => id, :periodo_id => session[:periodo].id, :establecimiento_id => @usuario_actual.departamento.establecimiento_id, :order => :nombre)
      if datos.blank?
        mensaje = "No existen objetivos creados para Dimension <b>'#{dimencion.nombre}'</b> para el perido <b>#{session[:periodo].anio}</b>"
      else
        mensaje = ""
      end
      array = "<option value=''>Seleccione Objetivo</option>"
      datos.each do |p|
      array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => { 
      :opciones => array,
      :mensaje => mensaje
    }
  end


  def sl_objetivo
    
      id = params[:id]
      dimencion = Dimencion.get id
      datos = Objetivo.all(:dimencion_id => id, :periodo_id => session[:periodo].id, :establecimiento_id => @usuario_actual.departamento.establecimiento_id, :order => :nombre)
      if datos.blank?
        mensaje = "No existen objetivos creados para Dimension <b>'#{dimencion.nombre}'</b> para el perido <b>#{session[:periodo].anio}</b>"
      else
        mensaje = ""
      end
      array = "<option value=''>Seleccione Objetivo</option>"
      datos.each do |p|
      array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => { 
      :opciones => array,
      :mensaje => mensaje
    }
  end


  def jeditable_nombre

    nuevo = params[:newvalue]
    id = params[:elementid].to_i
    obj = Objetivo.get id
    obj.update :nombre => nuevo
    render :json => nuevo

  end

  def borrar
    
    obj = Objetivo.get params[:id]

    if obj.accions.size == 0
      obj.destroy
      flash[:notificacion] = "Se ha borrado el objetivo exitosamente"
    else
      flash[:error] = "No se ha borrado el objetivo #{obj.nombre}, ya que exiten #{obj.accions.size} acciones asociadas"
    end

    redirect_to nuevo_objetivo_path

    
      
  end
    
end
