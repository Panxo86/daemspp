class TipoGastosController < ApplicationController

 
  def sl_tipo_gasto
    
      id = params[:id]
      datos = TipoGasto.all(:es_vigente => true, :subvencion_id => id, :es_vigente => true, :order => :nombre, :es_persona => false)
      array = "<option value=''>Seleccione Tipo Gasto</option>"
      datos.each do |p|
      array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => { :opciones => array }
  end

  def get_tipo_gasto
    id = params[:id]
    tipo = TipoGasto.get id

    render :json => tipo    
  end

    def guarda_tipo_gasto
    gas = TipoGasto.new params[:tipo]
    gas.es_vigente = true
    gas.save
    flash[:notificacion] = "Se ha creado exitosamente"

    redirect_to tipo_gasto_subvencion_path params[:tipo][:subvencion_id].to_i
  rescue
    flash[:error] = "revise los campos ingresados"
    redirect_to tipo_gasto_subvencion_path params[:tipo][:subvencion_id].to_i
  end

  def jeditable_nombre_tipo_gasto
    nuevo = params[:newvalue]
    id = params[:elementid].to_i
    tipo = TipoGasto.get id
    tipo.update :nombre => nuevo
    render :json => nuevo
  end

  def borrar_tipo_gasto
    tipo = TipoGasto.get params[:id]
    tipo.update :es_vigente => false
    flash[:notificacion] = "Se ha eliminado exitosamente"
    redirect_to :back
  rescue
    flash[:error] = "Ha ocurrido un error, intentelo nuevamente"
    redirect_to :back
  end
  
end
