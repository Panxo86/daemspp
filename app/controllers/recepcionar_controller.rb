#encoding: utf-8
include ActionView::Helpers::NumberHelper
class RecepcionarController < ApplicationController


  def seleccionar
    if params[:oc].blank?
      raise "Debe ingresar numero de OC de mercado publico"
    else
      adq = Adquisicion.all(:eliminada => false, :orden_compra => params[:oc]).last
      if adq.blank?
        raise "OC Ingresada no existe"
      end
    end
    redirect_to nueva_recepcion_path adq.id
    rescue Exception => e
      flash[:error] = "Error #{e.message}"
      redirect_to recepcionar_path 
  end

  def adq
  
     @r = Recepcion.get params[:id]
  end

  def act_adq
    Recepcion.transaction do 
      @r = Recepcion.get params[:id]
      @r.update params[:recepcion]
      @r.update(:adquisiciones => true)
      items = RecepcionItem.all(:recepcion_id => @r.id)
      items.destroy
      params[:articulo].each do |a|
        nuevos = RecepcionItem.create a
      end
      proveedor = Proveedor.get params[:recepcion][:proveedor_id]
      if not proveedor.blank?
        puts "#{proveedor.inspect}"
        puts "#{params[:nombre_proveedor]}"
        proveedor.update(:nombre => params[:nombre_proveedor])
      end
      
    end
    flash[:recepcion] = "/recepcionar/#{@r.id}/ver"
    flash[:notificacion] = "Enviada exitosamente"
    redirect_to visar_adq_path
    rescue Exception => e
    flash[:error] = "Error #{e.message}"
    puts "#{e.backtrace}".red
    redirect_to adq_recepcion_path params[:id]
  end

  def visar_adq
    @recepciones = Recepcion.all(:pagar => true, :orden => {:periodo_id => session[:periodo].id, :es_nula => false}, :firmado => true, :adquisiciones => false)
    
  end

  def index

    @adquisiciones = Adquisicion.all(
      :eliminada => false,
      :orden => {
        :lista_recepcion => true, 
        :periodo_id => session[:periodo].id, 
        :es_nula => false, 
        :departamento => {
          :establecimiento_id => @usuario_actual.departamento.establecimiento_id}
        }
      )
   # @ordenes = Orden.all()

  end

  def para_firma
    @recepciones = Recepcion.all(
      :pagar => true, 
      :orden => {
        :periodo_id => session[:periodo].id, 
        :es_nula => false
      }, 
      :firmado => false,
      :orden => {
        :periodo_id => session[:periodo].id, 
        :es_nula => false, 
        :departamento => {
          :establecimiento_id => @usuario_actual.departamento.establecimiento_id
        }
      }
    )
  end

  def firmar
    @r = Recepcion.get params[:id]
  end

  def insertar_firma
    Recepcion.transaction do 
      @r = Recepcion.get params[:id]
      @r.update params[:recepcion]
      @r.update(:firmado => true, :firmado_por => @usuario_actual.id)
      items = RecepcionItem.all(:recepcion_id => @r.id)
      items.destroy
      params[:articulo].each do |a|
        nuevos = RecepcionItem.create a
      end
      
    end
    flash[:notificacion] = "Enviada exitosamente"
    redirect_to para_firmar_recepcion_path
    rescue Exception => e
    flash[:error] = "Error #{e.message}"
    redirect_to para_firmar_recepcion_path
  end

  def nueva
    @id = params[:id].to_i
    if @id > 0
      @adq = Adquisicion.get @id
    end
    
  end


  def crear


    exito = "0"
    msj = "error"
    id_recepcion = 0
    Recepcion.transaction do     
      orden = Orden.get params[:orden].to_i
      if !orden.blank?
        #puts "#{params[:recepcion].inspect}".blue
       # puts "#{params[:articulo].inspect}".blue
        rut_proveedor = params[:rut_proveedor].gsub(".","")
        proveedor = Proveedor.all(:rut => rut_proveedor).first
        if proveedor.blank?
          nuevo_proveedor = Proveedor.create(:rut => rut_proveedor, :nombre => params[:nombre_proveedor]) 
          id_proveedor = nuevo_proveedor.id
        else
          proveedor.update nombre: params[:nombre_proveedor]
          id_proveedor = proveedor.id
        end
        recepcion = Recepcion.new params[:recepcion]
        recepcion.recepcion_items = params[:articulo]
        recepcion.usuario_id = @usuario_actual.id
        recepcion.orden_id = orden.id
        recepcion.proveedor_id = id_proveedor
        if @usuario_actual.departamento.es_direccion 
          recepcion.firmado = true
          recepcion.firmado_por = @usuario_actual.id
        end 
        if @usuario_actual.departamento.establecimiento_id == 1
          recepcion.firmado = true
          recepcion.firmado_por = 138
        end
        recepcion.save
        id_recepcion = recepcion.id
        movimiento = Movimiento.new(
              :descripcion => "Recepcion conforme ID: #{recepcion.id}",
              :estado => "Recepcionada",
              :comentario => "Ok",
              :departamento_id => @usuario_actual.departamento_id,
              :orden_id => orden.id,
              :usuario_id => @usuario_actual.id
          )
          movimiento.save
        msj = "Guardada exitosamente"
        exito = "Exito"
      else
        msj = "Error, no existe la OPI #{params[:orden].to_i}"
      end
    end

    respond_to do |format|
      format.html {       
        flash[:notificacion] = "Se ha Guardado la orden Exitosamente."
        redirect_to nueva_orden_path 
      }
      format.json {
        render :json => {
            "estado" => exito,
            "msj" => msj,
            "recepcion" => id_recepcion
        }
      }
    end
  rescue Exception => e
    puts "ERROR:::: #{e.message.inspect}"
    puts "ERROR:::: #{e.backtrace}"
    respond_to do |format|
      format.html { 
        flash[:error] = "Verifique los datos ingresados"
        redirect_to nueva_orden_path
      }
      format.json {
        render :json => {
            "message" => "Ha ocurrido un error #{e.message}",
            "estado" => exito,
            "msj" => msj
        }
      }
    end
  end

  def listado
    @recepciones = Recepcion.all(:orden => {:periodo_id => session[:periodo].id}, :es_nula => false, :usuario => {:departamento_id => @usuario_actual.departamento_id})
  end

  def ver

    id = params[:id]
    @recepcion = Recepcion.get id
    @solicitado = 0
    @articulos = []
    @articulos << [
        "Cant",
        "Descripcion",
        "Desc Comprador",
        "Desc Proveedor",
        "Valor",
        "Total"
      ] 
    @recepcion.recepcion_items.each do |a, i|
      @articulos << [
        "#{miles(a.cantidad)}",
        "#{a.descripcion}",
        "#{a.descripcion_comprador}",
        "#{a.descripcion_proveedor}",
        "$ #{miles(a.neto)}",
        "$ #{miles(a.total)}",
      ] 
    end
    
    @nombre_director = ""
    @est = ""
    if not @recepcion.firmado_por.blank?
      @est = @recepcion.orden.departamento.establecimiento_id

      @director = Usuario.get @recepcion.firmado_por.to_i
      if not @director.blank?
        @nombre_director = "#{@director.nombre} #{@director.apaterno} #{@director.amaterno}"
      end
    end
    
    @subtotal = "$ #{miles(@recepcion.subtotal.to_i)}"
    @descuento = "$ #{miles(@recepcion.descuento.to_i)}"
    @neto = "$ #{miles(@recepcion.neto.to_i)}"
    @iva = "$ #{miles(@recepcion.iva.to_i)}"
    @esp = "$ #{miles(@recepcion.imp_especifico.to_i)}"
    @total = "$ #{miles(@recepcion.total.to_i)}"

  end

    def miles num
        if !num.blank?
            number_to_currency(num, :precision => (num.round == num) ? 0 : 2, :delimiter => ".", :unit => "")
        end
    end

  def eliminar
      recepcion = Recepcion.get params[:id]
      recepcion.update :es_nula => true
      flash[:notificacion] = "Se ha eliminado la recepcion ID: #{params[:id]} Exitosamente"
      redirect_to :back
    rescue
      flash[:error] = "Ha ocurrido un error eliminado la recepcion"
      redirect_to :back
  end

  def quitar
      orden = Orden.get params[:id]
      orden.update :lista_recepcion => false
      flash[:notificacion] = "Se ha quitado de la lista la recepcion ID: #{params[:id]} Exitosamente"
      redirect_to :back
  rescue
    flash[:error] = "Ha ocurrido un error quitando la recepcion"
    redirect_to :back
  end
end
