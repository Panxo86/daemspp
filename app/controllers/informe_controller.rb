#encoding: utf-8
include ActionView::Helpers::NumberHelper
require "csv"
class InformeController < ApplicationController

	def gastos_categoria
		
	end

	def informe_gastos_categoria

	    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :lista_recepcion => true)
	    puts "#{@ordenes.count}".green
	    @est = "TODOS"
	    if not params[:est].blank?
	    	@est = Establecimiento.get params[:est].to_i
	    	@est = @est.nombre
	    	@ordenes = @ordenes.all(:departamento => {:establecimiento_id => params[:est].to_i})
	    end
	    if not params[:cat].blank?
	    	@ordenes = @ordenes.all(:subcategoria_gasto => {:categoria_gasto_id => params[:cat].to_i})
	    end
	    puts "#{@ordenes.count}".green
	    @informe = []
	    @informe << [
	        "Fecha",
	        "Categoria de Gasto",
	        "Subcategoria de Gasto",
	        "Cantidad",
	        "Neto",
	        "Total",
	        "OPI",
	        "Orden Compra",
	        "Establecimiento"
	      ] 
	    @ordenes.recepcion.recepcion_items.each do |a, i|
	      @informe << [
	        "#{fecha(a.recepcion.created_at)}",
	        "#{a.recepcion.orden.subcategoria_gasto.categoria_gasto.nombre}",
	        "#{a.recepcion.orden.subcategoria_gasto.nombre}",
	        "#{a.cantidad}",
	        "$ #{miles(a.neto)}",
	        "$ #{miles(a.total)}",
	        "#{a.recepcion.orden.id}",
	        "#{a.recepcion.orden_compra}",
	        "#{a.recepcion.orden.departamento.establecimiento.nombre}",
	      ] 
	    end
	    respond_to do |format|
		    format.html
		    format.pdf
		    format.xlsx
		  end
	end

	def informe_gastos_subvencion

	    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :lista_recepcion => true)
	    puts "#{@ordenes.count}".green
	    @est = "TODOS"
	    if not params[:est].blank?
	    	@est = Establecimiento.get params[:est].to_i
	    	@est = @est.nombre
	    	@ordenes = @ordenes.all(:departamento => {:establecimiento_id => params[:est].to_i})
	    end
	    if not params[:sub].blank?
	    	@ordenes = @ordenes.all(:subcategoria_gasto => {:categoria_gasto_id => {:tipo_gasto => {:subvencion_id => params[:sub].to_i}}})
	    end
	    puts "#{@ordenes.count}".green
	    @informe = []

	    @informe << [
	        "Fecha",
	        "Subvencion",
	        "Subcategoria de Gasto",
	        "Cantidad",
	        "Neto",
	        "Total",
	        "OPI",
	        "Orden Compra",
	        "Establecimiento"
	      ] 
	    @ordenes.recepcion.recepcion_items.each do |a, i|
	      @informe << [
	        "#{fecha(a.recepcion.created_at)}",
	        "#{a.recepcion.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.nombre}",
	        "#{a.recepcion.orden.subcategoria_gasto.nombre}",
	        "#{a.cantidad}",
	        "$ #{miles(a.neto)}",
	        "$ #{miles(a.total)}",
	        "#{a.recepcion.orden.id}",
	        "#{a.recepcion.orden_compra}",
	        "#{a.recepcion.orden.departamento.establecimiento.nombre}",
	      ] 
	    end
	    respond_to do |format|
		    format.html
		    format.pdf
		    format.csv { send_data @products.to_csv }
		    format.xlsx # { send_data @products.to_csv(col_sep: "\t") }
		  end
	end

	def informe_compras

	    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :lista_recepcion => true)
	    puts "#{@ordenes.count}".green
	    @est = "TODOS"
	    if not params[:est].blank?
	    	@est = Establecimiento.get params[:est].to_i
	    	@est = @est.nombre
	    	@ordenes = @ordenes.all(:departamento => {:establecimiento_id => params[:est].to_i})
	    end
	    puts "#{@ordenes.count}".green
	    @informe = []
	    @informe << [
	        "Fecha Recep.",
	        "Subvencion",
	        "Subcategoria de Gasto",
	        "Articulo",
	        "Cant.",
	        "Monto",
	        "OPI",
	        "Orden Compra",
	        "Establecimiento",
	        "Proveedor"
	      ] 
	    @ordenes.recepcion.recepcion_items.each do |a, i|
	      @informe << [
	        "#{fecha(a.recepcion.created_at)}",
	        "#{a.recepcion.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.nombre}",
	        "#{a.recepcion.orden.subcategoria_gasto.nombre}",
	        "#{a.descripcion}",
	        "#{a.cantidad}",
	        "$#{miles(a.total)}",
	        "#{a.recepcion.orden.id}",
	        "#{a.recepcion.orden_compra}",
	        "#{a.recepcion.orden.departamento.establecimiento.nombre}",
	        "#{a.recepcion.proveedor.rut}",
	      ] 
	    end
	    respond_to do |format|
		    format.html
		    format.pdf
		    format.csv #{ send_data @products.to_csv }
		    format.xlsx # { send_data @products.to_csv(col_sep: "\t") }
		  end
	end

	def informe_supereduc

	    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :lista_recepcion => true)
	    puts "#{@ordenes.count}".green
	    @est = "TODOS"
	    if not params[:est].blank?
	    	@est = Establecimiento.get params[:est].to_i
	    	@est = @est.nombre
	    	@ordenes = @ordenes.all(:departamento => {:establecimiento_id => params[:est].to_i})
	    end

	    @informe = CSV.generate do |csv|
		    csv << [
		        "RBD",
		        "Subvencion",
		        "Codigo Cuenta",
		        "Tipo de documento",
		        "N Documento",
		        "Fecha documento",
		        "Fecha Pago",
		        "Descripcion Gasto",
		        "Rut Proveedor",
		        "Nombre Proveedor",
		        "Monto Gasto",
		        "Monto documento",
		        "Documento Original",
		      ] 
		    @ordenes.recepcion(:pagar => false).each do |a, i|
		    	desc_gasto = ""
		    a.recepcion_items.each do |x|
		    	if not x.descripcion_comprador.blank?
		    		desc_gasto += "#{x.descripcion_comprador.gsub(/\,/mi, '')} "
		    		puts "#{a.recepcion_items.first.descripcion_comprador.gsub(/\,/mi, '').gsub(/\;/mi, '')}".yellow
		    	end
		    end
		    
		    orden_compra = Adquisicion.all(:orden_compra => a.orden_compra).last
		      csv << [
		        "#{a.orden.departamento.establecimiento.rbd}",
		        "#{a.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.nombre}",
		        "#{a.orden.subcategoria_gasto.codigo}",
		        "#{a.tipo_documento}",
		        "#{a.num_factura}",
		        "#{a.fecha_factura}",
		        "#{a.fecha_pago}",
		        "#{desc_gasto}",
		        "#{a.proveedor.rut}",
		        "#{a.proveedor.nombre}",
		        "#{a.total}",
		        "#{orden_compra.monto}",
		        ""
		      ] 
		  	end
		  	
		  	
		end
		respond_to do |format|
		    format.csv { send_data @informe}
		  end

	    
	end

	def gastos_categoria_xls
		
	end


	def gastos_subvencion
		
	end

	def ver_gastos_subvencion
		
	end

	def ingresos_subvencion
		
	end

	def ver_ingresos_subvencion
		
	end

	def compras
		
	end

	def ver_compras
		
	end

	def supereduc
		
	end

	def generar_supereduc
		
	end

	private
	def dinero num
		if !num.blank?
	    	number_to_currency(num, :precision => (num.round == num) ? 0 : 2, :delimiter => ".")
	    end
    end

    def miles num
        if !num.blank?
            number_to_currency(num, :precision => (num.round == num) ? 0 : 2, :delimiter => ".", :unit => "")
        end
    end

    def fecha_hora fecha
        fecha.strftime("%d/%m/%Y a las %H:%M hrs.")
    rescue
        nil
    end

    def fecha fecha
        fecha.strftime("%d/%m/%Y")
    rescue
        nil
    end
end
