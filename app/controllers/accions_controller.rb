class AccionsController < ApplicationController
 

  def create

    accion = Accion.new params[:accion]
    accion.desde = params[:fechas].split(" - ").first
    accion.hasta = params[:fechas].split(" - ").last
    accion.periodo_id = session[:periodo].id
    accion.save
  
    respond_to do |format|
        format.html {       
            flash[:notificacion] = "Se ha Guardado la accion Exitosamente."
            redirect_to acciones_pme_path 
        }
        format.json {
            render :json => {
                "estado" => "Exito"
            }
        }
    end
    rescue Exception => e
        puts "ERROR:::: #{e.message.inspect}"
        respond_to do |format|
          format.html { 
            flash[:error] = "Verifique los datos ingresados"
            redirect_to nueva_pme_path
          }
          format.json {
            render :json => {
                "estado" => "Ha ocurrido un error #{e.message}"
            }
        }
      end
    
  end

  def borrar_accion
    accion = Accion.get params[:id]
    accion.update :es_vigente => false
    flash[:notificacion] = "Eliminada Exitosamente"
    redirect_to :back
  rescue
    flash[:error] = "Ha ocurrido un error, intentelo nuevamente"
    redirect_to :back
  end
  

  def sl_accion
    
      id = params[:id]
      datos = Accion.all(:objetivo_id => id, :es_vigente => true, :order => :nombre)
      array = "<option value=''>Seleccione Accion</option>"
      datos.each do |p|
      array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => { :opciones => array }
  end

  def presupuesto
    id = params[:id]
    accion = Accion.get id
    ordenes = Orden.all(:orden_tiene_accion => {:accion_id => id}, :periodo_id => session[:periodo].id)
    gastado = 0
    ordenes.each do |o|
      o.adquisicions.each do |a|
        gastado += a.monto
      end
    end
    presupuesto = accion.presupuesto.to_i
    restante = presupuesto - gastado
    render :json => {
      :total => presupuesto,
      :gastado => gastado,
      :restante => restante
    }

  end



 
end
