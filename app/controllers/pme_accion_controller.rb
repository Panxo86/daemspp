class PmeAccionController < ApplicationController
  def index
    if params[:col].blank?
  	  @est = Establecimiento.all(:hex_url => params[:est]).first
    else
      @est = Establecimiento.all(:hex_url => params[:col]).first 
    end

    if not params[:dimension].blank?
      @pme_objetivos = PmeObjetivo.all(
        :pme_dimension_id => params[:dimension], 
        :establecimiento_id => @est.id,
        :periodo_id => session[:periodo].id,
        :es_vigente => true)
      
    end
  end

  def ver
    @est = Establecimiento.all(:hex_url => params[:est]).first
    @accion = PmeAccion.get params[:id]
    @dimension = @accion.pme_estrategia.pme_objetivo.pme_dimension_id
    @objetivo = @accion.pme_estrategia.pme_objetivo.id
    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :orden_tiene_pme_accion => {:pme_accion_id => @accion.id} )
  end

  def get_pme_accion
  	est = Establecimiento.get params[:est]
  	if not params[:area].blank?
  		area = PmeArea.get params[:area]
  	end
  	if not params[:dimension].blank?
  		dimension = PmeDimension.get params[:dimension]
  	end
  	if not params[:subdimension].blank?
  		subdimension = PmeSubDimension.get params[:subdimension]
  	end	
  end

  def sl_pme_accion
    acciones = PmeAccion.all(:es_vigente => true, :pme_estrategia => {
      :pme_objetivo_id => params[:id]
      })
     if acciones.blank?
        mensaje = "No existen acciones creadas para Objetivo"
      else
        mensaje = ""
      end
     array = "<option value=''>Seleccione Accion</option>"
      acciones.each do |p|
      array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => { 
      :opciones => array,
      :mensaje => mensaje
    }
  end

  def nueva
  	@est = Establecimiento.all(:hex_url => params[:est]).first

  end

  def crear

  	PmeAccion.transaction do

  		if not params[:accion][:pme_estrategia_id].blank?
=begin
  			if not params[:sl_objetivo].blank?
			  	if params[:sl_objetivo] == "0"
			  		if not params[:txt_objetivo].blank?
				  		objetivo = PmeObjetivoEstrategico.create(
				  			:nombre => params[:txt_objetivo],
				  			:pme_sub_dimension_id => params[:sl_subdimension]
				  		)
				  	else
				  		raise "El nuevo objetivo no puede estar vacio"
				  	end
			  	else
			  		objetivo = PmeObjetivoEstrategico.get params[:sl_objetivo]
			  	end

			  	if params[:sl_estrategia] == "0"
			  		if not params[:txt_estrategia].blank?
				  		estrategia = PmeEstrategia.create(
				  			:nombre => params[:txt_estrategia],
				  			:pme_objetivo_estrategico_id => objetivo.id
				  		)
			  		else
				  		raise "La nueva estrategia no puede estar vacia"
				  	end
			  	else
			  		estrategia = PmeEstrategia.get params[:sl_estrategia]
			  	end
=end
			  	@accion = PmeAccion.new params[:accion]
			  	#@accion.pme_estrategia_id = estrategia.id
          @accion.desde = params[:fechas].split(" - ").first
          @accion.hasta = params[:fechas].split(" - ").last
			  	@accion.save
		  	#else
				#raise "Debe ingresar el objetivo"
			#end
		else
			raise "Debe ingresar la estrategia"
		end

  	end

  	respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Guardado la orden Accion."
                redirect_to pme_nueva_accion_path 
            }
            format.json {
                render :json => {
                    "estado" => "Exito",
                    :id => @accion.id
                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            puts "ERROR:::: #{e.backtrace}".red
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to pme_nueva_accion_path
              }
              format.json {
                render :json => {
                    "estado" => "Error: #{e.message}"
                }
            }
        end
  end
end
