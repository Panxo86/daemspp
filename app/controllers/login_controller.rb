# encoding: utf-8
class LoginController < ApplicationController
    # Las 3 acciones de este controlador, serán renderizadas 
    # dentro del layout login.html.erb 
    layout "login"

    #nos saltamos la validacion de usuario
    skip_before_filter :verificar_autentificacion, :entrantes_count

    def new 

    end 

    def create
      # Aquí llevamos a cabo el proceso de autentificación 
      user = params[:usuario]
      password = params[:password]
      usuario = Usuario.first(:email => user, :password => password, :es_nulo => false)
      
      if not usuario.nil?
          if not params[:recordar].nil?
            cookies.permanent[:usuario_id] = usuario.id
          end
          session[:usuario_id] = usuario.id
          session[:periodo] = Periodo.first(:es_vigente => true)
          redirect_to redirigir_path
      else
        flash[:error] = "La combinacion Email / Password no es correcta"
        redirect_to new_login_path
      end

    end 

    def logout 
        # Aquí llevamos a cabo el proceso de cerrar la sessión 
        cookies.delete :usuario_id
        session[:usuario_id] = nil
        redirect_to new_login_path
    end 
   end 
