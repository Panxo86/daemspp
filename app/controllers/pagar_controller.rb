class PagarController < ApplicationController

	def index
		@recepciones = Recepcion.all(:pagar => true, :orden => {:periodo_id => session[:periodo].id, :es_nula => false}, :firmado => true, :adquisiciones => true)
	end

	def todas_recepciones
		@recepciones = Recepcion.all(:orden => {:periodo_id => session[:periodo].id, :es_nula => false}, :pagar => false, :firmado => true)
		
	end

	def quitar_lista_pagar
		@r = Recepcion.get params[:id]
		@r.update :pagar => false
		flash[:notificacion] = "Quitada exitosamente"
		redirect_to pagar_path
	rescue
		flash[:notificacion] = "Error"
		redirect_to pagar_path
	end

	def ver
		@r = Recepcion.get params[:id]
	end

	def guarda_cheque
		Recepcion.transaction do 
			@r = Recepcion.get params[:id]
			@r.update params[:recepcion]
			@r.update :pagar => false
			items = RecepcionItem.all(:recepcion_id => @r.id)
			items.destroy
			params[:articulo].each do |a|
				nuevos = RecepcionItem.create a
			end
			
		end
		flash[:notificacion] = "Guardada exitosamente"
		redirect_to pagar_path
	rescue Exception => e
		flash[:error] = "Error #{e.message}"
		redirect_to pagar_path
	end
end
