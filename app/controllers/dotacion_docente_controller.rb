class DotacionDocenteController < ApplicationController


	def ver_solicitud

		@solicitud = DocenteSolicitud.get params[:id]
		
	end

	def actualizar_solicitud
		solicitud = DocenteSolicitud.get params[:id]
	   	solicitud.update params[:solicitud]
		solicitud.update :desde => params[:fechas].split(" - ").first
	    solicitud.update :hasta => params[:fechas].split(" - ").last

	    if not solicitud.docente_contratado.blank?
    		con = DocenteContratado.get solicitud.docente_contratado.id
    		con.destroy
    	end

	    if params[:accion] == "Aprobada"	
	    	solicitud.update :estado => "Aprobada"
	    	 if not params[:id_contrato].blank?
	    	 	contratado = DocenteContratado.new
	    	 	contratado.docente_id = params[:id_contrato]
	        	contratado.docente_solicitud_id = solicitud.id
	        	contratado.desde = params[:fecha_contrato].split(" - ").first
	    		contratado.hasta = params[:fecha_contrato].split(" - ").last
	    		contratado.horas = params[:horas_contrato]
	        	contratado.save
	        	descripcion = "Solicitud Aprobada se contrata a: #{contratado.docente.nombre} #{contratado.docente.apaterno}, #{contratado.horas} Horas"
	    	 else
	    	 	raise "Debe ingresar funcionario a contratar"
	    	 end
	    elsif params[:accion] == "Rechazada" 
	    	solicitud.update :estado => "Rechazada"
	    	descripcion = "Solicitud Rechazada"

	    elsif params[:accion] == "Devuelta" 
	    	solicitud.update :estado => "Devuelta"
	    	descripcion = "Solicitud Devuelta"
	    elsif params[:accion] == "Enviada" 
	    	solicitud.update :estado => "Enviada"
	    	descripcion = "Solicitud reenviada"
	    elsif params[:accion] == "Eliminada" 
	    	solicitud.update :estado => "Eliminada"
	    	descripcion = "Solicitud Cancelada"	
	    end
	    	
	    	
		movimiento = DocenteSolicitudMovimiento.new
        movimiento.descripcion = descripcion
        movimiento.estado = params[:accion]
        movimiento.comentario = params[:comentario]
        movimiento.departamento_id = @usuario_actual.departamento_id
        movimiento.usuario_id = @usuario_actual.id
        movimiento.docente_solicitud_id = solicitud.id
        movimiento.save

        if not params[:id_reemplazo].blank?
        	if not solicitud.docente_reemplazar.blank?
        		rem = DocenteReemplazar.get solicitud.docente_reemplazar.id
        		rem.destroy
        	end
        	reemplazo = DocenteReemplazar.new
        	reemplazo.docente_id = params[:id_reemplazo]
        	reemplazo.docente_solicitud_id = solicitud.id
        	reemplazo.save
        end

        if not params[:id_sugerido].blank?
        	if not solicitud.docente_sugerido.blank?
        		sug = DocenteSugerido.get solicitud.docente_sugerido.id
        		sug.destroy
        	end
        	sugerido = DocenteSugerido.new
        	sugerido.docente_id = params[:id_sugerido]
        	sugerido.docente_solicitud_id = solicitud.id
        	sugerido.save
        end

        #docuentos adjuntos   
			if !params[:doc_guardar].blank?
				params[:doc_guardar].each do |d|
					doc = DocenteSolicitudArchivo.get d[:id].to_i
					doc.update(:docente_solicitud_id => solicitud.id)
				end
			end
			if !params[:doc_borrar].blank?
				params[:doc_borrar].each do |d|
					doc = DocenteSolicitudArchivo.get d[:id].to_i
					doc.destroy
				end
			end
			#fin doc adjuntos

   		respond_to do |format|
       		format.json {
       			render :json => {
       				"estado" => "Exito"
       			}
       		}
     	end
		rescue Exception => e
			respond_to do |format|
		      format.json {
       			render :json => {
       				"estado" => "Error #{e.message}"
       			}
       		}
		end     	

	end

	def crea_solicitud

		DocenteSolicitud.transaction do 
			solicitud = DocenteSolicitud.new params[:solicitud]
			solicitud.desde = params[:fechas].split(" - ").first
	        solicitud.hasta = params[:fechas].split(" - ").last
	        solicitud.usuario_id = @usuario_actual.id
	        solicitud.estado = "Enviada"
	        solicitud.save

	        movimiento = DocenteSolicitudMovimiento.new
	        movimiento.descripcion = "Solicitud Creada"
	        movimiento.estado = "Enviada"
	        movimiento.comentario = "-"
	        movimiento.departamento_id = @usuario_actual.departamento_id
	        movimiento.usuario_id = @usuario_actual.id
	        movimiento.docente_solicitud_id = solicitud.id
	        movimiento.save

	        if not params[:id_reemplazo].blank?
	        	reemplazo = DocenteReemplazar.new
	        	reemplazo.docente_id = params[:id_reemplazo]
	        	reemplazo.docente_solicitud_id = solicitud.id
	        	reemplazo.save
	        end

	        if not params[:id_sugerido].blank?
	        	sugerido = DocenteSugerido.new
	        	sugerido.docente_id = params[:id_sugerido]
	        	sugerido.docente_solicitud_id = solicitud.id
	        	sugerido.save
	        end

	        #docuentos adjuntos   
			if !params[:doc_guardar].blank?
				params[:doc_guardar].each do |d|
					doc = DocenteSolicitudArchivo.get d[:id].to_i
					doc.update(:docente_solicitud_id => solicitud.id)
				end
			end
			if !params[:doc_borrar].blank?
				params[:doc_borrar].each do |d|
					doc = DocenteSolicitudArchivo.get d[:id].to_i
					doc.destroy
				end
			end
			#fin doc adjuntos
		end

   		respond_to do |format|
       		format.json {
       			render :json => {
       				"estado" => "Exito"
       			}
       		}
     	end
		rescue Exception => e
			respond_to do |format|
		      format.json {
       			render :json => {
       				"estado" => "Error #{e.message}"
       			}
       		}
		end     	
		
	end

	def guarda_doc_solicitud

		documento = DocenteSolicitudArchivo.new params[:doc]
		documento.departamento_id = @usuario_actual.departamento_id
		documento.usuario_id = @usuario_actual.id
		documento.docente_solicitud_id = 1
		documento.save


		
		respond_to do |format|
       		format.json {
       			render :json => {
       				"estado" => "Exito",
       				"url" => documento.archivo.url,
       				"id" => documento.id,
       				"name" => documento.name,
       				"fecha" => documento.created_at.strftime("%d/%m/%Y %H:%M"),
       				"por" => "#{@usuario_actual.nombre} #{@usuario_actual.apaterno}"
       			}
       		}
     	end
		rescue Exception => e
			respond_to do |format|
		      format.json {
       			render :json => {
       				"estado" => "Error #{e.message}"
       			}
       		}
		    end
		
	end

	def solicitudes

		if @usuario_actual.sistema.dotacion
			if @usuario_actual.sistema.dotacion_admin 
				@solicitudes = DocenteSolicitud.all(
					:estado => "Enviada", 
					:es_nulo => false
				) | DocenteSolicitud.all(
						:establecimiento_id => @usuario_actual.departamento.establecimiento_id,
						:estado => "Rechazada", 
						:es_nulo => false
				) | DocenteSolicitud.all(
						:usuario_id => @usuario_actual.departamento.establecimiento_id, 
						:estado => "Devuelta", 
						:es_nulo => false)
			else
				@solicitudes = DocenteSolicitud.all(
						:establecimiento_id => @usuario_actual.departamento.establecimiento_id,
						:estado => "Rechazada", 
						:es_nulo => false
				) | DocenteSolicitud.all(
						:usuario_id => @usuario_actual.departamento.establecimiento_id, 
						:estado => "Devuelta", 
						:es_nulo => false
				)
			end
		end

	end

	def solicitudes_enviadas

		@solicitudes = DocenteSolicitud.all(
			:establecimiento_id => @usuario_actual.departamento.establecimiento_id, 
			:es_nulo => false) | DocenteSolicitud.all(
			:usuario_id => @usuario_actual.departamento.establecimiento_id, 
			:es_nulo => false)
		
	end


	def remplazos_activos
		@contratos = DocenteContratado.all(:desde.lte => Date.today, :hasta.gt => Date.today)
	end

	def remplazos_terminados
		@contratos = DocenteContratado.all(:desde.lte => Date.today, :hasta.gt => Date.today)
	end


end
