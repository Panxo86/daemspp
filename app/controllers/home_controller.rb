#encoding: utf-8
class HomeController < ApplicationController




	def general
		periodo = session[:periodo].id
		est = @usuario_actual.departamento.establecimiento_id
		respond_to do |format|
	      format.html 
	      format.json { render json: OpisDatatable.new(view_context, periodo, est) }
	    end
		
	end

	def redirigir
		if @usuario_actual.encargado_subvencions(:es_vigente => true).count > 0

			redirect_to entrantes_encargado_subvencion_path

		elsif  @usuario_actual.asesor_subvencions(:es_vigente => true).count > 0

			redirect_to entrantes_asesor_subvencion_path

		elsif not @usuario_actual.bandeja_usuarios.blank?

			 redirect_to bandeja_entrantes_path @usuario_actual.bandeja_usuarios.first.bandeja_id
		else
			redirect_to mi_bandeja_path
		end
	end


	def dashboard
		@e = Establecimiento.get params[:id]
		@ordenes_totales = Orden.count(:periodo_id => session[:periodo].id.to_i, :es_nula => false);
		@ordenes_proceso = Orden.count(:periodo_id => session[:periodo].id.to_i, :es_nula => false);
			
		@o = Orden.all(
			:periodo_id => session[:periodo].id.to_i, 
			:es_nula => false, 
			:departamento => {
				:establecimiento_id => @e.id
				},
			:es_entrante => true
			)

		@dpto_direccion = Departamento.first(:es_direccion => true, :establecimiento_id => @e.id)
		@ordenes_director = @o.count(:estado => "En digitacion", :ubicacion => {:departamento_id => @dpto_direccion.id})
		@ordenes_daem =  @o.count(:estado => "En proceso", :ubicacion => {:departamento_id => 10})
		@ordenes_consulta = @o.count(:estado => "En consulta") 
		@ordenes_finanzas = @o.count(:estado => "En proceso", :ubicacion => {:departamento_id => 2})
		@ordenes_adquisiciones = @o.count(:ubicacion => {:departamento_id => 1}) 

Subvencion.todos.each do |s|
	 			 	end

	 @o.map do |sub|
	 	{
	 		 
	 	}
	 end

		


	end

	def perfil
		
	end

	def actualiza_perfil
		user = Usuario.get @usuario_actual.id
		user.update params[:usuario]
		redirect_to perfil_path
		flash[:notificacion] = "Se ha actualizado su perfil exitosamente"
	rescue
		flash[:error] = "Error, verifique los campos ingresados e intentelo nuevamente"
		redirect_to perfil_path
	end

	def pass
		
	end

	def actualiza_pass

		if params[:nueva] == params[:verifica]
			if params[:actual] == @usuario_actual.password
				user = Usuario.get @usuario_actual.id
				user.update(:password => params[:nueva])
				flash[:notificacion] = "Se ha cambiado su password exitosamente"

			else
				flash[:error] = "Password actual es erronea, intentelo nuevamente"
			end
		else
			flash[:error] = "Las passwords no coinciden, intentelo nuevamente"
		end
		redirect_to pass_path
		rescue
			flash[:error] = "ha ocurrido un error cambiando su password, intentelo nuevamente"
			redirect_to pass_path
	end
end