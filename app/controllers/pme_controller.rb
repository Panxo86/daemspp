class PmeController < ApplicationController

  def index
  	est = params[:establecimiento_id]
  	@objetivos = Objetivo.all(:periodo_id => session[:periodo].id, :establecimiento_id => @usuario_actual.departamento.establecimiento_id, :es_vigente => true, :order => :dimencion_id)

  end

  def objetivo
  	
  end

  def ver

  	@o = Objetivo.get params[:id]
  	
  end

  def ver_accion
  	@a = Accion.get params[:id]
  	@ordenes = Orden.all(:periodo_id => session[:periodo].id, :orden_tiene_accion => {:accion_id => @a.id} )
  end

  def actualiza_accion_plan
    
    accion = Accion.get params[:id]
    accion.update :desde => params[:fechas].split(" - ").first
    accion.update :hasta => params[:fechas].split(" - ").last
    accion.update params[:accion]
    flash[:notificacion] = "Actualizada Exitosamente"
    redirect_to :back
  rescue
    flash[:error] = "Ha ocurrido un error, intentelo nuevamente"
    redirect_to :back
  end

    
end
