class DocenteController < ApplicationController

	def get_docente

		rut = params[:rut].gsub(".","").split("-").first
		dv = params[:rut].gsub(".","").split("-").last

		docente = Docente.all(:rut => rut).first

		render :json => docente

	end

	def actualizar_docente

		rut = params[:rut].gsub(".","").split("-").first
		dv = params[:rut].gsub(".","").split("-").last

		buscar = Docente.all(:rut => rut).first
		if buscar.blank?
			docente = Docente.new params[:docente]
			docente.rut = rut
			docente.dv = dv
			docente.save
		else
			buscar.update params[:docente]
			docente = buscar
		end
		
	 respond_to do |format|
            format.json {
                render :json => {
                    "estado" => "Exito",
                    :docente => docente
                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            puts "ERROR:::: #{e.backtrace}".red
            respond_to do |format|
              format.json {
                render :json => {
                    "estado" => "Error: #{e.message}"
                }
            }
        end
	end


end
