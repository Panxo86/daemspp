class PmeParametrosController < ApplicationController

	def dimensiones
		@dimensiones = PmeDimension.all(:es_vigente => true)
	end

	def get_pme_dimension
    
		id = params[:id]
		datos = PmeDimension.all(:es_vigente => true, :order => :nombre)
		array = "<option value=''></option>"
		datos.each do |p|
			array += "<option value='#{p.id}'>#{p.nombre}</option>"
		end
		render :json => { :opciones => array }
	end

	def create_pme_dimension
		dimension = PmeDimension.new params[:dimension]
		#puts "#{dimension.inspect}".yellow
		dimension.save

		flash[:notificacion] = "Se ha guardado con exito"
		redirect_to :back
	rescue Exception => e
		flash[:error] = "Ha ocurrido un error, #{e.message}"
		redirect_to :back
	end


	def jeditable_nombre_pme_dimension
		nuevo = params[:newvalue]
		id = params[:elementid].to_i
		obj = PmeDimension.get id
		obj.update :nombre => nuevo
		render :json => nuevo
	end

	def destroy_pme_dimension
		@dimension = PmeDimension.get(params[:id])
		@dimension.update(es_vigente: false)
		

		flash[:notificacion] = "Se ha borrado existosamente"
		redirect_to :back

	rescue
		flash[:error] = "Error"
		redirect_to :back
	end  


	def subdimensiones
		@dimension = PmeDimension.get(params[:id])
		@subdimensiones = PmeSubDimension.all(:pme_dimension_id => @dimension.id, :es_vigente => true)
	end

	def get_pme_subdimension
    
		id = params[:id]
		datos = PmeSubDimension.all(:pme_dimension_id => id, :es_vigente => true, :order => :nombre)
		array = "<option value=''></option>"
		datos.each do |p|
			array += "<option value='#{p.id}'>#{p.nombre}</option>"
		end
		render :json => { :opciones => array }
	end

	def create_pme_subdimension
		dimension = PmeSubDimension.new params[:dimension]
		dimension.save

		flash[:notificacion] = "Se ha guardado con exito"
		redirect_to :back
	rescue 
		flash[:error] = "Ha ocurrido un error, revise campo ingresado"
		redirect_to :back
	end


	def jeditable_nombre_pme_dimension
		nuevo = params[:newvalue]
		id = params[:elementid].to_i
		obj = PmeSubDimension.get id
		obj.update :nombre => nuevo
		render :json => nuevo
	end

	def destroy_pme_subdimension
		@dimension = PmeSubDimension.get(params[:id])
		@dimension.update(es_vigente: false)
		

		flash[:notificacion] = "Se ha borrado existosamente"
		redirect_to :back

	rescue
		flash[:error] = "Error"
		redirect_to :back
	end  

	

end
