class AdquisicionesController < ApplicationController

  def index
  	@buzon = "Adquisiciones"
  end

  def decreto
  	    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :estado => "Espera decreto")

  end

  def orden_compras

  	@adquisiciones = Adquisicion.all(:orden => {:periodo_id => session[:periodo].id, :es_nula => false})
  	
  end

  def elimina_orden

    @adquisicion = Adquisicion.get params[:id]
    @adquisicion.update(:motivo => params[:motivo], :eliminada => true)



    respond_to do |format|
        format.html {       
            flash[:notificacion] = "Se ha Eliminado la orden Exitosamente."
            redirect_to orden_compras_path 
        }
        format.json {
            render :json => {
                "estado" => "Exito",
                "id" => params[:id],
                "motivo" => params[:motivo],
                :orden => @orden
          }
        }
    end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to orden_compras_path
              }
              format.json {
                render :json => {
                    "estado" => "Ha ocurrido un error: #{e.message}"
                }
            }
        end

  end


  def guarda_oc

    url = ""
    id = ""
    Adquisicion.transaction do

      if params[:doc].blank?
        raise "Error sin doc"
      end
      busca_oc = Adquisicion.all(
        :orden_id => params[:id], 
        :orden_compra => params[:compra][:orden_compra],
        :eliminada => false,
        :es_nula => false)
      if not busca_oc.blank?
        raise "La OC ya ha sido ingresada"
      end
      movimiento = Movimiento.new(
                :descripcion => "Agregada Orden de Compra",
                :estado => "-",
                :comentario => "Orden #{params[:compra][:orden_compra]} guardada",
                :departamento_id => @usuario_actual.departamento_id,
                :orden_id => params[:id],
                :usuario_id => @usuario_actual.id
              )
      movimiento.save
      ad = Adquisicion.new params[:compra]
      ad.orden_id = params[:id]
      ad.save
      id = ad.id             
      documento = OrdenCompraDoc.new params[:doc]
      documento.adquisicion_id = ad.id
      documento.save
      url = documento.archivo.url
    end



    respond_to do |format|
        format.html {       
            flash[:notificacion] = "Se ha Eliminado la orden Exitosamente."
            redirect_to orden_compras_path 
        }
        format.json {
            render :json => {
                "estado" => "Exito",
                "oc" => params[:compra][:orden_compra],
                "id" => id,
                "fecha" => params[:compra][:fecha],
                "monto" => params[:compra][:monto],
                "url" => url
          }
        }
    end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to orden_compras_path
              }
              format.json {
                render :json => {
                    "estado" => "#{e.message}"
                }
            }
        end

  end


end
