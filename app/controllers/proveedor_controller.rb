#encoding: utf-8
class ProveedorController < ApplicationController
	

	def get
		  rut = params[:rut].gsub('.', '')
	      proveedor = Proveedor.first(:rut => rut)
	      msj = {}
	      if not proveedor.blank?
	        msj['msj'] = "si"
	      else
	        msj['msj'] = "no"
	      end
	      render :json => {
	        :msj => msj,
	        :proveedor => proveedor
	      }
			
		end

	def crear

		@proveedor = Proveedor.new params[:proveedor]
		@proveedor.rut = @proveedor.rut.gsub('.', '')
		@proveedor.save

		respond_to do |format|
       		format.html { 		
       			flash[:notificacion] = "Se a registrado el proveedor exitosamente."
				redirect_to alta_path 
			}
       		format.js
     	end
		rescue Exception => e
			flash[:error] = "Error en el registro #{e.message}"
			redirect_to alta_path 
	end
end