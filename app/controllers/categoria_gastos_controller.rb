class CategoriaGastosController < ApplicationController

  def sl_categoria_gasto
    id = params[:id]
    datos = CategoriaGasto.all(:es_vigente => true, :tipo_gasto_id => id, :order => :nombre)
    array = "<option value=''>Seleccione Tipo Gasto</option>"
    datos.each do |p|
    array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => { :opciones => array }
  end

  def guarda_categoria_gasto
	cat = CategoriaGasto.new params[:cat]
    cat.save
    flash[:notificacion] = "Se ha creado exitosamente"

    redirect_to categoria_gasto_subvencion_path params[:cat][:tipo_gasto_id].to_i
  rescue
    flash[:error] = "revise los campos ingresados"
    redirect_to categoria_gasto_subvencion_path params[:cat][:tipo_gasto_id].to_i
  end

    def jeditable_nombre_cat_gasto
    nuevo = params[:newvalue]
    id = params[:elementid].to_i
    cat = CategoriaGasto.get id
    cat.update :nombre => nuevo
    render :json => nuevo
  end

  def jeditable_codigo_cat_gasto
    nuevo = params[:newvalue]
    id = params[:elementid].to_i
    cat = CategoriaGasto.get id
    cat.update :codigo => nuevo
    render :json => nuevo
  end

  def borrar_cat_gasto
    cat = CategoriaGasto.get params[:id]
    cat.update :es_vigente => false
    flash[:notificacion] = "Se ha eliminado exitosamente"
    redirect_to :back
  rescue
    flash[:error] = "Ha ocurrido un error, intentelo nuevamente"
    redirect_to :back
  end
end
