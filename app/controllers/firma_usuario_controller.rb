class FirmaUsuarioController < ApplicationController

	def sube

		if not params[:doc].blank?
			actual = FirmaUsuario.all(:usuario_id => params[:id])
			if not actual.blank?
				actual.destroy
			end
			firma = FirmaUsuario.new params[:doc]
			firma.usuario_id = params[:id]
			firma.save
		else
			raise "Debe incluir el archivo"
		end
		
		flash[:notificacion] = "Firma creada con exito"
		redirect_to editar_usuario_path params[:id]

		rescue Exception => e
			flash[:error] = "#{e.message}"
		    redirect_to editar_usuario_path params[:id]
		
	end

	def borra
		actual = FirmaUsuario.all(:usuario_id => params[:id])
		actual.destroy
		flash[:notificacion] = "Firma borrada con exito"
		redirect_to editar_usuario_path params[:id]

		rescue Exception => e
			flash[:error] = "#{e.message}"
		    redirect_to editar_usuario_path params[:id]
	end
end
