class SubvencionsController < ApplicationController
  def guarda_subvencion
    Subvencion.transaction do
      sub = Subvencion.new params[:subvencion]
      sub.save

      establecimientos = Establecimiento.all
        establecimientos.each do |e|
            presupuesto = PresupuestoSubvencion.create(
              :monto => "0",
              :establecimiento_id => e.id,
              :subvencion_id => sub.id,
              :periodo_id => session[:periodo].id
            )
          end
    end
    flash[:notificacion] = "Se ha creado exitosamente"

    redirect_to subvenciones_orden_path
  rescue
    flash[:error] = "revise los campos ingresados"
    redirect_to subvenciones_orden_path
  end

  def get_subvencion
    @s = Subvencion.get params[:id].to_i
=begin
    @pagado = 0
    @en_gestion = 0
    @disponible = 0

    @presupuesto = PresupuestoSubvencion.first(
      :periodo_id => session[:periodo].id,
      :establecimiento_id => @usuario_actual.departamento.establecimiento_id.to_i,
      :subvencion_id => @s.id
      )

    compradas = Adquisicion.all(
      :orden => {
        :es_nula => false,
        :periodo_id => session[:periodo].id,
        :departamento => {
          :establecimiento_id => @usuario_actual.departamento.establecimiento_id.to_i
        },
        :subcategoria_gasto => {
          :categoria_gasto => {
            :tipo_gasto => {
              :subvencion_id => @s.id
            }
          }
        }
      }
    )
    compradas.each do |c|
      @pagado += c.monto.to_i
    end


    en_proceso = Orden.all(
      :es_nula => false,
      :periodo_id => session[:periodo].id,
      :departamento => {
        :establecimiento_id => @usuario_actual.departamento.establecimiento_id.to_i
      },
      :subcategoria_gasto => {
            :categoria_gasto => {
              :tipo_gasto => {
                :subvencion_id => @s.id
              }
            }
          }
    )
    en_proceso = en_proceso.all(:estado => "Espera decreto") | en_proceso.all(:estado => "En proceso") | en_proceso.all(:estado => "En consulta") |en_proceso.all(:estado => "En digitacion") | en_proceso.all(:estado => "En Licitacion") | en_proceso.all(:estado => "Rechazada")

    en_proceso.each do |o|
      o.bien_servicios.each do |b|
        @en_gestion += b.total.to_i
      end
      
    end
    @disponible = @presupuesto.monto.to_i - @en_gestion - @pagado
=end

    @pagado = pagado_subvencion(@usuario_actual.departamento.establecimiento_id, @s.id)
    @disponible = disponible_subvencion(@usuario_actual.departamento.establecimiento_id, @s.id)
    @presupuesto = presupuesto_sub(@usuario_actual.departamento.establecimiento_id, @s.id)
    @en_gestion = en_gestion_subvencion(@usuario_actual.departamento.establecimiento_id, @s.id)
    render :json => {
      :es_sep => @s.es_sep,
      :pagado => @pagado,
      :presupuesto => @presupuesto,
      :tramite => @en_gestion,
      :restante => @disponible
    }
  end

end
