class OpiController < ApplicationController

	def nueva
		
	end


  def actualizar


    Orden.transaction do
      @orden = Orden.get(params[:id])
      subvencion_actual = obtener_subvencion_subcategoria(@orden.subcategoria_gasto_id)

      if not params[:orden][:subcategoria_gasto_id].blank?
        subvencion = obtener_subvencion_subcategoria(params[:orden][:subcategoria_gasto_id])
        saldo_disponible = disponible_subvencion(@usuario_actual.departamento.establecimiento_id, subvencion.id)
        if subvencion_actual.id == subvencion.id
          total_actual = 0
          @orden.bien_servicios.each do |b|
            total_actual += b.total.to_i
          end
          total_actual = total_actual * 1.19
          saldo_disponible = saldo_disponible + total_actual
        end
        solicitado = params[:solicitado].to_i * 1.19
        #puts "#{subvencion.id}".yellow
        if saldo_disponible.to_i <= solicitado.to_i
          raise "El monto solicitado excede al disponible"
        end

        

        if subvencion_actual.id != subvencion.id

          if subvencion.es_sep
            if params[:accion_id].blank?
              raise "Debe ingresar el PME antes de guardar"
            else
              pme = OrdenTienePmeAccion.create(:orden_id => @orden.id, :pme_accion_id => params[:accion_id])
            end
          else
            busca_pme = OrdenTienePmeAccion.all(:orden_id => @orden.id)
            if not busca_pme.blank?
              busca_pme.destroy
            end
          end
          ruta_orden = Ruta.all(:orden_id => @orden.id)
          ruta_orden.update(:aprobada => false, :activa => false)
          ruta_orden.first.update(:activa => true)


        end
      else
        saldo_disponible = disponible_subvencion(@usuario_actual.departamento.establecimiento_id, subvencion_actual.id)
        total_actual = 0
        @orden.bien_servicios.each do |b|
          total_actual += b.total.to_i
        end
        total_actual = total_actual * 1.19
        saldo_disponible = saldo_disponible + total_actual
        solicitado = params[:solicitado].to_i * 1.19
        if saldo_disponible.to_i <= solicitado.to_i
          raise "El monto solicitado excede al disponible"
        end
      end

      if !params[:orden][:titulo].blank?
        @orden.update(:titulo => params[:orden][:titulo])
      else
        raise "El titulo no puede estar vacio"
      end
      if !params[:orden][:justificacion].blank?
        @orden.update(:justificacion => params[:orden][:justificacion])
      else
        raise "La justificacion no puede estar vacia"
      end

      articulos = BienServicio.all(:orden_id => @orden.id)
      articulos.destroy

      if not params[:articulo].blank?
        params[:articulo].each do |a|
          if a[:cantidad].blank?
            raise "La cantidad del bien o servicio no puede ir en blanco"
          end
          if a[:descripcion].blank?
            raise "La descripcion del bien o servicio no puede ir en blanco"
          end
          if a[:valor].blank?
            raise "El valor del bien o servicio no puede ir en blanco"
          end
        end
      else
        raise "Debe ingresar algun bien o servicio a solicitar"
      end

      params[:articulo].each do |a|
        if !a[:cantidad].blank? and !a[:descripcion].blank?
          bien = BienServicio.create(
            :descripcion => a[:descripcion],
            :cantidad => a[:cantidad],
            :valor => a[:valor],
            :total => a[:total],
            :orden_id => @orden.id
          ) 
        else
          raise "No existen articulos en la OPI"
        end
      end
      if !params[:orden][:subcategoria_gasto_id].blank?
        @orden.update(:subcategoria_gasto_id => params[:orden][:subcategoria_gasto_id])
      end
      if @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.es_sep
        #puts "ES SEEEEEP"
        if !params[:accion_id].blank?
          accion = OrdenTienePmeAccion.first(:orden_id => @orden.id)
          accion.update(:pme_accion_id => params[:accion_id])
        end
      end
    end

    respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Guardado la orden Exitosamente."
                redirect_to nueva_orden_path 
            }
            format.json {
                render :json => {
                    "estado" => "Exito",
                    :orden => @orden
                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            puts "ERROR:::: #{e.backtrace}".red
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to nueva_orden_path
              }
              format.json {
                render :json => {
                    "estado" => "Error: #{e.message}"
                }
            }
        end
    
  end

	def crear

    Orden.transaction do

      subvencion = obtener_subvencion_subcategoria(params[:orden][:subcategoria_gasto_id])
      solicitado = params[:solicitado].to_i * 1.19
      #puts "#{subvencion.id}".yellow
      saldo_disponible = disponible_subvencion(@usuario_actual.departamento.establecimiento_id, subvencion.id)
      if saldo_disponible.to_i <= solicitado.to_i
      	raise "El monto solicitado excede al disponible"
      end
      @orden = Orden.new(params[:orden])
      @orden.usuario_id = @usuario_actual.id
      @orden.cod = "#"
      @orden.departamento_id = @usuario_actual.departamento.id
      @orden.estado = "En digitacion"
      @orden.periodo_id = session[:periodo].id

      if not params[:articulo].blank?
        params[:articulo].each do |a|
          if a[:cantidad].blank?
            raise "La cantidad del bien o servicio no puede ir en blanco"
          end
          if a[:descripcion].blank?
            raise "La descripcion del bien o servicio no puede ir en blanco"
          end
          if a[:valor].blank?
            raise "El valor del bien o servicio no puede ir en blanco"
          end
        end
      else
        raise "Debe ingresar algun bien o servicio a solicitar"
      end

      @orden.bien_servicios = params[:articulo]
      @orden.save

      #firmas imprimible
      firmas = Firma.new
      firmas.orden_id = @orden.id
      firmas.save

      #docuentos adjuntos   
      if !params[:doc_guardar].blank?
        params[:doc_guardar].each do |d|
          doc = Documento.get d[:id].to_i
          doc.update(:orden_id => @orden.id)
        end
      end
      if !params[:doc_borrar].blank?
        params[:doc_borrar].each do |d|
          doc = Documento.get d[:id].to_i
          doc.destroy
        end
      end
      #fin doc adjuntos



      #obtener direccion del establecimiento de la opi
      direccion = Departamento.first(:establecimiento_id => @usuario_actual.departamento.establecimiento_id, :es_direccion => true)
      director = Usuario.all(:departamento_id => direccion.id, :es_nulo => false).last
      if not director.blank?

        #ruta OPI
      conducto = Conducto.all(:bien => true)
      conducto.each do |c|
        ruta = Ruta.new(
          :bandeja_id => c.bandeja_id,
          :orden_id => @orden.id,
          :usuario_id => director.id,
          :firma => c.firma
        )
        ruta.save
      end
        #si es que es el director emitiendo opi
        if @usuario_actual.departamento_id == direccion.id || @usuario_actual.departamento.establecimiento_id == 1

        	mover_opi = Ruta.all(:orden_id => @orden.id).first
        	mover_opi.update(:activa => true)
          EducaMailer.entrante(mover_opi).deliver
          
          movimiento = Movimiento.new(
            :descripcion => "OPI Creada",
            :estado => "Conducto Regular",
            :comentario => "En proceso",
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          @orden.update(:estado => "Conducto Regular", :en_proceso => true, :director => director.id)

          #firma = Firma.all(:orden_id => @orden.id).first
          #firma.update :director_est => true
        
        #else si es otro dpto emitiendo (por lo q se debe enviar a direccion para validar)
        else
          movimiento = Movimiento.new(
            :descripcion => "OPI Creada",
            :estado => "Digitada",
            :comentario => "Espera validacion Director Establecimiento",
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          @orden.update(:estado => "Digitada", :en_proceso => true, :es_editable => true)
          
          EducaMailer.validar(director.email, @orden).deliver

        end
      else
        raise "No existe director asociado al establecimiento!"
      end

      if !params[:accion_id].blank?
        accion_orden = OrdenTienePmeAccion.new(
          :orden_id => @orden.id,
          :pme_accion_id => params[:accion_id]
        )
        accion_orden.save
      end
    end

    respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Creado la orden Exitosamente."
                redirect_to nueva_orden_path 
            }
            format.json {
                render :json => {
                    "estado" => "Exito",
                    :orden => @orden
                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            puts "#{e.backtrace}".red
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to nueva_orden_path
              }
              format.json {
                render :json => {
                    "estado" => "Error: #{e.message}"
                }
            }
        end
  end

  def validar
    
    if not @usuario_actual.departamento.es_direccion
    	flash[:error] = "Solo el Director puede validar OPIS digitadas"
    	redirect_to general_path
    end
  	@validar = Orden.all(
  		:estado => "Digitada", 
      :es_nula => false,
      :periodo_id => session[:periodo].id,
  		:en_proceso => true,
  		:departamento => {
  			:establecimiento_id => @usuario_actual.departamento.establecimiento_id
  			})
  end

  def ver
    @orden = Orden.get params[:id]
  end


end
