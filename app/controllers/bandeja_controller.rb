#encoding: utf-8
class BandejaController < ApplicationController


	def mi_bandeja

      	direccion = Departamento.first(:establecimiento_id => @usuario_actual.departamento.establecimiento_id, :es_direccion => true)
      	director = Usuario.all(:departamento_id => direccion.id).first

      	if @usuario_actual.id == director.id
      		#es director
			@ordenes = Orden.all(
				:es_nula => false,
				:en_proceso => true,
				:periodo_id => session[:periodo].id,
				:consulta => {
					:usuario_id => @usuario_actual.id
				}
			) | Orden.all(
				:es_nula => false,
				:en_proceso => true,
				:periodo_id => session[:periodo].id,
				:en_emisor => true,
				:usuario_id => @usuario_actual.id
			) | Orden.all(
				:es_nula => false,
				:en_proceso => true,
				:periodo_id => session[:periodo].id,
				:en_direccion => true,
				:departamento => {
					:establecimiento_id => direccion.establecimiento_id
				}
			)
		else
			@ordenes = Orden.all(
				:es_nula => false,
				:en_proceso => true,
				:periodo_id => session[:periodo].id,
				:consulta => {
					:usuario_id => @usuario_actual.id
				}
			) | Orden.all(
				:es_nula => false,
				:en_proceso => true,
				:periodo_id => session[:periodo].id,
				:en_emisor => true,
				:usuario_id => @usuario_actual.id
			)

		end



	end

	def entrantes

		@bandeja = Bandeja.get params[:id]

		@ordenes = Orden.all(
			:es_nula => false,
			:en_proceso => true, 
			:estado => "Conducto Regular",
			:periodo_id => session[:periodo].id,
			:rutas =>{
				:bandeja_id => @bandeja.id,
				:activa => true
				})
		
	end

	def asesor_subvencion
		#puts "def asesor #{@usuario_actual.asesor_subvencions.inspect}".yellow
		if @usuario_actual.asesor_subvencions.blank? 
			flash[:error] = "No tiene permiso para ingresar a esta bandeja"
			redirect_to general_path
		end
		@ordenes = Orden.all(
			:es_nula => false,
			:en_proceso => true, 
			:estado => "Conducto Regular",
			:periodo_id => session[:periodo].id,
			:rutas =>{
				:bandeja_id => 2,
				:activa => true
				},
			:subcategoria_gasto => {
				:categoria_gasto => {
					:tipo_gasto => {
						:subvencion => {
							:asesor_subvencions => {
								:usuario_id => @usuario_actual.id,
								:es_vigente => true
							}
						}
					}
				}
			}
		)
	end

	def encargado_subvencion
		#puts "asesor #{@usuario_actual.encargado_subvencions.inspect}".yellow

		if @usuario_actual.encargado_subvencions.blank? 
			flash[:error] = "No tiene permiso para ingresar a esta bandeja"
			redirect_to general_path
		end

		@ordenes = Orden.all(
			:es_nula => false,
			:en_proceso => true, 
			:estado => "Conducto Regular",
			:periodo_id => session[:periodo].id,
			:rutas =>{
				:bandeja_id => 1,
				:activa => true
				},
			:subcategoria_gasto => {
				:categoria_gasto => {
					:tipo_gasto => {
						:subvencion => {
							:encargado_subvencions => {
								:usuario_id => @usuario_actual.id,
								:es_vigente => true
							}
						}
					}
				}
			}
			)

	end

	def bandeja_entrantes

		@b = Bandeja.get params[:id]
		usuario = BandejaUsuario.all(:usuario_id => @usuario_actual.id, :bandeja_id => params[:id])
		#puts "def entrantes #{usuario.inspect}".blue
		if usuario.blank?
			flash[:error] = "No tiene permiso para ingresar a esta bandeja"
			redirect_to general_path
		end
		
	end

	def guarda

		bandeja = Bandeja.new params[:nombre]
		bandeja.save

		flash[:notificacion] = "Bandeja creada exitosamente"

		redirect_to ver_bandeja_path bandeja.id

	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to bandejas_path
	end

	def ver

		@b = Bandeja.get params[:id]
		
	end

	def guarda_usuario

		@b = Bandeja.get params[:id]

		usuario = BandejaUsuario.new params[:usuario]
		usuario.bandeja_id = @b.id
		usuario.save
		
		flash[:notificacion] = "Usuario Agregado exitosamente"

		redirect_to ver_bandeja_path @b.id

	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to ver_bandeja_path @b.id
	end

	def elimina_usuario
		
		@b = Bandeja.get params[:id]

		usuario = BandejaUsuario.all(:bandeja_id => params[:id], :usuario_id => params[:usuario_id])
		usuario.destroy
		
		flash[:notificacion] = "Usuario quitado exitosamente"

		redirect_to ver_bandeja_path params[:id]
	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to ver_bandeja_path params[:id]
	end
end
