#encoding: utf-8
class FinanzasController < ApplicationController

  def liquidacion_subvenciones



  end

  def elimina_liquidacion
    liq = LiquidacionSubvencion.get params[:id]
    liq.update :es_nula => true
    flash[:notificacion] = "Se ha eliminado exitosamente"

    redirect_to listado_liquidacion_path
  rescue Exception => e
    puts "ERROR:::: #{e.message}"
    flash[:error] = "Error al eliminar"
    redirect_to ver_liquidacion_path params[:id]
  end


  def eliminar_subvencion_base
    sub = SubvencionBase.get params[:id_subvencion_base].to_i
    sub.update :es_vigente => false
      redirect_to subvenciones_base_path 
    flash[:notificacion] = "Se ha eliminado exitosamente"
  rescue
    flash[:error] = "Error, intente nuevamente"
    redirect_to subvenciones_base_path 
    
  end

  def eliminar_subvencion_orden
    sub = Subvencion.get params[:id_subvencion_orden].to_i
    sub.update :es_vigente => false
      redirect_to subvenciones_orden_path 
    flash[:notificacion] = "Se ha eliminado exitosamente"
  rescue Exception => e
    puts "#{e.message}".red
    flash[:error] = "Error, intente nuevamente "
    redirect_to subvenciones_orden_path 
    
  end

  def carga_presupuesto
    @cargas = CargaDirecta.all(:periodo_id => session[:periodo].id, :es_nula => false)
  end

  def guarda_presupuesto

    CargaDirecta.transaction do 
      carga = CargaDirecta.new params[:cargar]
      carga.periodo_id = session[:periodo].id
      carga.usuario_id = @usuario_actual.id
      puts "#{carga.inspect}".yellow
      carga.save

      puts "#{carga.inspect}".yellow
      presupuesto = PresupuestoSubvencion.last(
        :periodo_id => session[:periodo].id, 
        :establecimiento_id => carga.establecimiento_id,
        :subvencion_id => carga.subvencion_id
        )
      if presupuesto.blank?
        presupuesto = PresupuestoSubvencion.create(
        :periodo_id => session[:periodo].id, 
        :establecimiento_id => carga.establecimiento_id,
        :subvencion_id => carga.subvencion_id,
        :monto => carga.total)
      else
        nuevo_presupuesto = presupuesto.monto.to_i + carga.total.to_i
        presupuesto.update :monto => nuevo_presupuesto
      end
      saldo = saldo_subvencion(carga.establecimiento_id, carga.subvencion_id)
      historial = HistorialPresupuesto.create(
        :descripcion => "Carga directa ID: #{carga.id}, #{carga.periodo.anio} - #{carga.mes}",
        :entrada => "#{carga.total}",
        :salida => "0",
        :total => "#{saldo}",
        :subvencion_id => carga.subvencion_id,
        :periodo_id => session[:periodo].id, 
        :establecimiento_id => carga.establecimiento_id,
        :usuario_id => @usuario_actual.id
      )
    end
    redirect_to carga_presupuesto_path 
    flash[:notificacion] = "Se ha cargado exitosamente"
  rescue Exception => e
    puts "#{e.message}".red
    puts "#{e.backtrace}".red
    flash[:error] = "Error, intente nuevamente"
    redirect_to carga_presupuesto_path 

  end

   def anula_carga

    CargaDirecta.transaction do 
      carga = CargaDirecta.get params[:id]
      carga.update :es_nula => true

      presupuesto = PresupuestoSubvencion.first(
        :periodo_id => session[:periodo].id, 
        :establecimiento_id => carga.establecimiento_id,
        :subvencion_id => carga.subvencion_id
        )
      nuevo_presupuesto = presupuesto.monto.to_i - carga.total.to_i
      presupuesto.update :monto => nuevo_presupuesto
     saldo = saldo_subvencion(carga.establecimiento_id, carga.subvencion_id)
      historial = HistorialPresupuesto.create(
        :descripcion => "Anula Carga directa ID: #{carga.id}, #{carga.periodo.anio} - #{carga.mes}",
        :entrada => "0",
        :salida => "#{carga.total}",
        :total => "#{saldo}",
        :subvencion_id => carga.subvencion_id,
        :periodo_id => session[:periodo].id, 
        :establecimiento_id => carga.establecimiento_id,
        :usuario_id => @usuario_actual.id
      )
    end
    redirect_to carga_presupuesto_path 
    flash[:notificacion] = "Se ha eliminado exitosamente"
  rescue Exception => e
    puts "#{e.message}".red
    flash[:error] = "Error, intente nuevamente"
    redirect_to carga_presupuesto_path 

  end

  def actualiza_liquidacion
      
    
    LiquidacionSubvencion.transaction do
      @ant = LiquidacionSubvencion.get params[:id].to_i
      @estado = "Exito"
      crear = 0
      obt = LiquidacionSubvencion.first(
        :periodo_id => session[:periodo].id, 
        :establecimiento_id => params[:liq][:establecimiento_id],
        :mes => params[:liq][:mes],
        :es_nula => false)
      if !obt.blank?
        if @ant.mes == obt.mes and @ant.periodo_id == obt.periodo_id
            crear = 1
            puts "El mismo mes remplazar".yellow
        else
          @estado = "Existe"
          puts "otro mes que ya existe".yellow
        end  
      else
        crear = 1
        puts "Mes nuevo".yellow
      end

      if crear == 1
        puts "Entre ".yellow
            sep = Subvencion.first(
              :es_vigente => true,
              :es_sep => true)

            ##PRESUPUESTO SEP 
            presupuesto_sep = PresupuestoSubvencion.first(
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => params[:liq][:establecimiento_id],
              :subvencion_id => sep.id
              )
            if @ant.sep_est.to_i != params[:liq][:sep_est].to_i
              
              nuevo_presupuesto_sep = presupuesto_sep.monto.to_i - @ant.sep_est.to_i
              presupuesto_sep.update :monto => nuevo_presupuesto_sep
              historial = HistorialPresupuesto.create(
                :descripcion => "Modificacion Liq de Subvencion ID #{@ant.id} Subv. SEP",
                :entrada => "0",
                :salida => "#{@ant.sep_est}",
                :total => "#{presupuesto_sep.monto}",
                :subvencion_id => sep.id,
                :periodo_id => session[:periodo].id, 
                :establecimiento_id => params[:liq][:establecimiento_id].to_i,
                :usuario_id => @usuario_actual.id
              )
              
            nuevo_presupuesto_sep = presupuesto_sep.monto.to_i + params[:liq][:sep_est].to_i
            presupuesto_sep.update :monto => nuevo_presupuesto_sep

            historial = HistorialPresupuesto.create(
              :descripcion => "Modificacion Liq de Subvencion ID #{@ant.id} Subv. SEP",
              :entrada => "#{params[:liq][:sep_est]}",
              :salida => "0",
              :total => "#{presupuesto_sep.monto}",
              :subvencion_id => sep.id,
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => params[:liq][:establecimiento_id].to_i,
              :usuario_id => @usuario_actual.id
            )
          end


          ##PRESUPUESTO SEP DAEM
            presupuesto_sep_daem = PresupuestoSubvencion.first(
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => 1,
              :subvencion_id => sep.id
            )
          if @ant.sep_daem.to_i != params[:liq][:sep_daem].to_i
            
            nuevo_presupuesto_sep_daem = presupuesto_sep_daem.monto.to_i - @ant.sep_daem.to_i
            presupuesto_sep_daem.update :monto => nuevo_presupuesto_sep
            puts "Aqi".yellow
            historial_daem = HistorialPresupuesto.create(
              :descripcion => "Modificacion Liq de Subvencion ID #{@ant.id} Subv. SEP",
              :entrada => "0",
              :salida => "#{@ant.sep_daem}",
              :total => "#{presupuesto_sep.monto}",
              :subvencion_id => sep.id,
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => 1,
              :usuario_id => @usuario_actual.id
            )
            nuevo_presupuesto_sep_daem = presupuesto_sep_daem.monto.to_i + params[:liq][:sep_daem].to_i
            presupuesto_sep_daem.update :monto => nuevo_presupuesto_sep_daem
            historial_daem = HistorialPresupuesto.create(
              :descripcion => "Modificacion Liq de Subvencion ID #{@ant.id} Subv. SEP",
              :entrada => "#{params[:liq][:sep_daem].to_i}",
              :salida => "0",
              :total => "#{presupuesto_sep.monto}",
              :subvencion_id => sep.id,
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => 1,
              :usuario_id => @usuario_actual.id
            )
            
          end

          ##SUBVENCIONES BASE

            @ant.liq_bases.each do |b|
              s_base = SubvencionBase.get b.subvencion_base_id.to_i
              presupuesto_est = PresupuestoSubvencion.first(
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => params[:liq][:establecimiento_id],
              :subvencion_id => s_base.subvencion.id
              )
              nuevo_presupuesto_est = presupuesto_est.monto.to_i - b.asignado.to_i
              presupuesto_est.update(:monto => nuevo_presupuesto_est)

              historial = HistorialPresupuesto.create(
                :descripcion => "Modificacion Liq de Subvencion ID #{@ant.id} S. Base Cod: #{s_base.codigo}",
                :entrada => "0",
                :salida => "#{b.asignado}",
                :total => "#{presupuesto_est.monto}",
                :subvencion_id => s_base.subvencion.id,
                :periodo_id => session[:periodo].id, 
                :establecimiento_id => params[:liq][:establecimiento_id].to_i,
                :usuario_id => @usuario_actual.id
              )
              #daem
              presupuesto_daem = PresupuestoSubvencion.first(
                :periodo_id => session[:periodo].id,
                :establecimiento_id => 1,
                :subvencion_id => s_base.subvencion_id
                )
              porcentaje = (b.porcentaje.to_i / 100)
              nuevo_monto = (presupuesto_sep.monto.to_i * porcentaje)
              nuevo_presupuesto_daem = presupuesto_daem.monto.to_i - nuevo_monto
              presupuesto_daem.update :monto => nuevo_presupuesto_daem.to_i
              historial_daem = HistorialPresupuesto.create(
                :descripcion => "Modificacion Liq de Subvencion ID #{@ant.id} S. Base Cod: #{s_base.codigo}",
                :entrada => "0",
                :salida => "#{nuevo_presupuesto_daem.to_i}",
                :total => "#{presupuesto_daem.monto}",
                :subvencion_id => s_base.subvencion.id,
                :periodo_id => session[:periodo].id, 
                :establecimiento_id => 1,
                :usuario_id => @usuario_actual.id
              )
            end  

            ##CREEARRRR
             #NUEVA!!!!!
            @ant.update params[:liq]
            @ant.usuario_id = @usuario_actual.id
            base_ant = LiqBase.all(:liquidacion_subvencion_id => @ant.id)
            if !base_ant.blank?
              base_ant.destroy
            end
            if !params[:base].blank?
              puts "base"
              params[:base].each do |p|
                nueva = LiqBase.create(
                  :monto => p[:monto],
                  :porcentaje => p[:porcentaje],
                  :asignado => p[:asignado],
                  :subvencion_base_id => p[:subvencion_base_id].to_i,
                  :liquidacion_subvencion_id => @ant.id
                ) 
              end
            end
            des_ant = LiqDescuento.all(:liquidacion_subvencion_id => @ant.id)
            if !des_ant.blank?
              des_ant.destroy
            end
            if !params[:descuento].blank?
               puts "descuento"
               params[:descuento].each do |p|
                nueva = LiqDescuento.create(
                  :descripcion => p[:descripcion],
                  :monto => p[:monto],
                  :liquidacion_subvencion_id => @ant.id
                )
               end
            end
            ret_ant = LiqRetencion.all(:liquidacion_subvencion_id => @ant.id)
            if !ret_ant.blank?
              ret_ant.destroy
            end
            if !params[:retencion].blank?
              puts "retencion"
              params[:retencion].each do |p|
                nueva = LiqRetencion.create(
                  :descripcion => p[:descripcion],
                  :monto => p[:monto],
                  :liquidacion_subvencion_id => @ant.id
                )
               end
            end
            bon_ant = LiqBono.all(:liquidacion_subvencion_id => @ant.id)
            if !bon_ant.blank?
              bon_ant.destroy
            end
            if !params[:bono].blank?
              puts "bono"
              params[:bono].each do |p|
                nueva = LiqBono.create(
                  :descripcion => p[:descripcion],
                  :monto => p[:monto],
                  :liquidacion_subvencion_id => @ant.id
                )
               end
            end

            

            
            
            params[:base].each do |b|
              s_base = SubvencionBase.get b[:subvencion_base_id].to_i
              presupuesto_est = PresupuestoSubvencion.first(
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => params[:liq][:establecimiento_id],
              :subvencion_id => s_base.subvencion.id
              )
              nuevo_presupuesto_est = presupuesto_est.monto.to_i + b[:asignado].to_i
              presupuesto_est.update(:monto => nuevo_presupuesto_est)

              historial = HistorialPresupuesto.create(
                :descripcion => "Modificacion Liq de Subvencion ID #{@ant.id} S. Base Cod: #{s_base.codigo}",
                :entrada => "#{b[:asignado]}",
                :salida => "0",
                :total => "#{presupuesto_est.monto}",
                :subvencion_id => s_base.subvencion.id,
                :periodo_id => session[:periodo].id, 
                :establecimiento_id => params[:liq][:establecimiento_id].to_i,
                :usuario_id => @usuario_actual.id
              )
              #daem
              presupuesto_daem = PresupuestoSubvencion.first(
                :periodo_id => session[:periodo].id,
                :establecimiento_id => 1,
                :subvencion_id => s_base.subvencion_id
                )
              porcentaje = b[:porcentaje].to_i / 100
              nuevo_presupuesto_daem = presupuesto_sep.monto.to_i * porcentaje
              presupuesto_daem.update :monto => nuevo_presupuesto_daem.to_i
              historial_daem = HistorialPresupuesto.create(
                :descripcion => "Modificacion Liq de Subvencion ID #{@ant.id} S. Base Cod: #{s_base.codigo}",
                :entrada => "#{nuevo_presupuesto_daem.to_i}",
                :salida => "0",
                :total => "#{presupuesto_daem.monto}",
                :subvencion_id => s_base.subvencion.id,
                :periodo_id => session[:periodo].id, 
                :establecimiento_id => 1,
                :usuario_id => @usuario_actual.id
              )
            
              ###
              
              
            
          end
      end



    end


    respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Guardado la Liquidacion Exitosamente."
                redirect_to liquidacion_path 
            }
            format.json {
                render :json => {
                    "estado" => @estado,
                    "id" => @ant.id                
                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect} #{@ant.inspect}"
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to liquidacion_path
              }
              format.json {
                render :json => {
                    "estado" => "Ha ocurrido un error #{e.message}"
                }
            }
        end
  end

  def ver_liquidacion
    @liq = LiquidacionSubvencion.get params[:id]
  end

  def get_liquidacion_est
    @liq = LiquidacionSubvencion.all(:establecimiento_id => params[:id].to_i, :periodo_id => session[:periodo].id, :es_nula => false, :order => :mes)

    render :json => @liq
  end

  def get_porcentaje_base
    @por = SubvencionBase.get params[:id]
    render :json => @por
  end

  def crear_liquidacion

    LiquidacionSubvencion.transaction do
      @estado = "Exito"
      obt = LiquidacionSubvencion.first(
        :periodo_id => session[:periodo].id, 
        :establecimiento_id => params[:liq][:establecimiento_id],
        :mes => params[:liq][:mes],
        :es_nula => false)
      if obt.blank?
        @liq = LiquidacionSubvencion.new params[:liq]
        @liq.periodo_id = session[:periodo].id
        @liq.usuario_id = @usuario_actual.id
        @liq.anio = session[:periodo].anio
        if !params[:base].blank?
          puts "base"
          @liq.liq_bases = params[:base]
        end
        if !params[:descuento].blank?
           puts "descuento"
          @liq.liq_descuentos = params[:descuento]
        end
        if !params[:retencion].blank?
          puts "retencion"
          @liq.liq_retencions = params[:retencion]
        end
        if !params[:bono].blank?
          puts "bono"
          @liq.liq_bonos = params[:bono]
        end
        @liq.save
        sep = Subvencion.first(
          :es_vigente => true,
          :es_sep => true)
        presupuesto_sep = PresupuestoSubvencion.first(
        :periodo_id => session[:periodo].id, 
        :establecimiento_id => params[:liq][:establecimiento_id],
        :subvencion_id => sep.id
        )
        nuevo_presupuesto_sep = presupuesto_sep.monto.to_i + @liq.sep_est.to_i
        presupuesto_sep.update :monto => nuevo_presupuesto_sep

        historial = HistorialPresupuesto.create(
          :descripcion => "Ingreso Liq de Subvencion ID #{@liq.id} Subv. SEP",
          :entrada => "#{@liq.sep_est}",
          :salida => "0",
          :total => "#{presupuesto_sep.monto}",
          :subvencion_id => sep.id,
          :periodo_id => session[:periodo].id, 
          :establecimiento_id => params[:liq][:establecimiento_id].to_i,
          :usuario_id => @usuario_actual.id
        )

        #Daem SEP
        presupuesto_sep_daem = PresupuestoSubvencion.first(
          :periodo_id => session[:periodo].id, 
          :establecimiento_id => 1,
          :subvencion_id => sep.id
        )
        nuevo_presupuesto_sep_daem = presupuesto_sep_daem.monto.to_i + @liq.sep_daem.to_i
        presupuesto_sep_daem.update :monto => nuevo_presupuesto_sep_daem
        historial_daem = HistorialPresupuesto.create(
          :descripcion => "Ingreso Liq de Subvencion ID #{@liq.id} Subv. SEP",
          :entrada => "#{@liq.sep_daem}",
          :salida => "0",
          :total => "#{presupuesto_sep_daem.monto}",
          :subvencion_id => sep.id,
          :periodo_id => session[:periodo].id, 
          :establecimiento_id => 1,
          :usuario_id => @usuario_actual.id
        )
        ##
        
        params[:base].each do |b|
          s_base = SubvencionBase.get b[:subvencion_base_id].to_i
          presupuesto_est = PresupuestoSubvencion.first(
          :periodo_id => session[:periodo].id, 
          :establecimiento_id => params[:liq][:establecimiento_id],
          :subvencion_id => s_base.subvencion.id
          )
          nuevo_presupuesto_est = presupuesto_est.monto.to_i + b[:asignado].to_i
          presupuesto_est.update(:monto => nuevo_presupuesto_est)

          historial = HistorialPresupuesto.create(
            :descripcion => "Ingreso Liq de Subvencion ID #{@liq.id} S. Base Cod: #{s_base.codigo}",
            :entrada => "#{b[:asignado]}",
            :salida => "0",
            :total => "#{presupuesto_est.monto}",
            :subvencion_id => s_base.subvencion.id,
            :periodo_id => session[:periodo].id, 
            :establecimiento_id => params[:liq][:establecimiento_id].to_i,
            :usuario_id => @usuario_actual.id
          )
          #daem
          presupuesto_daem = PresupuestoSubvencion.first(
            :periodo_id => session[:periodo].id,
            :establecimiento_id => 1,
            :subvencion_id => s_base.subvencion_id
            )
          porcentaje = b[:porcentaje].to_i / 100
          nuevo_monto = (b[:asignado].to_i * porcentaje)
          nuevo_presupuesto_daem = presupuesto_daem.monto.to_i + nuevo_monto
          presupuesto_daem.update :monto => nuevo_presupuesto_daem.to_i
          historial_daem = HistorialPresupuesto.create(
            :descripcion => "Ingreso Liq de Subvencion ID #{@liq.id} S. Base Cod: #{s_base.codigo}",
            :entrada => "#{nuevo_presupuesto_daem.to_i}",
            :salida => "0",
            :total => "#{presupuesto_daem.monto}",
            :subvencion_id => s_base.subvencion.id,
            :periodo_id => session[:periodo].id, 
            :establecimiento_id => 1,
            :usuario_id => @usuario_actual.id
          )
        
          ###

          
        
      end

      else
        @estado = "Existe"
      end



    end

    respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Guardado la Liquidacion Exitosamente."
                redirect_to liquidacion_path 
            }
            format.json {
                render :json => {
                    "estado" => @estado
                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect} #{@liq.inspect}"
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to liquidacion_path
              }
              format.json {
                render :json => {
                    "estado" => "Ha ocurrido un error #{e.message}",
                }
            }
        end


  end

  def subvenciones_orden
  	@subvenciones = Subvencion.all(:es_vigente => true)
  end


  def tipo_gasto_subvencion
	@s = Subvencion.get params[:id].to_i  	
  end

    def categoria_gasto_subvencion
	@s = Subvencion.get params[:id].to_i  	
  end

    def categoria_gasto_subvencion
	@t = TipoGasto.get params[:id].to_i  	
  end

  def subcategoria_gasto_subvencion
	@c = CategoriaGasto.get params[:id].to_i  	
  end

  def subvenciones_base
  @subvenciones = SubvencionBase.all(:order => :codigo, :es_vigente => true, :subvencion => {:es_vigente => true})
  end

  def guarda_subvencion_base
    sub = SubvencionBase.new params[:subvencion]
    sub.save
    flash[:notificacion] = "Se ha creado exitosamente"

    redirect_to subvenciones_base_path
  rescue Exception => e
    puts "ERROR:::: #{e.message}"
    flash[:error] = "revise los campos ingresados"
    redirect_to subvenciones_base_path
  end

  def get_subvencion_base
    sub = SubvencionBase.all(:codigo => params[:id].to_i, :es_vigente => true, :subvencion => {:es_vigente => true}) 
    render :json => sub
  end


  def presupuesto_subvencion
    @presupuesto = PresupuestoSubvencion.all(:establecimiento_id => 5)
  end

  def actualiza_presupuesto_subvencion
    params[:presupuesto].each do |p|
      pre = PresupuestoSubvencion.get p[:id].to_i
      monto = p[:monto].gsub('.', '')
      pre.update :monto => monto
    end

    flash[:notificacion] = "Se ha actualizado los montos exitosamente"
    redirect_to presupuesto_subvencion_path
  end
end
