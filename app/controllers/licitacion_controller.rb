#encoding: utf-8
class LicitacionController < ApplicationController

  def gestionar
  	@orden = Orden.get(params[:id])
    pos_actual = @orden.posicion.to_i + 1
    posicion = ConductoRegular.first(:posicion => pos_actual)
    @dpto = ""
    if !posicion.blank?
      @dpto = "a #{posicion.departamento.nombre}"
    end
  end

   	def enviar

    	Orden.transaction do
			@orden = Orden.get params[:id].to_i

			if !params[:doc_guardar].blank?
				params[:doc_guardar].each do |d|
				  doc = Documento.get d[:id].to_i
				  doc.update(:orden_id => @orden.id)
				end
			end
			if !params[:doc_borrar].blank?
				params[:doc_borrar].each do |d|
				  doc = Documento.get d[:id].to_i
				  doc.destroy
				end
			end

			puts "Orden: #{@orden.id}".yellow
			compra_lic_ant = CompraLicitacion.all(:adquisicion => {:orden_id => @orden.id})
			compra_lic_ant.destroy
			lic_ant = Licitacion.all(:orden_id => @orden.id)
			if lic_ant.destroy
				puts "LICITACIONES DESTRUIDAS".red
			else
				puts "NOOO".green
			end
			ad_ant = Adquisicion.all(:orden_id => @orden.id)
			ad_ant.destroy

			if !params[:licitacion].blank?
				params[:licitacion].each do |l|
					puts "#{l}".yellow
					lic = Licitacion.new l
					lic.orden_id = @orden.id
					lic.save
					puts "Lic ID: #{lic.id}".yellow
					if lic.estado == "Adjudicada"

			           	busca_oc = Adquisicion.all(:orden_id => params[:id], :orden_compra => params[:compra][:orden_compra], :eliminada => false, :es_nula => false)
					      if not busca_oc.blank?
					      	puts "#{busca_oc.inspect}".blue
					        raise "La OC ya ha sido ingresada"
					      end
			            ad = Adquisicion.new params[:compra]
			            ad.orden_id = @orden.id
			            ad.save   
			            subvencion_orden = @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id
			            est_orden = @orden.departamento.establecimiento_id
			            saldo = saldo_subvencion(est_orden, subvencion_orden)
			            historial = HistorialPresupuesto.create(
			              :descripcion => "Adquisicion OPI #{@orden.id}, OC: #{ad.orden_compra}",
			              :entrada => "0",
			              :salida => "#{ad.monto}",
			              :total => saldo,
			              :subvencion_id => subvencion_orden,
			              :periodo_id => session[:periodo].id, 
			              :establecimiento_id => est_orden,
			              :usuario_id => @usuario_actual.id
			            )          

			            documento = OrdenCompraDoc.new params[:doc]
			            documento.adquisicion_id = ad.id
			            documento.save
			            compra_lic = CompraLicitacion.create(
						        	:licitacion_id => lic.id,
						        	:adquisicion_id => ad.id
						        )
=begin
						params[:compra].each do |c|
						    puts "SI #{c[:mercado_publico]} == #{l[:cod]}".yellow
						    if c[:mercado_publico] == l[:cod]
						    	puts "Entra a compra".yellow
						        ad = Adquisicion.create(
						        	:orden_compra => c[:orden_compra],
						        	:monto => c[:monto],
						        	:fecha => c[:fecha],
						        	:cod_licitacion => c[:mercado_publico],
						        	:fue_licitacion => true,
						        	:orden_id => @orden.id
						        )
						        compra_lic = CompraLicitacion.create(
						        	:licitacion_id => lic.id,
						        	:adquisicion_id => ad.id
						        )
						       # @orden.update(:estado => "Comprada", :lista_recepcion => true, :en_proceso => false)
						    end
						end
=end
					end
				end
			end


			licitaciones = 0
			puts "Adjudicadas = #{licitaciones}".yellow
			if !params[:licitacion].blank?
				params[:licitacion].each do |l|
					if l[:estado] == "Adjudicada"
						licitaciones += 1
					end
				end
				puts "Adjudicadas = #{licitaciones}".yellow
				puts "SI Cantidad lic #{params[:licitacion].size} == adjudicas: #{licitaciones}".yellow
			end
			
			
			if !@orden.licitacions.blank?
				if @orden.licitacions.size == licitaciones
					movimiento = Movimiento.new(
					:descripcion => "Licitacion Terminada",
					:estado => "Comprada",
					:comentario => "-",
					:departamento_id => @usuario_actual.departamento_id,
					:orden_id => @orden.id,
					:usuario_id => @usuario_actual.id
					)
					movimiento.save
					@orden.update(:estado => "Comprada", :es_editable => false, :es_entrante => false, :lista_recepcion => true, :en_proceso => false)
				elsif @orden.licitacions.size != licitaciones and @orden.licitacions.size > 0
					movimiento = Movimiento.new(
					:descripcion => "Licitación guardada",
					:estado => "En Licitacion",
					:comentario => "-",
					:departamento_id => @usuario_actual.departamento_id,
					:orden_id => @orden.id,
					:usuario_id => @usuario_actual.id
					)
					movimiento.save
				end
				if @orden.licitacions(:estado => "Desierta").size > 0
					if @orden.licitacions(:estado => "Desierta").size + licitaciones == @orden.licitacions.size
						@orden.update(:estado => "Conducto Regular", :es_editable => false, :es_entrante => true, :lista_recepcion => false)
		      			movimiento = Movimiento.new(
						:descripcion => "Enviada a Adquisiciones por licitacion desierta",
		            	:estado => "Adquisiciones",
						:comentario => "-",
						:departamento_id => @usuario_actual.departamento_id,
						:orden_id => @orden.id,
						:usuario_id => @usuario_actual.id
						)
						movimiento.save
					end
				end
			end
	

			if not @orden.adquisicions.blank?
      			@orden.update(:lista_recepcion => true)
      		else
      			@orden.update(:lista_recepcion => false)
      		end

      		
		end
    
	    flash[:notificacion] = "Se ha Guardado la orden Exitosamente."
	    redirect_to ordenes_licitacion_path
	rescue Exception => e
		puts "ERROR:::: #{e.message.inspect}"
		flash[:error] = "Verifique los datos ingresados"
		redirect_to gestionar_licitacion_path params[:id]   
	end


end
