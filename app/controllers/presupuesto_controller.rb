#encoding: utf-8
class PresupuestoController < ApplicationController

	
	
	def establecimiento

		@e = Establecimiento.get params[:id]
		@tabla = []

		subvenciones = Subvencion.all(:es_vigente => true)

		subvenciones.each do |s|
			pre_sub_ext = PresupuestoSubvencion.all(
		      :periodo_id => session[:periodo].id, 
		      :establecimiento_id => @e.id, 
		      :subvencion_id => s.id)
			if pre_sub_ext.present?
				pagado = pagado_subvencion(@e.id, s.id)
	    		disponible = disponible_subvencion(@e.id, s.id)
	    		presupuesto = presupuesto_sub(@e.id, s.id)
	    		en_gestion = en_gestion_subvencion(@e.id, s.id) 
				@tabla << [
					"#{s.nombre}",
					"#{pagado}",
					"#{presupuesto}",
					"#{en_gestion}",
					"#{disponible}",
					"#{s.id}",
				]
			end
		end

	end

	def movimientos

		@e = Establecimiento.get params[:id]
		@s = Subvencion.get params[:sub]
		@historial = HistorialPresupuesto.all(
			:periodo_id => session[:periodo].id,
			:establecimiento_id => @e.id,
			:subvencion_id => @s.id
		)
		@pagado = pagado_subvencion(@e.id, @s.id)
    	@disponible = disponible_subvencion(@e.id, @s.id)
    	@presupuesto = presupuesto_sub(@e.id, @s.id)
    	@en_gestion = en_gestion_subvencion(@e.id, @s.id) 
    	@saldo = saldo_subvencion(@e.id, @s.id) 
		
		@ordenes_en_proceso = Orden.all(
			:es_nula => false,
			:en_proceso => true,
			:periodo_id => session[:periodo].id,
			:departamento => {
          		:establecimiento_id => @e.id
		        },
	        :subcategoria_gasto => {
	          :categoria_gasto => {
	            :tipo_gasto => {
	              :subvencion_id => @s.id
	            }
	          }
	        }
			)

		@ordenes_compradas = Adquisicion.all(
	      :es_nula => false,
	      :eliminada => false,
	      :orden => {
	        :es_nula => false,
	        :periodo_id => session[:periodo].id,
	        :departamento => {
	          :establecimiento_id => @e.id
	        },
	        :subcategoria_gasto => {
	          :categoria_gasto => {
	            :tipo_gasto => {
	              :subvencion_id => @s.id
	            }
	          }
	        }
	      }
	    )
	end
end