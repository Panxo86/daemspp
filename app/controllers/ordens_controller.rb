#encoding: utf-8
include ActionView::Helpers::NumberHelper
class OrdensController < ApplicationController

  skip_before_filter :entrantes_count, :only => [:imprimible]

  def create

    Orden.transaction do
      @orden = Orden.new(params[:orden])
      @orden.usuario_id = @usuario_actual.id
      @orden.cod = "#"
      @orden.departamento_id = @usuario_actual.departamento.id
      @orden.estado = "En digitacion"
      @orden.periodo_id = session[:periodo].id
      @orden.posicion = "0"

      if !params[:articulo][0][:cantidad].blank?
        @orden.bien_servicios = params[:articulo]
      end
      @orden.save

      #firmas imprimible
      firmas = Firma.new
      firmas.orden_id = @orden.id
      firmas.save

      #docuentos adjuntos   
      if !params[:doc_guardar].blank?
        params[:doc_guardar].each do |d|
          doc = Documento.get d[:id].to_i
          doc.update(:orden_id => @orden.id)
        end
      end
      if !params[:doc_borrar].blank?
        params[:doc_borrar].each do |d|
          doc = Documento.get d[:id].to_i
          doc.destroy
        end
      end
      #fin doc adjuntos

      #obtener direccion del establecimiento de la opi
      direccion = Departamento.first(:establecimiento_id => @usuario_actual.departamento.establecimiento_id, :es_direccion => true)
      
      #si es que es el director emitiendo opi
      if @usuario_actual.departamento_id == direccion.id

        ubicacion = Ubicacion.new(
          :departamento_id => posicion.departamento_id.to_i,
          :orden_id => @orden.id
        )
        ubicacion.save

        

        if @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.es_sep
            posicion = ConductoRegularSep.first(:posicion => 1)
          elsif @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id == 11
            posicion = ConductoRegularJunji.first(:posicion => 1)
          else
            posicion = ConductoRegular.first(:posicion => 1)
          end

        
        
        movimiento = Movimiento.new(
          :descripcion => "Enviada por Director Establecimiento",
          :estado => "En proceso",
          :comentario => "En proceso",
          :departamento_id => @usuario_actual.departamento_id,
          :orden_id => @orden.id,
          :usuario_id => @usuario_actual.id
        )
        movimiento.save
        @orden.update(:estado => "En proceso", :posicion => "1")

        firma = Firma.all(:orden_id => @orden.id).first
        firma.update :director_est => true
      
      #else si es otro dpto emitiendo (por lo q se debe enviar a direccion para validar)
      else
        ubicacion = Ubicacion.new(
          :departamento_id => direccion.id.to_i,
          :orden_id => @orden.id
        )
        ubicacion.save
        movimiento = Movimiento.new(
          :descripcion => "En espera de validacion por director establecimiento",
          :estado => "Digitada",
          :comentario => "Digitada",
          :departamento_id => @usuario_actual.departamento_id,
          :orden_id => @orden.id,
          :usuario_id => @usuario_actual.id
        )
        movimiento.save
      end

      if !params[:accion_id].blank?
        accion_orden = OrdenTieneAccion.new(
          :orden_id => @orden.id,
          :accion_id => params[:accion_id]
        )
        accion_orden.save
      end
    end

    respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Guardado la orden Exitosamente."
                redirect_to nueva_orden_path 
            }
            format.json {
                render :json => {
                    "estado" => "Exito",
                    :orden => @orden
                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to nueva_orden_path
              }
              format.json {
                render :json => {
                    "estado" => "Ha ocurrido un error #{e.message}"
                }
            }
        end
  end



  def actualizar
    Orden.transaction do
      @orden = Orden.get(params[:id])
      

      if !params[:orden][:titulo].blank?
        @orden.update(:titulo => params[:orden][:titulo])
      else
        raise "El titulo no puede estar vacio"
      end
      if !params[:orden][:justificacion].blank?
        @orden.update(:justificacion => params[:orden][:justificacion])
      else
        raise "La justificacion no puede estar vacia"
      end

      articulos = BienServicio.all(:orden_id => @orden.id)
      articulos.destroy
      params[:articulo].each do |a|
        if !a[:cantidad].blank? and !a[:descripcion].blank?
          bien = BienServicio.create(
            :descripcion => a[:descripcion],
            :cantidad => a[:cantidad],
            :valor => a[:valor],
            :total => a[:total],
            :orden_id => @orden.id
          ) 
        else
          raise "No existen articulos en la OPI"
        end
      end
      if !params[:orden][:subcategoria_gasto_id].blank?
        @orden.update(:subcategoria_gasto_id => params[:orden][:subcategoria_gasto_id])
      end
      if @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.es_sep
        puts "ES SEEEEEP"
        if !params[:accion_id_a].blank?
          accion = OrdenTieneAccion.first(:orden_id => @orden.id)
          accion.update(:accion_id => params[:accion_id_a])
        end
      end
    end

    respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Guardado la orden Exitosamente."
                redirect_to nueva_orden_path 
            }
            format.json {
                render :json => {
                    "estado" => "Exito",
                    :orden => @orden
                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to nueva_orden_path
              }
              format.json {
                render :json => {
                    "estado" => "Ha ocurrido un error: #{e.message}"
                }
            }
        end
    
  end



  def miles num
        if !num.blank?
            number_to_currency(num, :precision => (num.round == num) ? 0 : 2, :delimiter => ".", :unit => "")
        end
    end

  def eliminar
    orden = Orden.get params[:id]
    orden.update :es_nula => true
    flash[:notificacion] = "Se ha eliminado la orden ID: #{params[:id]} Exitosamente"
    redirect_to :back
  rescue
    flash[:error] = "Ha ocurrido un error eliminado la orden"
    redirect_to :back
  end

  def imprimible

    id = params[:id]
    @opi = Orden.get id
    @solicitado = 0
    @articulos = []
    @articulos << [
        "Cantidad",
        "Descripción",
        "Valor",
        "Total"
      ] 
    @opi.bien_servicios.each do |a, i|
      @solicitado += a.total
      @articulos << [
        "#{miles(a.cantidad)}",
        "#{a.descripcion}",
        "$ #{miles(a.valor)}",
        "$ #{miles(a.total)}",
      ] 
    end

    @movimientos = []
    @movimientos << [
        "Fecha",
        "Departamento",
        "Usuario",
        "Estado",   
        "Comentario"
      ] 

    @opi.movimientos.each do |m|
      @movimientos << [
        "#{m.created_at.strftime('%d/%m/%Y')}",
        "#{m.departamento.nombre}",
        "#{m.usuario.nombre} #{m.usuario.apaterno}",
        "#{m.estado}",
        "#{m.comentario}"
      ] 
    end
    @director = Usuario.get @opi.director.to_i
    @orden_compra = "-"
    @fecha_termino = "-"
    @monto_asignado = "-"
    @cuenta_bancaria = "-"
    @idchilecompra = "-"
    @fecha_licitacion = "-"
    @financiamiento = "-"
    @preobligacion = "-"


  end

  def licitacion
    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :estado => "En licitacion")
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ordens }
    end
  end

  def compradas
    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :estado => "Comprada")
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ordens }
    end
  end

  def index
    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :es_entrante => true, :ubicacion => {:departamento_id => @usuario_actual.departamento_id}, :order => :updated_at)
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @ordens }
    end
  end

  def new

  end

  def show
    @orden = Orden.get(params[:id])
  end

  def gestionadas
    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :movimientos => {:departamento_id => @usuario_actual.departamento.id}, :order => :id.desc)
  end

  def gestionar
    @orden = Orden.get(params[:id])
    pos_actual = @orden.posicion.to_i + 1
    if @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.es_sep
      posicion = ConductoRegularSep.first(:posicion => pos_actual)
    elsif @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id == 11
      posicion = ConductoRegularJunji.first(:posicion => pos_actual)
    else
      posicion = ConductoRegular.first(:posicion => pos_actual)
    end
    
    
    @dpto = ""
    if !posicion.blank?
      @dpto = "a #{posicion.departamento.nombre}"
    else
      if @orden.estado == "Espera decreto" || @orden.estado == "En consulta"
        @dpto = "1"
      end
    end
  end

  def enviadas
    @ordenes = Orden.all(:periodo_id => session[:periodo].id, :es_nula => false, :departamento_id => @usuario_actual.departamento.id, :order => :id.desc)
  end



  def enviar

    Orden.transaction do
      @orden = Orden.get params[:id].to_i
      firma = Firma.all(:orden_id => @orden.id).first
      if !params[:doc_guardar].blank?
        params[:doc_guardar].each do |d|
          doc = Documento.get d[:id].to_i
          doc.update(:orden_id => @orden.id)
        end
      end
      if !params[:doc_borrar].blank?
        params[:doc_borrar].each do |d|
          doc = Documento.get d[:id].to_i
          doc.destroy
        end
      end
      puts "FIRMA #{firma.inspect}".green
      firma.update :director_est => true
      if @orden.estado == "En proceso" || @orden.estado == "En digitacion" || @orden.estado == "Espera decreto"
        
        if !params[:tesoreria_fin].blank?
          if params[:tesoreria_fin] == "finalizar"
            @orden.update(:es_entrante => false, :estado => "Aprobada")
            movimiento = Movimiento.new(
              :descripcion => "OPI Finalizada en Tesoreria",
              :estado => "Aprobada",
              :comentario => params[:comentario],
              :departamento_id => @usuario_actual.departamento_id,
              :orden_id => @orden.id,
              :usuario_id => @usuario_actual.id
            )
            movimiento.save 
          end
        end

        if !params[:finanzas_fin].blank?
          firma.update :finanzas => true
          ubicaciones = Ubicacion.all(:orden_id => @orden.id)
            ubicaciones.destroy
          if params[:finanzas_fin] == "tesoreria"
            ubicacion = Ubicacion.new(
              :departamento_id => 30,
              :orden_id => @orden.id
            )
            mov = "Enviado a tesoreria"
          elsif params[:finanzas_fin] == "personal" 
            ubicacion = Ubicacion.new(
              :departamento_id => 29,
              :orden_id => @orden.id
            )
            mov = "Devuelta a Personal"
          elsif params[:finanzas_fin] == "Rechazar" 
            ubicacion = Ubicacion.new(
              :departamento_id => @orden.departamento_id,
              :orden_id => @orden.id
            )
            mov = "Rechazada"
          end

          ubicacion.save
          movimiento = Movimiento.new(
              :descripcion => mov,
              :estado => "Aprobada",
              :comentario => params[:comentario],
              :departamento_id => @usuario_actual.departamento_id,
              :orden_id => @orden.id,
              :usuario_id => @usuario_actual.id
            )
          movimiento.save       
        end

        if !params[:personal].blank?
            ubicaciones = Ubicacion.all(:orden_id => @orden.id)
            ubicaciones.destroy
            ubicacion = Ubicacion.new(
                :departamento_id => 2,
                :orden_id => @orden.id
              )
            ubicacion.save
            @orden.update :enviada_personal => true
            movimiento = Movimiento.new(
                :descripcion => "Enviada a finanzas, decreto adjuntado",
                :estado => "Aprobada",
                :comentario => params[:comentario],
                :departamento_id => @usuario_actual.departamento_id,
                :orden_id => @orden.id,
                :usuario_id => @usuario_actual.id
              )
              movimiento.save       
        end

        if !params[:enviar_fin].blank?

            ubicaciones = Ubicacion.all(:orden_id => @orden.id)
            ubicaciones.destroy
            

            estado_fin = ""
            if params[:enviar_fin] == "Personal"
              
              ubicacion = Ubicacion.new(
                :departamento_id => 29,
                :orden_id => @orden.id
              )
              mov = "Enviado a Personal"
              estado_fin = "Enviada"
            elsif params[:enviar_fin] == "Adquisiciones"
              estado_fin = "Enviada"
              firma.update :finanzas => true
              ubicacion = Ubicacion.new(
                :departamento_id => 1,
                :orden_id => @orden.id
              )
              mov = "Enviado a Adquisiciones"
            elsif params[:enviar_fin] == "Rechazar"
              estado_fin = "Rechazada"
              ubicacion = Ubicacion.new(
                :departamento_id => @orden.departamento_id,
                :orden_id => @orden.id
              )
              mov = "Rechazada"
              @orden.update :estado => "Rechazada"
            end
            ubicacion.save
              movimiento = Movimiento.new(
                :descripcion => mov,
                :estado => estado_fin,
                :comentario => params[:comentario],
                :departamento_id => @usuario_actual.departamento_id,
                :orden_id => @orden.id,
                :usuario_id => @usuario_actual.id
              )
              movimiento.save
        end
        if params[:accion] == "Aprobada"
          
           pos_actual = @orden.posicion.to_i + 1

       
           if @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.es_sep
              posicion = ConductoRegularSep.first(:posicion => pos_actual)
            
              if posicion.posicion == 1
                firma.update :director_est => true

              end
              if posicion.posicion > 1

                firma.update :coordinador_tec => true
              end

              if posicion.posicion > 2
                firma.update :asesor_sep => true
              end

              if posicion.posicion > 4
                firma.update :director_daem => true
              end

            elsif @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id == 11
              
              posicion = ConductoRegularJunji.first(:posicion => pos_actual)
              if posicion.posicion == 1
                firma.update :director_est => true

              end
              if posicion.posicion > 1

                firma.update :coordinador_tec => true
              end

              if posicion.posicion > 2
                firma.update :asesor_junji => true
              end

              if posicion.posicion > 4
                firma.update :director_daem => true
              end
                
            else
              posicion = ConductoRegular.first(:posicion => pos_actual)

              if posicion.posicion > 1
                firma.update :coordinador_tec => true
              end

              if posicion.posicion > 3
                firma.update :director_daem => true
              end

            end
           if !posicion.blank?
              ubicaciones = Ubicacion.all(:orden_id => @orden.id)
              ubicaciones.destroy
              ubicacion = Ubicacion.new(
                :departamento_id => posicion.departamento_id,
                :orden_id => @orden.id
              )
              ubicacion.save
              @orden.update(:posicion => "#{pos_actual}", :estado => "En proceso")
              #puts "UBICACION: #{ubicacion.departamento_id}"
           end

           if not params[:cuenta].blank?
            #puts "#{params[:cuenta].inspect}".yellow
              if @orden.cuenta_gasto_orden.blank?
                cuenta_gasto = CuentaGastoOrden.new params[:cuenta]
                cuenta_gasto.orden_id = @orden.id
                cuenta_gasto.cuenta_gasto_id = 1
               # puts "#{cuenta_gasto.inspect}".yellow
                cuenta_gasto.save
                mov_cuenta = "Cuenta gasto: #{cuenta_gasto.cuenta_gasto.numero} | #{cuenta_gasto.cuenta_gasto.descripcion}"
              else

                cuenta_gasto = CuentaGastoOrden.all(:orden_id => @orden.id).last
                if params[:cuenta][:cuenta] != cuenta_gasto.cuenta_gasto_id
                  cuenta_gasto.update params[:cuenta]
                  mov_cuenta = "Cuenta gasto Modificada a: #{cuenta_gasto.cuenta_gasto.numero} | #{cuenta_gasto.cuenta_gasto.descripcion}"  
                end
              end
              movimiento_c = Movimiento.new(
                  :descripcion => mov_cuenta,
                  :estado => "Aprobada",
                  :comentario => "Cuenta asignada",
                  :departamento_id => @usuario_actual.departamento_id,
                  :orden_id => @orden.id,
                  :usuario_id => @usuario_actual.id
                )
                movimiento_c.save
           end
          
          movimiento = Movimiento.new(
            :descripcion => "Sigue conducto regular",
            :estado => "Aprobada",
            :comentario => params[:comentario],
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save

        elsif params[:accion] == "Rechazada"
          ubi_actual = Ubicacion.all(:orden_id => @orden.id)
          ubi_actual.destroy
          ubicacion = Ubicacion.new(
                :departamento_id => @orden.departamento_id,
                :orden_id => @orden.id
              )
          ubicacion.save
          movimiento = Movimiento.new(
            :descripcion => "Rechazada por #{@usuario_actual.departamento.nombre}",
            :estado => "Rechazada",
            :comentario => params[:comentario],
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          @orden.update(:estado => "Rechazada", :es_editable => true)

      
        elsif params[:accion] == "Consulta"
          ubi_actual = Ubicacion.all(:orden_id => @orden.id)
          ubi_actual.destroy
          ubicacion = Ubicacion.new(
                :departamento_id => params[:destino].to_i,
                :orden_id => @orden.id
              )
          ubicacion.save
          movimiento = Movimiento.new(
            :descripcion => "Enviada a consulta",
            :estado => "En consulta",
            :comentario => params[:comentario],
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          @orden.update(:estado => "En consulta", :es_editable => true)

        elsif params[:accion] == "Licitar"
          puts "Licitar".blue
          movimiento = Movimiento.new(
            :descripcion => "Enviada a licitacion",
            :estado => "En Licitacion",
            :comentario => params[:comentario],
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          @orden.update(:estado => "En Licitacion", :es_editable => false, :es_entrante => false)
          params[:licitacion].each do |l|
            puts "#{l}".yellow
            lic = Licitacion.new l
            lic.orden_id = @orden.id
            lic.estado = "Publicada"
            lic.save
          end

        elsif params[:accion] == "Comprar"
          puts "Comprar".yellow
          if params[:btn_enviar].blank?
            ubi_actual = Ubicacion.all(:orden_id => @orden.id)
            ubi_actual.destroy
            ubicacion = Ubicacion.new(
                  :departamento_id => @orden.departamento_id,
                  :orden_id => @orden.id
                )
            ubicacion.save
            movimiento = Movimiento.new(
              :descripcion => "Aprobada compra directa",
              :estado => "Comprada",
              :comentario => params[:comentario],
              :departamento_id => @usuario_actual.departamento_id,
              :orden_id => @orden.id,
              :usuario_id => @usuario_actual.id
            )
            movimiento.save
            @orden.update(:estado => "Comprada", :es_editable => false, :es_entrante => false, :lista_recepcion => true)
            ad = Adquisicion.new params[:compra]
            ad.orden_id = @orden.id
            ad.save   
            subvencion_orden = @orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id
            est_orden = @orden.departamento.establecimiento_id
            saldo = saldo_subvencion(est_orden, subvencion_orden)
            historial = HistorialPresupuesto.create(
              :descripcion => "Adquisicion OPI #{@orden.id}, OC: #{ad.orden_compra}",
              :entrada => "0",
              :salida => "#{ad.monto}",
              :total => saldo,
              :subvencion_id => subvencion_orden,
              :periodo_id => session[:periodo].id, 
              :establecimiento_id => est_orden,
              :usuario_id => @usuario_actual.id
            )          

            documento = OrdenCompraDoc.new params[:doc]
            documento.adquisicion_id = ad.id
            documento.save
          else
            movimiento = Movimiento.new(
              :descripcion => "Agregada Orden de Compra",
              :estado => "En proceso",
              :comentario => params[:comentario],
              :departamento_id => @usuario_actual.departamento_id,
              :orden_id => @orden.id,
              :usuario_id => @usuario_actual.id
            )
            movimiento.save
            ad = Adquisicion.new params[:compra]
            ad.orden_id = @orden.id
            ad.save             
            documento = OrdenCompraDoc.new params[:doc]
            documento.adquisicion_id = ad.id
            documento.save

            flash[:notificacion] = "Se ha Guardado la orden Exitosamente."
            redirect_to gestionar_orden_path @orden.id

          end
         
      elsif params[:accion] == "Terminar"
        movimiento = Movimiento.new(
          :descripcion => "Terminada en adquisiciones",
          :estado => "Terminada",
          :comentario => params[:comentario],
          :departamento_id => @usuario_actual.departamento_id,
          :orden_id => @orden.id,
          :usuario_id => @usuario_actual.id
        )
        movimiento.save
        @orden.update(:estado => "Terminada", :es_editable => false, :es_entrante => false)

      elsif params[:accion] == "Espera decreto"
        movimiento = Movimiento.new(
          :descripcion => "Enviada a espera de decreto",
          :estado => "En proceso",
          :comentario => params[:comentario],
          :departamento_id => @usuario_actual.departamento_id,
          :orden_id => @orden.id,
          :usuario_id => @usuario_actual.id
        )
        movimiento.save
        @orden.update(:estado => "Espera decreto", :es_editable => false, :es_entrante => false)
      end
          
     elsif @orden.estado == "En consulta"
          ultimo_movimiento = Movimiento.last(:orden_id => @orden.id)
          ubi_actual = Ubicacion.all(:orden_id => @orden.id)
          ubi_actual.destroy
          ubicacion = Ubicacion.new(
                :departamento_id => ultimo_movimiento.departamento_id,
                :orden_id => @orden.id
              )
          ubicacion.save
          movimiento = Movimiento.new(
            :descripcion => "Respondida ",
            :estado => "Consultada",
            :comentario => params[:comentario],
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          @orden.update(:estado => "En proceso", :es_editable => false)

     elsif @orden.estado == "Rechazada"

        if params[:rechazada] == "Reenviar"
          ultimo_movimiento = Movimiento.last(:orden_id => @orden.id)
          ubi_actual = Ubicacion.all(:orden_id => @orden.id)
          ubi_actual.destroy
          ubicacion = Ubicacion.new(
                :departamento_id => ultimo_movimiento.departamento_id,
                :orden_id => @orden.id
              )
          ubicacion.save
          movimiento = Movimiento.new(
            :descripcion => "Reactivada ",
            :estado => "En proceso",
            :comentario => params[:comentario],
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          @orden.update(:estado => "En proceso", :es_editable => false)
        elsif params[:rechazada] == "Terminar"  
          @orden.update(:estado => "Terminada", :es_entrante => false)
          movimiento = Movimiento.new(
            :descripcion => "Terminada y cerrada por rechazo ",
            :estado => "Terminada",
            :comentario => params[:comentario],
            :departamento_id => @usuario_actual.departamento_id,
            :orden_id => @orden.id,
            :usuario_id => @usuario_actual.id
          )
          movimiento.save
          
        end

     end     
        
          
        
    end
    if params[:btn_enviar].blank?
      flash[:notificacion] = "Se ha Guardado la orden Exitosamente."
      redirect_to ordenes_entrantes_path 
    end

  rescue Exception => e
      puts "ERROR:::: #{e.message.inspect}".red
      puts "#{e.backtrace}".red
      flash[:error] = "Verifique los datos ingresados"
      redirect_to gestionar_orden_path params[:id]   
  end






end
