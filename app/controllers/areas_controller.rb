class AreasController < ApplicationController
  
  def index
   

    @area = Area.all(:es_vigente => true, :periodo_id => session[:periodo].id)

    @a = Area.get params[:id]


  end

 
  def new
   

   
  end

  
  def edit
    @a = Area.get params[:id]
  end


  def create
    area = Area.new(
        :nombre => params[:tx_nombre]
        )
    area.periodo_id = session[:periodo].id
      area.save
        flash[:notificacion] = "Se ha guardado con exito"
      redirect_to areas_path


      rescue 
            flash[:error] = "Ha ocurrido un error, revise campo ingresado"
            redirect_to nueva_area_path 
  end

  
  def update
    @area = Area.get params[:id]
    @area.update :nombre => params[:tx_nombre]
    @area.update :es_vigente => params[:vigente]

    flash[:notification] = "Se ha guardado con exito"

    redirect_to areas_path

  rescue 
    flash[:error] = "Ha ocurrido un error, revise campo ingresado"
    redirect_to editar_area_path params[:id] 

    
  end
  

  def borrar

      @a = Area.get params[:id]
      
  end



  def destroy
    @area = Area.get(params[:id])
    @area.destroy


    flash[:notificacion] = "Se ha borrado existosamente"
        redirect_to areas_path 

        rescue
        flash[:error] = "Error"
        redirect_to borrar_area_path params[:id]

  end  

  
  def show

    @a = Area.get params[:id]

    @area = Area.all
    

    @dimencion = Dimencion.all(:area_id => params[:id], :es_vigente => true)

   

   
  end
    
end
