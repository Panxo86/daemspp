class UsuariosController < ApplicationController
  skip_before_filter :entrantes_count, :only => [:guardar]
  def index

  end

  def nuevo
    
  end

  def get_usuario
    @usuario = Usuario.where(:email => params[:email]).first
    if @usuario
      e = @usuario.email
    else
      e = "no"
    end
    render :json => {
          "existe" => e
        }
  end

  def baja
    usuario = Usuario.get params[:id]
    usuario.update es_nulo: params[:baja]
    flash[:notificacion] = "Se ha guardado Exitosamente"
    redirect_to usuarios_path
  rescue => e
    flash[:error] = "Error : #{e.message}"
    redirect_to :back
  end

  def guardar

    Usuario.transaction do
      existe_rut = Usuario.all(:rut => params[:emp][:rut]).first
        if existe_rut.blank?
          existe_email = Usuario.all(:email => params[:emp][:email]).first
          if existe_email.blank?
            usuario = Usuario.new params[:emp]
            usuario.save
            modulos = Sistema.new 
            modulos.usuario_id = usuario.id
            if not params[:admin].blank?
              modulos.admin = params[:admin]
            else
              modulos.admin = 0
            end
            if not params[:finanzas].blank?
              modulos.finanzas = params[:finanzas]
            else
              modulos.finanzas = 0
            end
            if not params[:adquisiciones].blank?
              modulos.adquisiciones = params[:adquisiciones]
            else
               modulos.adquisiciones = 0
            end
            if not params[:tesoreria].blank?
              modulos.tesoreria = params[:tesoreria]
            else
              modulos.tesoreria = 0
            end
            if not params[:dotacion].blank?
              if params[:dotacion] == "Solicitar"
                modulos.dotacion = 0
              end

              if params[:dotacion] == "Autorizar"
                modulos.dotacion = 0
                modulos.dotacion_admin = 0
              end

              if params[:dotacion] == "No"
                modulos.dotacion = 0
                modulos.dotacion_admin = 0
              end
            else
                modulos.dotacion = 0
                modulos.dotacion_admin = 0
            end
            puts "#{modulos.inspect}".yellow
            modulos.save
          else
            if existe_email.es_nulo?
              baja_email = "(dado de baja)"
            else
              baja_email = ""
            end
            raise "El correo electronico ya esta siendo utilizado por #{existe_email.rut} #{existe_email.nombre} #{existe_email.apaterno} #{baja_email}"
          end
      else
        if existe_rut.es_nulo?
          raise "Usuario Rut #{params[:emp][:rut]} actualmente dado de baja, para habilitar debe darlo de alta nuevamente"
        else
          raise "El Usuario Rut #{params[:emp][:rut]} ya esta registrado"
        end
      end
    end

    respond_to do |format|
      format.html {       
        flash[:notificacion] = "Se ha Guardado el Usuario Exitosamente."
        redirect_to Usuarios_path 
      }
      format.json {
        render :json => {
          "estado" => "Exito"
        }
      }
    end

  rescue => e
    puts "ERROR:::: #{e.message.inspect}".red
    puts "ERROR:::: #{e.backtrace}".red
    respond_to do |format|
      format.html { 
        flash[:error] = "Verifique los datos ingresados"
        redirect_to nuevo_Usuario_path
      }
      format.json {
        render :json => {
          "estado" => "Ha ocurrido un error #{e.message} "
        }
      }
    end
  end

  def editar
    @usuario = Usuario.get params[:id]
  end
  def perfil
    @usuario = Usuario.get params[:id]
  end

  def actualizar_perfil
    Usuario.transaction do

      rut = params[:rut].delete(".")
      run = rut.split("-").first
      dv = rut.split("-").last
      usuario = Usuario.find params[:id]
      usuario.update params.require(:emp).permit(:nombres, :apaterno, :amaterno, :email, :cargo)
      usuario.rut = run
      usuario.dv = dv
      usuario.save
    
    end
    

    respond_to do |format|
      format.html {       
        flash[:notificacion] = "Usuario actualizado Exitosamente"
        redirect_to editar_Usuario_path params[:id]
      }
      format.json {
        render :json => {
          "estado" => "Exito"
        }
      }
    end

  rescue => e
    puts "ERROR:::: #{e.message.inspect}".red
    puts "ERROR:::: #{e.backtrace}".red
    respond_to do |format|
      format.html { 
        flash[:error] = "Verifique los datos ingresados"
        redirect_to editar_Usuario_path params[:id]
      }
      format.json {
        render :json => {
          "estado" => "Ha ocurrido un error #{e.message} "
        }
      }
    end
  end

  def actualizar



    Usuario.transaction do
      usuario = Usuario.get params[:id]
      usuario.update params[:emp]
      modulos_old = Sistema.all(:usuario_id => usuario.id)
      modulos_old.destroy
      modulos = Sistema.new 
      modulos.usuario_id = usuario.id
      if not params[:admin].blank?
        modulos.admin = params[:admin]
      end
      if not params[:finanzas].blank?
        modulos.finanzas = params[:finanzas]
      end
      if not params[:adquisiciones].blank?
        modulos.adquisiciones = params[:adquisiciones]
      end
      if not params[:tesoreria].blank?
        modulos.tesoreria = params[:tesoreria]
      end
      if not params[:dotacion].blank?
        if params[:dotacion] == "Solicitar"
          modulos.dotacion = 1
          modulos.dotacion_admin = 0
        end

        if params[:dotacion] == "Autorizar"
          modulos.dotacion = 1
          modulos.dotacion_admin = 1
        end

        if params[:dotacion] == "No"
          modulos.dotacion = 0
          modulos.dotacion_admin = 0
        end
      else
        modulos.dotacion = 0
        modulos.dotacion_admin = 0
      end
      puts "#{modulos.inspect}"
      modulos.save
    end

    respond_to do |format|
      format.html {       
        flash[:notificacion] = "Usuario actualizado Exitosamente"
        redirect_to editar_Usuario_path params[:id]
      }
      format.json {
        render :json => {
          "estado" => "Exito"
        }
      }
    end

  rescue => e
    puts "ERROR:::: #{e.message.inspect}".red
    puts "ERROR:::: #{e.backtrace}".red
    respond_to do |format|
      format.html { 
        flash[:error] = "Verifique los datos ingresados"
        redirect_to editar_Usuario_path params[:id]
      }
      format.json {
        render :json => {
          "estado" => "Ha ocurrido un error #{e.message} "
        }
      }
    end
  end

  def actualiza_password

    usuario = Usuario.get params[:id]
    if params[:admin].blank?
      usuario.update :password => params[:password]
    else
      if @usuario_actual.password == params[:password_actual]
       usuario.update :password => params[:password]
      else
        raise "Password actual erronea"
      end
    end
    flash[:notificacion] = "Password actualizada Exitosamente"
    redirect_to :back
  rescue => e
    flash[:error] = "Verifique los datos ingresados: #{e.message}"
    redirect_to :back
  end
end
