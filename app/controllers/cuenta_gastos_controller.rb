class CuentaGastosController < ApplicationController

	def index

	end

	def guardar
		
		cuenta = CuentaGasto.new params[:cuenta]
		cuenta.save

		flash[:notificacion] = "Se ha guardado la cuenta exitosamente"
		redirect_to cuentas_gastos_path

	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to cuentas_gastos_path
	end

	def eliminar
		
		cuenta = CuentaGasto.get params[:id]
		cuenta.update :es_nula => true

		flash[:notificacion] = "Se ha eliminado la cuenta exitosamente"
		redirect_to cuentas_gastos_path

	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to cuentas_gastos_path
	end

end