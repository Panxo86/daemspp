class SubcategoriaGastosController < ApplicationController

  def guarda_subcategoria_gasto
  cat = SubcategoriaGasto.new params[:cat]
    cat.save
    flash[:notificacion] = "Se ha creado exitosamente"

    redirect_to subcategoria_gasto_subvencion_path params[:cat][:categoria_gasto_id].to_i
  rescue
    flash[:error] = "revise los campos ingresados"
    redirect_to subcategoria_gasto_subvencion_path params[:cat][:categoria_gasto_id].to_i
  end

  def sl_subcategoria_gasto
    id = params[:id]
    datos = SubcategoriaGasto.all(:es_vigente => true, :categoria_gasto_id => id, :order => :nombre)
    array = "<option value=''>Seleccione Subcategoria de Gasto</option>"
    datos.each do |p|
    array += "<option value='#{p.id}'>#{p.nombre}</option>"
    end
    render :json => { :opciones => array }
  end
  def jeditable_nombre_subcat_gasto
    nuevo = params[:newvalue]
    id = params[:elementid].to_i
    cat = SubcategoriaGasto.get id
    cat.update :nombre => nuevo
    render :json => nuevo
  end

  def jeditable_codigo_subcat_gasto
    nuevo = params[:newvalue]
    id = params[:elementid].to_i
    cat = SubcategoriaGasto.get id
    cat.update :codigo => nuevo
    render :json => nuevo
  end
  def borrar_subcat_gasto
    cat = SubcategoriaGasto.get params[:id]
    cat.update :es_vigente => false
    flash[:notificacion] = "Se ha eliminado exitosamente"
    redirect_to :back
  rescue
    flash[:error] = "Ha ocurrido un error, intentelo nuevamente"
    redirect_to :back
  end

end
