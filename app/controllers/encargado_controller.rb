class EncargadoController < ApplicationController

	def asesores
		@funcionarios = Usuario.daem

		@vigentes = AsesorSubvencion.all(:es_vigente => true)
	end

	def guarda_asesor
		asesor = AsesorSubvencion.new params[:sub]

		#puts "#{asesor.inspect}".blue
		asesor.save

		flash[:notificacion] = "Guardado con exito"
		redirect_to :back

	rescue Exception => e
		flash[:error] = "#{e.message}"

		puts "#{e.backtrace}".red
		redirect_to :back
	end

	def borra_asesor
		asesor = AsesorSubvencion.get params[:id]
		asesor.update :es_vigente => false
	
		flash[:notificacion] = "Eliminado con exito"
		redirect_to :back

	rescue Exception => e
		flash[:error] = "#{e.message}"
		puts "#{e.backtrace}".red
		redirect_to :back
	end


	def encargados
		@funcionarios = Usuario.daem

		@vigentes = EncargadoSubvencion.all(:es_vigente => true)
	end

	def guarda_encargado
		encargado = EncargadoSubvencion.new params[:sub]

		#puts "#{encargado.inspect}".blue
		encargado.save

		flash[:notificacion] = "Guardado con exito"
		redirect_to :back

	rescue Exception => e
		flash[:error] = "#{e.message}"

		puts "#{e.backtrace}".red
		redirect_to :back
	end

	def borra_encargado
		encargado = EncargadoSubvencion.get params[:id]
		encargado.update :es_vigente => false
	
		flash[:notificacion] = "Eliminado con exito"
		redirect_to :back

	rescue Exception => e
		flash[:error] = "#{e.message}"
		puts "#{e.backtrace}".red
		redirect_to :back
	end
end
