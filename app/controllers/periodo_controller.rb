class PeriodoController < ApplicationController

  def index
  end

  def eliminar
    periodo = Periodo.get params[:id]
    periodo.update :es_obsoleto => true
    flash[:notification] = "Periodo eliminado Exitosamente!"
    redirect_to :back
  rescue
    flash[:error] = "Algo ocurrio mal, intentelo nuevamente"
    redirect_to :back
  end

  def create

  	Periodo.transaction do
      puts "Entre".red
	   	periodo = Periodo.new params[:periodo]
	   	if !params[:activar].blank?
	   		todos = Periodo.all
	   		todos.update :es_vigente => false
	   		periodo.es_vigente = true
	   	end
      
	   	periodo.save

      establecimientos = Establecimiento.all
      establecimientos.each do |e|
        subvenciones = Subvencion.all(:es_vigente => true)
        subvenciones.each do |s|
          presupuesto = PresupuestoSubvencion.create(
            :monto => "0",
            :establecimiento_id => e.id,
            :subvencion_id => s.id,
            :periodo_id => periodo.id
          )
        end
        
      end
	end
	flash[:notification] = "Periodo Ingresado Exitosamente!"
  	redirect_to periodo_path

  rescue Exception => e
  	flash[:error] = "Ha ocurrido un error al crear el periodo, intentelo nuevamente"
  	puts "ERROR CREANDO PERIODO::::::::::: #{e.message.inspect}"
  	redirect_to periodo_path
  end

  def activar
  		Periodo.transaction do
	  		todos = Periodo.all
		   	todos.update :es_vigente => false
		   	nuevo = Periodo.get params[:id]
		   	nuevo.update :es_vigente => true
		end
    redirect_to periodo_path
  end

  def cambiar
  	if !params[:periodo].blank?
  		periodo = Periodo.get params[:periodo]
      session[:periodo][:anio] = periodo.anio
      session[:periodo][:id] = periodo.id
  	end
  	redirect_to :back
  end


end
