class SuministroController < ApplicationController

	def index

		@ordenes = Orden.all(:es_nula => false, :es_suministro => true, :estado => "Enviada Suministro", :periodo_id => session[:periodo].id)
		
	end

	def guardar

		suministro = Suministro.new params[:sum]
		suministro.periodo_id = session[:periodo].id
		suministro.save

		flash[:notificacion] = "Se ha guardado el contrato exitosamente"
		redirect_to suministros_path
	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to nuevo_suministro_path
		
	end

	def ver
		@s = Suministro.get params[:id]
	end

	def ver_orden
		@orden = Orden.get params[:id]
	end

	def asignar_orden

		OrdenSuministro.transaction do 
			@orden = Orden.get params[:id]
			suministro = Suministro.get params[:asignar][:suministro_id]
			asignar = OrdenSuministro.new params[:asignar]
			asignar.orden_id = @orden.id
			asignar.save
			movimiento = Movimiento.new(
	          :descripcion => "OPI Asignada a contrato de Suministro",
	          :estado => "Asignada Suministro",
	          :comentario => "Contrato Suministro: #{suministro.titulo}",
	          :departamento_id => @usuario_actual.departamento_id,
	          :orden_id => @orden.id,
	          :usuario_id => @usuario_actual.id
	        )
	        movimiento.save
	        @orden.update(:estado => "Asignada Suministro")
	    end
	    flash[:notificacion] = "Se ha asignado la OPI exitosamente"
	    redirect_to suministros_path 
	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to ver_orden_suministro_path params[:id]
	end

	def quitar_orden
		OrdenSuministro.transaction do 
			@orden = Orden.get params[:id]
			asignar = OrdenSuministro.all(:orden_id => params[:id]).last
			asignar.destroy
			movimiento = Movimiento.new(
	          :descripcion => "OPI DESASIGNADA de contrato de Suministro",
	          :estado => "Desasignada Suministro",
	          :comentario => "Devuelta a Encargado Subvencion",
	          :departamento_id => @usuario_actual.departamento_id,
	          :orden_id => @orden.id,
	          :usuario_id => @usuario_actual.id
	        )
	        movimiento.save
	        @orden.update(:estado => "Conducto Regular", :en_proceso => true, :es_suministro => false)
	    end
	    flash[:notificacion] = "Se ha desasignado la OPI exitosamente"
	    redirect_to suministros_path 
	rescue Exception => e
		flash[:error] = "#{e.message}"
		redirect_to ver_suministro_path params[:id]
	end
end
