class PmeActividadController < ApplicationController


	def ver
    @est = Establecimiento.all(:hex_url => params[:est]).first
    @accion = PmeAccion.get params[:id]
    @dimension = @accion.pme_estrategia.pme_objetivo.pme_dimension_id
    @objetivo = @accion.pme_estrategia.pme_objetivo.id
 	@actividad = PmeActividad.get params[:act]

  end

  def nueva
    @est = Establecimiento.all(:hex_url => params[:est]).first
    @accion = PmeAccion.get params[:id]



  end

  def subir_pme_actividad_doc

  	documento = PmeActividadDoc.new params[:doc]
	documento.pme_actividad_id = params[:id]
	documento.departamento_id = @usuario_actual.departamento_id
	documento.usuario_id = @usuario_actual.id
	documento.save
  @actividad = PmeActividad.get params[:id]
  if @actividad.avance == "100"     
    @actividad.update terminado: true 
  end
	size = PmeActividadDoc.all(:pme_actividad_id => params[:id]).count + 1
	respond_to do |format|
   		format.html { 		
   			flash[:notificacion] = "exito."
			redirect_to :back
		}
   		format.json {
   			render :json => {
   				"estado" => "Exito",
   				"url" => documento.archivo.url,
   				"id" => documento.id,
   				"name" => documento.name,
   				"uploader" => "#{@usuario_actual.nombre} #{@usuario_actual.apaterno}",
   				"numero" => size,
   				"fecha" => documento.created_at.strftime("%d/%m/%Y %H:%M")

   			}
   		}
 	end
	rescue Exception => e
		puts "ERROR::::::: #{e.message} #{e.message.inspect}".red
		respond_to do |format|
	      format.html { 
	      	flash[:error] = "Verifique los datos ingresados"
	      	redirect_to :back
	      }
	      format.json {
   			render :json => {
   				"estado" => "Error #{e.message}"
   			}
   		}
	    end
  	
  end

  def actualiza
  		@actividad = PmeActividad.get params[:id]
		  @actividad.update params[:actividad]	

      if @actividad.avance == "100"   	
        if not @actividad.pme_actividad_docs.blank?
           @actividad.update terminado: true 
        end
      else
        @actividad.update terminado: false
      end
	

  	respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Guardado la actividad."
                redirect_to :back
            }
            format.json {
                render :json => {
                    "estado" => "Exito",
                    :id => @actividad.id

                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            puts "ERROR:::: #{e.backtrace}".red
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to :back
              }
              format.json {
                render :json => {
                    "estado" => "Error: #{e.message}"
                }
            }
        end
  end


  	def crear
		@actividad = PmeActividad.new params[:actividad]
		@actividad.pme_accion_id = params[:id]	  	
		@actividad.avance = "0"  	
        @actividad.desde = params[:fechas].split(" - ").first
        @actividad.hasta = params[:fechas].split(" - ").last
		@actividad.save




  	respond_to do |format|
            format.html {       
                flash[:notificacion] = "Se ha Guardado la orden Accion."
                redirect_to pme_nueva_accion_path 
            }
            format.json {
                render :json => {
                    "estado" => "Exito",
                    :id => @actividad.id

                }
            }
        end
        rescue Exception => e
            puts "ERROR:::: #{e.message.inspect}"
            puts "ERROR:::: #{e.backtrace}".red
            respond_to do |format|
              format.html { 
                flash[:error] = "Verifique los datos ingresados"
                redirect_to pme_nueva_accion_path
              }
              format.json {
                render :json => {
                    "estado" => "Error: #{e.message}"
                }
            }
        end
  end


end
