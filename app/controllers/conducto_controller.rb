#encoding: utf-8
class ConductoController < ApplicationController

	def index
		
	end

	def guardar

		Conducto.transaction do
			anterior = Conducto.all
			anterior.destroy
			params[:conducto].each do |c|
				conducto = Conducto.new c
				conducto.bien = true 
				puts "#{conducto.inspect}".blue
				conducto.save
			end
		end

		flash[:notificacion] = "El conducto regular se ha guardado, nuevas OPI generadas desde este momento utilizaran esta nueva ruta"
		redirect_to conducto_path
	rescue Exception => e
		flash[:error] = "No se ha guardado, #{e.message}"
		redirect_to conducto_path

	end
end
