	pdf.bounding_box [pdf.bounds.left, pdf.bounds.top ], :width  => pdf.bounds.width do
		pdf.image "#{Rails.root}/app/assets/images/daem.png", :position => :left, :width => 120,:indent_paragraphs => 5 

		pdf.draw_text "ORDEN DE PEDIDO INTERNO", :size => 12, :style => :bold, :at => [190, 30]
		pdf.draw_text "OPI N° #{@opi.id}", :size => 12, :style => :bold, :at => [450, 30]

		fecha = Date.today
		fecha = fecha.to_s.split('-')

		pdf.draw_text "DAEM", :size => 12, :style => :bold, :at => [260, 10]
	end
	
	pdf.move_down(30)
    data =  [["Titulo OPI: #{@opi.titulo}"]]
  pdf.table(data, :row_colors =>  ["e6e6e6", "e6e6e6"]) do
    cells.borders =  [] 
    columns(0).width =  520
        cells.size =  10
        row(0).borders      =  [:right] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
        columns(0).borders =  [:bottom]

  end
  pdf.move_down(10)

  if @opi.cuenta_gasto_orden.blank?
    cuenta = ""
    item = ""
    asignacion = ""
    subtitulo = ""

  else
    cuenta = @opi.cuenta_gasto_orden.cuenta
    item = @opi.cuenta_gasto_orden.item
    asignacion = @opi.cuenta_gasto_orden.asignacion
    subtitulo = @opi.cuenta_gasto_orden.subtitulo
  end

    data =  [  ["Solicitado:", "$ #{miles(@solicitado)}", "Fecha Inicio:", "#{@opi.created_at.strftime('%d/%m/%Y a las %I:%M%p')}"],
               
               ["Subvención:", "#{@opi.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.nombre}","Subcategoria Gasto:", "#{@opi.subcategoria_gasto.nombre}"], 
               ["Origen:", "#{@opi.departamento.establecimiento.nombre} - #{@opi.departamento.nombre}", "Emisor:", "#{@opi.usuario.nombre} #{@opi.usuario.apaterno}"],
               ["Periodo:", "#{@opi.periodo.anio}", "Estado:", "#{@opi.estado}"] ,
               ["Justificación:", "#{@opi.justificacion}", "", ""], 
               ["Cuenta Gasto:", "#{cuenta}", "Item", "#{item}"], 
               ["Subtitulo:", "#{subtitulo}", "Asignacion", "#{asignacion}"]
               ] 
    pdf.table(data)  do 
      cells.size =  8
      cells.borders =  []
      columns(0).font_style   =  :bold 
      columns(2).font_style   =  :bold 
      columns(0).width =  75
      columns(1).width =  170
      columns(2).width =  115
      columns(3).width =  160
    end
  

pdf.move_down(100)

pdf.define_grid(:columns => 2, :rows => 6, :gutter => 10)
x = 3
y = 0

  pdf.grid(3, 0).bounding_box do
    if not  @director.blank?
      if not @director.firma_usuario.blank?
        pdf.image "#{Rails.root}/public#{@director.firma_usuario.archivo.url}", :position => :center, :fit => [170, 85]
      else
        pdf.move_down 85
      end
      @nombre_director = "#{@director.nombre} #{@director.apaterno} "
    else
      pdf.move_down 85
      @nombre_director = ""
    end
    pdf.horizontal_line 100, 200, :at => 50
    pdf.move_down 5
    pdf.text "#{@nombre_director} ", :size => 8, :style => :bold, :align => :center
    pdf.text "Director Establecimiento", :size => 8, :style => :bold, :align => :center
 
  end

  @opi.rutas(:firma => true).each_with_index do |r, i|

    case i
      when 0
        x = 3
        y = 1
      when 1
        x = 3
        y = 2
      when 2
        x = 4
        y = 0
      when 3
        x = 4
        y = 1
      when 4
        x = 4
        y = 2
      when 5
        x = 5
        y = 0
      when 6
        x = 5
        y = 1
      when 7
         x = 5
        y = 2
    end

    pdf.grid(x, y).bounding_box do
      if r.aprobada?
        respon = "#{r.usuario.nombre} #{r.usuario.apaterno}"
        if not r.usuario.firma_usuario.blank?
          pdf.image "#{Rails.root}/public#{r.usuario.firma_usuario.archivo.url}", :position => :center, :fit => [170, 85]
        else
          pdf.move_down 85
        end
      else
        respon = ""
        pdf.move_down 85
      end
      pdf.move_down 5
      pdf.text respon, :size => 8, :style => :bold, :align => :center
      pdf.text "#{r.bandeja.nombre}", :size => 8, :style => :bold, :align => :center
   
    end
   
  end
  
  
 





pdf.start_new_page

  pdf.bounding_box [pdf.bounds.left, pdf.bounds.top - 25], :width  => pdf.bounds.width do
    pdf.image "#{Rails.root}/app/assets/images/daem.png", :position => :left, :width => 120,:indent_paragraphs => 5 

    pdf.draw_text "ORDEN DE PEDIDO INTERNO", :size => 12, :style => :bold, :at => [190, 30]
    pdf.draw_text "N° #{@opi.id}", :size => 12, :style => :bold, :at => [475, 30]

    fecha = Date.today
    fecha = fecha.to_s.split('-')

    pdf.draw_text "DAEM", :size => 12, :style => :bold, :at => [260, 10]
  end

  pdf.move_down(20)
  data =  [["Items solicitados"]]
  pdf.table(data, :row_colors =>  ["e6e6e6", "e6e6e6"]) do
    cells.borders =  [] 
    columns(0).width =  520
        cells.size =  10
        row(0).borders      =  [:right] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
        columns(0).borders =  [:bottom]

  end
  pdf.move_down(10)
  data = @articulos 
    pdf.table(data,  :row_colors =>  ["FFFFFF", "e6e6e6"]) do 
        cells.padding_left = 10 
        cells.borders =  [] 
        cells.size =  8


        row(0).borders      =  [:bottom] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
         
        columns(0..2).borders =  [:right] 
        columns(0).width =  60
        columns(1).width =  260
        columns(2).width =  100
        columns(3).width =  100

         
        row(0).columns(0..2).borders =  [:bottom,  :right] 
    end

    pdf.move_down(30)

pdf.start_new_page

  pdf.bounding_box [pdf.bounds.left, pdf.bounds.top - 25], :width  => pdf.bounds.width do
   

    pdf.draw_text "ORDEN DE PEDIDO INTERNO", :size => 12, :style => :bold, :at => [190, 30]
    pdf.draw_text "N° #{@opi.id}", :size => 12, :style => :bold, :at => [475, 30]

    fecha = Date.today
    fecha = fecha.to_s.split('-')

    pdf.draw_text "DAEM", :size => 12, :style => :bold, :at => [260, 10]
  end
    pdf.move_down(30)

  data =  [["Adquisición"]]
  pdf.table(data, :row_colors =>  ["e6e6e6", "e6e6e6"]) do
    cells.borders =  [] 
    columns(0).width =  520
        cells.size =  10
        row(0).borders      =  [:right] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
        columns(0).borders =  [:bottom]

  end


@opi.adquisicions.each_with_index do |a, x| 
  if a.fue_licitacion
    lic_titulo = "Licitación:"
    lic_cod = a.cod_licitacion
  else
    lic_titulo = ""
    lic_cod = ""
  end
  data =  [  ["Compra ##{x + 1}:", ""],
  ["Orden de Compra:", "#{a.orden_compra}"],
        
               ["Monto:", "$ #{miles(a.monto.to_i)}"],
               ["Fecha:", "#{a.fecha.strftime('%d/%m/%Y')}"] ,
               ["#{lic_titulo}", "#{lic_cod}"] 
               ] 
    pdf.table(data,  :column_widths => {0 => 120, 1 => 400})  do 
      cells.size =  8
      cells.borders =  []
      columns(0).font_style   =  :bold 
    end
pdf.move_down 10 
end
pdf.move_down 20 
  data =  [["Movimientos"]]
  pdf.table(data, :row_colors =>  ["e6e6e6", "e6e6e6"]) do
    cells.borders =  [] 
    columns(0).width =  520
        cells.size =  10
        row(0).borders      =  [:right] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
        columns(0).borders =  [:bottom]

  end
pdf.move_down 10 
data = @movimientos 
    pdf.table(data,  :row_colors =>  ["FFFFFF", "e6e6e6"]) do 
        cells.padding_left = 10 
        cells.borders =  [] 
        cells.size =  8


        row(0).borders      =  [:bottom] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
         
        columns(0..3).borders =  [:right] 
        columns(0).width =  65
        columns(1).width =  90
        columns(2).width =  130
        columns(3).width =  65
        columns(4).width =  170

         
        row(0).columns(0..3).borders =  [:bottom,  :right] 
    end






string = "Pagina <page> de <total>"
options = { :at => [pdf.bounds.right - 150, 0],
:width => 150,
:align => :center,
:size => 8,
:color => "000000" }
pdf.number_pages string, options





