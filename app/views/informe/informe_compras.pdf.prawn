	pdf = Prawn::Document.new(:page_size => "LETTER", :page_layout => :landscape)
    pdf.bounding_box [pdf.bounds.left, pdf.bounds.top ], :width  => pdf.bounds.width do
		pdf.image "#{Rails.root}/app/assets/images/daem.png", :position => :left, :width => 120,:indent_paragraphs => 5 

		pdf.draw_text "Informe de Compras ", :size => 12, :style => :bold, :at => [320, 25]
		pdf.draw_text "#{fecha(Time.now)}", :size => 10, :style => :bold, :at => [350, 05]

	end
	
	pdf.move_down(30)
	data =  [["Informe:"]]
	pdf.table(data, :row_colors =>  ["e6e6e6", "e6e6e6"]) do
		cells.borders =  [] 
		columns(0).width =  720
        cells.size =  10
        row(0).borders      =  [:right] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
        columns(0).borders =  [:bottom]

	end
	pdf.move_down(10)

	data = @informe	
    pdf.table(data,  :row_colors =>  ["FFFFFF", "e6e6e6"]) do 
        cells.padding_left = 10 
        cells.borders =  [] 
        cells.size =  8


        row(0).borders      =  [:bottom] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
         
        columns(0..7).borders =  [:right] 
        columns(0).width =  55
        columns(1).width =  102
        columns(2).width =  102
        columns(3).width =  72
        columns(4).width =  40
        columns(5).width =  72
        columns(6).width =  50
        columns(7).width =  72
        columns(8).width =  83
        columns(9).width =  72

         
        row(0).columns(0..7).borders =  [:bottom,  :right] 
    end
	
string = "Pagina <page> de <total>"
options = { :at => [pdf.bounds.right - 150, 0],
:width => 150,
:align => :center,
:size => 8,
:color => "000000" }
pdf.number_pages string, options





