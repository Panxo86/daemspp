	pdf.bounding_box [pdf.bounds.left, pdf.bounds.top ], :width  => pdf.bounds.width do
		pdf.image "#{Rails.root}/app/assets/images/daem.png", :position => :left, :width => 120,:indent_paragraphs => 5 

		pdf.draw_text "RECEPCIÓN DE BIENES ", :size => 12, :style => :bold, :at => [200, 30]
		pdf.draw_text "N° #{@recepcion.id}", :size => 12, :style => :bold, :at => [475, 30]
    pdf.draw_text "#{Time.now.strftime("%d/%m/%Y %H:%M")}", :size => 9, :at => [436, 10]
		fecha = Date.today
		fecha = fecha.to_s.split('-')

		pdf.draw_text "DAEM", :size => 12, :style => :bold, :at => [260, 10]
	end
	
	pdf.move_down(30)
	data =  [["Artículos:"]]
	pdf.table(data, :row_colors =>  ["e6e6e6", "e6e6e6"]) do
		cells.borders =  [] 
		columns(0).width =  520
        cells.size =  10
        row(0).borders      =  [:right] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
        columns(0).borders =  [:bottom]

	end
	pdf.move_down(10)

	data = @articulos	
    pdf.table(data,  :row_colors =>  ["FFFFFF", "e6e6e6"]) do 
        cells.padding_left = 10 
        cells.borders =  [] 
        cells.size =  8


        row(0).borders      =  [:bottom] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
         
        columns(0..4).borders =  [:right] 
        columns(0).width =  35
        columns(1).width =  121
        columns(2).width =  121
        columns(3).width =  121
        columns(4).width =  60
        columns(5).width =  60

         
        row(0).columns(0..4).borders =  [:bottom,  :right] 
    end

    pdf.move_down(10)

    data =  [  ["", "Subtotal:", "#{@subtotal}"],
               ["", "Descuento:", "#{@descuento}"], 
               ["", "Neto:", "#{@neto}"],
               ["", "IVA:", "#{@iva}"] ,
               ["", "Imp. Especi.:", "#{@esp}"] ,
               ["", "Total:", "#{@total}"], 
               ] 
    pdf.table(data)  do 
      cells.size =  8
      cells.borders =  []
      columns(1).font_style   =  :bold 
      columns(0).width =  398
      columns(1).width =  60
      columns(2).width =  60
      columns(0..2).borders =  [:right] 
    end

   

pdf.start_new_page

  data =  [["RECEPCION CONFORME"]]
  pdf.table(data, :row_colors =>  ["e6e6e6", "e6e6e6"]) do
    cells.borders =  [] 
    columns(0).width =  520
        cells.size =  10
        row(0).borders      =  [:right] 
        row(0).border_width = 1 
        row(0).font_style   =  :bold 
        columns(0).borders =  [:bottom]


  end

    data =  [  ["Tipo Doc.:", "#{@recepcion.tipo_documento}", "", ""],
               ["Rut:", "#{@recepcion.proveedor.rut}", "Nombre Prov.:", "#{@recepcion.proveedor.nombre}"],
               ["Num Factura:", "#{@recepcion.num_factura}","Fecha Fac.:", "#{@recepcion.fecha_factura.strftime('%d/%m/%Y a las %I:%M%p')}"], 
               ["Orden Compra:", "#{@recepcion.orden_compra}", "Recibido por:", "#{@recepcion.usuario.nombre} #{@recepcion.usuario.apaterno}"],
               ["OPI:", "#{@recepcion.orden.id}", "Periodo:", "#{@recepcion.orden.periodo.anio}"] ,
               ["Observación:", "#{@recepcion.observacion}", "Establecimiento:", "#{@est = @recepcion.orden.departamento.establecimiento.nombre}"], 
               ] 
    pdf.table(data)  do 
    	cells.size =  8
    	cells.borders =  []
      columns(0).font_style   =  :bold 
    	columns(2).font_style   =  :bold 
      columns(0).width =  75
      columns(1).width =  170
      columns(2).width =  115
      columns(3).width =  160
    end

pdf.move_down 30
if not @director.blank?
  if not @director.firma_usuario.blank?
    pdf.image "#{Rails.root}/public#{@director.firma_usuario.archivo.url}", :fit => [170, 85], :position => :center
    
  else
    pdf.move_down 85
  end
    pdf.horizontal_line 100, 200
    pdf.text "#{@nombre_director}", :size => 8, :style => :bold, :align => :center
    pdf.text "Director Establecimiento", :size => 8, :style => :bold, :align => :center
end



	
string = "Pagina <page> de <total>"
options = { :at => [pdf.bounds.right - 150, 0],
:width => 150,
:align => :center,
:size => 8,
:color => "000000" }
pdf.number_pages string, options





