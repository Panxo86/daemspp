# encoding: utf-8
class EducaMailer < ActionMailer::Base
  default from: "notificacion@daem.sanpedrodelapaz.cl"

  #.C!nD8?kM@;v
    def consulta orden
    	@orden = orden
    	#puts "#{orden.consulta.usuario.email}".red
  		mail to: "#{orden.consulta.usuario.email}", 
	    subject: "Tiene una consulta de la OPI N° #{@orden.id} en su bandeja personal",
	    from: "Sistema OPI Digital <notificacion@daem.sanpedrodelapaz.cl>"
    end

    def rechazada(orden)
    	@orden = orden
        direccion = Departamento.first(:establecimiento_id => @orden.departamento.establecimiento_id, :es_direccion => true)
    	director = Usuario.all(:departamento_id => direccion.id).first
    	#puts "#{director.email}, #{@orden.usuario.email}".red
  		mail to: "#{director.email}, #{@orden.usuario.email}", 
	    subject: "Se ha Rechazado OPI N° #{@orden.id}",
	    from: "Sistema OPI Digital <notificacion@daem.sanpedrodelapaz.cl>"
    end

    def entrante(etapa)
    	@e = etapa
	    destinatarios = ""

	    if @e.bandeja.nombre == "Encargado Subvención"
			encargados = EncargadoSubvencion.all(:es_vigente => true, :subvencion_id => @e.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id)
			encargados.each_with_index do |u, x|
				if x > 0
		        	destinatarios += ", "
		    	end
		        destinatarios += "#{u.usuario.email}"
			end
		elsif @e.bandeja.nombre == "Asesor Subvención"
			asesores = AsesorSubvencion.all(:es_vigente => true, :subvencion_id => @e.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.id)
			asesores.each_with_index do |u, x|
				if x > 0
		        	destinatarios += ", "
		    	end
		        destinatarios += "#{u.usuario.email}"
			end
			
		else
			@e.bandeja.bandeja_usuarios.each_with_index do |u, x|
		    	if x > 0
		        	destinatarios += ", "
		    	end
		        destinatarios += "#{u.usuario.email}"
		     end
		end
	    

	    mail to: "#{destinatarios}"	, 
	    subject: "Tiene una nueva OPI N° #{@e.orden.id}, en su bandeja de #{@e.bandeja.nombre}",
	    from: "Sistema OPI Digital <notificacion@daem.sanpedrodelapaz.cl>"
  	end

  	def validar(destino, orden)
  		@orden = orden
  		mail to: destino, 
	    subject: "Se ha creado una nueva OPI N° #{@orden.id}, para su validacion",
	    from: "Sistema OPI Digital <notificacion@daem.sanpedrodelapaz.cl>"
  	end
end
