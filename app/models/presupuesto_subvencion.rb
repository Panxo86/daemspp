#encoding: utf-8
class PresupuestoSubvencion

  include DataMapper::Resource

  property :id, Serial

  property :monto, String, :required => true, :length => 100

  belongs_to :establecimiento
  belongs_to :subvencion
  belongs_to :periodo

end
