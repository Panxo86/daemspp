class PmeActividad

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial
  property :nombre, String, :required => true, :length => 200
  property :descripcion, Text, :required => true
  property :desde, Date, :required => true
  property :hasta, Date, :required => true
  property :responsable_nombre, String, :required => true, :length => 200
  property :es_vigente, Boolean, :required => true, :default => true
  property :terminado, Boolean, :required => true, :default => false
  property :avance, String

  timestamps :at 

  has n, :pme_actividad_docs
  belongs_to :pme_accion
  

end
