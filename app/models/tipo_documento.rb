class TipoDocumento

  include DataMapper::Resource

  property :id, Serial
  property :nombre, String
  property :es_vigente, Boolean, :default => true

  def self.vigentes
	data = TipoDocumento.all(:es_vigente => true, :order => :nombre)
  end

end
