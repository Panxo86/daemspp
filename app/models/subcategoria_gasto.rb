#encoding: utf-8
class SubcategoriaGasto

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true
  property :codigo, String
  property :es_vigente, Boolean, :required => true, :default => true

  belongs_to :categoria_gasto
  has n, :ordens

  def self.todos
	data = SubcategoriaGasto.all(:es_vigente => true, :order => :nombre)
  end

end
