#encoding: utf-8
require 'carrierwave/datamapper'
class FirmaUsuario

  include DataMapper::Resource

  property :id, Serial
  property :width, String
  property :height, String
  mount_uploader :archivo, ArchivoUploader

  belongs_to :usuario
end
