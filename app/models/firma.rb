#encoding: utf-8
class Firma

  include DataMapper::Resource

  property :id, Serial
  property :director_est, Boolean, :default => false
  property :director_daem, Boolean, :default => false
  property :finanzas, Boolean, :default => false
  property :asesor_sep, Boolean, :default => false
  property :asesor_junji, Boolean, :default => false
  property :coordinador_tec, Boolean, :default => false
  property :personal, Boolean, :default => false
  property :tesoreria, Boolean, :default => false
  property :encargado_subvencion, Boolean, :default => false
  property :asesor_subvencion, Boolean, :default => false

  belongs_to :orden


end
