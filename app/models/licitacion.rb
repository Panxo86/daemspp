#encoding: utf-8
class Licitacion

  include DataMapper::Resource

  property :id, Serial
  property :cod, String, :required => true, :length => 100
  property :url, String, :required => true, :length => 500
  property :inicio, Date, :required => true
  property :termino, Date, :required => true
  property :estado, String, :required => true

  belongs_to :orden
  has 1, :compra_licitacion


end
