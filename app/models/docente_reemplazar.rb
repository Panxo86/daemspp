class DocenteReemplazar

  include DataMapper::Resource

  property :id, Serial

  belongs_to :docente
  belongs_to :docente_solicitud
end
