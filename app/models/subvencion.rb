#encoding: utf-8
class Subvencion

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true
  
  property :es_vigente, Boolean, :required => true, :default => true
  property :es_sep, Boolean, :required => true, :default => false

  has n, :tipo_gastos
  has n, :presupuesto_subvencions
  has n, :historial_presupuesto
  has n, :carga_directas
  has n, :asesor_subvencions
  has n, :encargado_subvencions
  #has n, :conductos
  
  def self.todos
	data = Subvencion.all(:order => :nombre, :es_vigente => true)
  end

end
