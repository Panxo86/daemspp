#encoding: utf-8
class SubvencionBase

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true, :length => 200
  property :codigo, String, :required => true
  property :porcentaje, Integer, :required => true
  property :es_vigente, Boolean, :required => true, :default => true

  belongs_to :subvencion
  has n, :liq_bases, "LiqBase"


  def self.todos
  data = SubvencionBase.all(:order => :nombre)
  end
  def self.codigo(cod)
  data = SubvencionBase.all(:codigo => cod)
  end

end
