class DocenteContratado

  include DataMapper::Resource

  property :id, Serial

  property :horas, String
  property :desde, Date
  property :hasta, Date

  belongs_to :docente
  belongs_to :docente_solicitud

end
