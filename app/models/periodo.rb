#encoding: utf-8
class Periodo

  include DataMapper::Resource

  property :id, Serial

  property :anio, String, :required => true
  property :es_vigente, Boolean, :required => true, :default => false
  property :es_obsoleto, Boolean, :required => true, :default => false

  has n, :ordens
  has n, :objetivos
  has n, :accions
  has n, :areas
  has n, :historial_presupuesto
  has n, :carga_directas
  has n, :pme_objetivos
  has n, :suministros

  def self.todos
	data = Periodo.all(:es_obsoleto => false, :order => :anio)
  end

end
