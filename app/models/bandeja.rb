class Bandeja

  include DataMapper::Resource

  property :id, Serial
  property :nombre, String
  property :es_asesor, Boolean, :default => false
  property :es_encargado, Boolean, :default => false

  has n, :rutas
  has n, :bandeja_usuarios
  has n, :conductos

  def self.todas
	data = Bandeja.all(:order => :id)
  end
end
