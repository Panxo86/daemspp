#encoding: utf-8
class Recepcion
	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :num_factura, String
	property :tipo_documento, String
	property :fecha_factura, Date
	property :observacion, Text
	property :es_nula, Boolean, :required => true, :default => false
	property :neto, Integer, :required => true
	property :iva, Integer, :required => true
	property :total, Integer, :required => true
	property :subtotal, Integer, :required => true
	property :total_documento, Integer, :required => true
	property :orden_compra, String
	property :tiene_pendiente, Boolean, :required => true, :default => false
	property :descuento, Integer, :required => true, :default => 0
	property :exento_iva, Boolean, :required => true, :default => false
	property :pagar, Boolean, :required => true, :default => true
	property :num_cheque, String, :length => 300
	property :fecha_pago, Date
	property :dec_pago, String
	property :adquisiciones, Boolean, :required => true, :default => false
	property :observacion_adq, Text
	property :firmado, Boolean, :required => true, :default => false
	property :firmado_por, Integer
	property :imp_especifico, Integer, :required => true, :default => 0



	belongs_to :orden
	belongs_to :usuario
	belongs_to :proveedor
	has n, :recepcion_items

	timestamps :at 
end