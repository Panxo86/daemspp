class EstadoSistemaHistorial

  include DataMapper::Resource

  property :id, Serial
  property :fecha, DateTime
  property :estado, String

  belongs_to :estado_sistema

end
