class Docente

  include DataMapper::Resource

  property :id, Serial
  property :rut, String, :required => true, :unique => true, :messages => { :is_unique => "El RUT ya esta registrado.",}
  property :dv, String, :required => true
  property :nombre, String, :required => true
  property :apaterno, String, :required => true
  property :amaterno, String, :required => true
  property :email, String
  property :telefono, String
  property :nacimiento, Date
  property :direccion, String, :length => 800
  property :nacionalidad, String
  property :estado_civil, String
  property :sexo, String
  property :profesion, String
  property :es_nulo, Boolean, :required => true, :default => false

  belongs_to :docente_tipo
  belongs_to :docente_cargo
  has n, :docente_contratados
  has n, :docente_sugeridos
  has n, :docente_reemplazars


end
