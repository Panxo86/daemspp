#encoding: utf-8
class CargaDirecta
	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :mes, String, :required => true, :length => 2 
	property :total, String, :required => true
	property :es_nula, Boolean, :default => false

	belongs_to :establecimiento
	belongs_to :subvencion
	belongs_to :periodo
	belongs_to :usuario

	timestamps :at 
end