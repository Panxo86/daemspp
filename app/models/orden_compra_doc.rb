#encoding: utf-8
require 'carrierwave/datamapper'
class OrdenCompraDoc
	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :name, String, :length => 256

	mount_uploader :archivo, ArchivoUploader

	belongs_to :adquisicion

	timestamps :at 
end