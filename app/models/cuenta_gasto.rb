#encoding: utf-8
class CuentaGasto

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial
  property :numero, String, :required => true
  property :descripcion, String, :required => true
  property :es_nula, Boolean, :required => true, :default => false

  has n, :cuenta_gasto_ordens
  def self.vigentes
	data = CuentaGasto.all(:es_nula => false, :order => :numero)
  end
end
