#encoding: utf-8
class BienServicio

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial

  property :descripcion, String, :length => 500, :required => true
  property :cantidad, Integer, :required => true
  property :valor, Integer, :required => true
  property :total, Integer, :required => true

  belongs_to :orden

  timestamps :at 

end