#encoding: utf-8
class TipoGasto

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true, :length => 200

  property :es_vigente, Boolean, :required => true, :default => true
  property :es_persona, Boolean, :required => true, :default => false

  belongs_to :subvencion
  has n, :categoria_gastos

  def self.todos
	data = TipoGasto.all(:es_vigente => true, :order => :nombre)
  end

  def self.subvencion id
    data = TipoGasto.all(:subvencion_id => id, :es_vigente => true, :order => :nombre)
  end
  
end
