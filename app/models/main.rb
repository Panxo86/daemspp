	# encoding: utf-8

	#rails r "Main.traspasa_pme"
	class Main


		def self.firmas

			rutas = Ruta.all(:aprobada => true)
			rutas.each do |r|
				puts "BANdeja: #{r.bandeja.nombre}"
				if r.bandeja_id == 1
					subvencion_id = r.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion_id
					puts "Subvencion ID #{subvencion_id} #{r.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.nombre}"
					encargado = EncargadoSubvencion.all(:subvencion_id => subvencion_id, :usuario => {:es_nulo => false}).first
					if not encargado.blank?
						r.update :usuario_id => encargado.usuario_id
						puts "Asignada a: #{r.usuario.nombre} ".green
					else
						puts "Sin encargado subvencion".red
					end
				elsif r.bandeja_id == 2
					subvencion_id = r.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion_id
					puts "SubvencionID #{subvencion_id} #{r.orden.subcategoria_gasto.categoria_gasto.tipo_gasto.subvencion.nombre}"
					asesor = AsesorSubvencion.all(:subvencion_id => subvencion_id, :usuario => {:es_nulo => false}).first
					if not asesor.blank?
						r.update :usuario_id => asesor.usuario_id
						puts "Asignada a: #{r.usuario.nombre} ".green
					else
						puts "Sin asesor subvencion".red
					end
				else
					usuario_bandeja = BandejaUsuario.all(:bandeja_id => r.bandeja_id).first
					if not usuario_bandeja.blank?
						r.update :usuario_id => usuario_bandeja.usuario_id
						puts "Asignada a: #{r.usuario.nombre} ".green
					else
						puts "Bandeja sin usuario".red
					end

				end
				puts "----------------------------------------------------------------------------------"
			end

			ordenes = Orden.all(:periodo_id => 9)
			ordenes.each do |o|
				puts "ORDEN: #{o.id}"
				direccion = Departamento.first(:establecimiento_id => o.departamento.establecimiento_id, :es_direccion => true)
				if not direccion.blank?
	      			director = Usuario.all(:departamento_id => direccion.id, :es_nulo => false).first
	      			if not director.blank?
	      				o.update :director => director.id
	      				puts "Director: #{director.nombre}".green
	      			else
	      				puts "Sin Director".red
	      			end
	      		else
	      			puts "ESTABLECIMIENTO sin direccion".red
	      		end
      			puts "----------------------------------------------------------------------------------"
			end

			recepciones = Recepcion.all(:firmado => true)
			recepciones.each do |c|
				puts "RECEPCION: #{c.id}"
				direccion = Departamento.first(:establecimiento_id => c.orden.departamento.establecimiento_id, :es_direccion => true)
				if not direccion.blank?
	      			director = Usuario.all(:departamento_id => direccion.id, :es_nulo => false).first
	      			if not director.blank?
	      				c.update :firmado_por => director.id
	      				puts "Director: #{director.nombre}".green
	      			else
	      				puts "Sin Director".red
	      			end
	      		else
	      			puts "ESTABLECIMIENTO sin direccion".red
	      		end
      			puts "----------------------------------------------------------------------------------"


			end
			
		end


		def self.new_pme
			Area.transaction do 
			areas = Area.all(:es_vigente => true, :periodo_id => 6)
				areas.each do |a|
					#area se convierte en pme_dimension
					pme_dimension = PmeDimension.create(:nombre => a.nombre)
					puts "PME_DIMENSION: #{pme_dimension.nombre}"
					#recorro las dimensiones del area
					a.dimencion.each do |d|

						#las dimension se convierte en subdimension
						pme_sub_dimension = PmeSubDimension.create(:nombre => d.nombre, :pme_dimension_id => pme_dimension.id)
						puts "PME_SUB_DIMENSION: #{pme_sub_dimension.nombre}"
						#recorro los objetivo de subdimension
						d.objetivos(:periodo_id => 9).each do |o|

							#creo nuevo pme_objetivo
							pme_objetivo = PmeObjetivo.create(
								:nombre => o.nombre,
								:establecimiento_id => o.establecimiento_id,
								:periodo_id => o.periodo_id,
								:pme_dimension_id => pme_dimension.id
							)
							puts "PME_OBJETIVO: #{pme_objetivo.nombre}"
							#asoscio el objetivo con su pme_subdimension
							pme_subdimension_area = PmeObjetivoSubDimension.create(
								:pme_objetivo_id => pme_objetivo.id,
								:pme_sub_dimension_id => pme_sub_dimension.id
							)
							pme_estrategia = PmeEstrategia.create(
								:pme_objetivo_id => pme_objetivo.id,
								:nombre => "ESTRATEGIA PARA: #{pme_objetivo.nombre}"
							)
							puts "PME_ESTRATEGIA: #{pme_estrategia.nombre}"
							#recorro acciones objetivo
							o.accions(:periodo_id => 9).each do |acc|

								#creo acciones se convierten en pme_accion
								pme_accion = PmeAccion.create(
									:nombre => acc.nombre,
									:descripcion => acc.descripcion,
									:desde => acc.desde,
									:hasta => acc.hasta,
									:responsable_nombre => acc.responsable_nombre,
									:presupuesto => acc.presupuesto,
									:pme_estrategia_id => pme_estrategia.id
								)
								puts "PME_ACCION: #{pme_accion.nombre}"
								#recorro las OPI de la accion 
								acc.orden_tiene_accions.each do |opi|
									orden_tiene_pme_accion = OrdenTienePmeAccion.create(
										:pme_accion_id => pme_accion.id,
										:orden_id => opi.orden.id
									)
									puts "ORDEN_PME_ACCION: #{orden_tiene_pme_accion.orden.id}"
								end
							end
						end
						
					end
					
				end
			end
			
		end

		def self.hex_est
			establecimiento = Establecimiento.all(:hex_url => nil)
			puts "#{establecimiento.size}"
			establecimiento.each do |e|
				hex = ""
				busca_hex = 1
				while busca_hex > 0
					hex = SecureRandom.hex(3)
					puts "hex = #{hex}"
					busca_hex = Establecimiento.all(hex_url: hex).count
					puts "busca_hex = #{busca_hex}"
				end
				if not hex.blank?
					e.update hex_url: hex
					puts "#{e.nombre} = #{e.hex_url}"
				end
			end
		rescue Exception => e
			puts "#{e.message}"
		end

		def self.sub_est
			e = Establecimiento.all
			e.each do |e|
				Subvencion.todos.each do |s|
				  sub = PresupuestoSubvencion.last(
				  	:establecimiento_id => e.id,
			        :subvencion_id => s.id,
			        :periodo_id => 6
			      )
			      if sub.blank?
			      	
			      	 presupuesto = PresupuestoSubvencion.create(
				        :monto => "0",
				        :establecimiento_id => e.id,
				        :subvencion_id => s.id,
				        :periodo_id => 6
			      	)
			      	puts "Est: #{e.nombre} -> Sub: #{s.nombre}"
			      end    
			    end
			end
		end

		def self.traspasa_pme

			Objetivo.transaction do
				objetivos = PmeObjetivo.all(:periodo_id => 10, :es_vigente => true)
				objetivos.each do |o|
					puts "----------"
					nuevo_objetivo = PmeObjetivo.create(
						:nombre => o.nombre,
						:es_vigente => true,
						:pme_dimension_id => o.pme_dimension_id,
						:periodo_id => 11,
						:establecimiento_id => o.establecimiento_id
					)
					puts "Creado objetivo ID:#{nuevo_objetivo.id} de obj ID #{o.id}"
					o.pme_objetivo_sub_dimensions.each do |s|
						nuevo_obj_sub_dim = PmeObjetivoSubDimension.create(
							:pme_sub_dimension_id => s.pme_sub_dimension_id,
							:pme_objetivo_id => nuevo_objetivo.id
						)
						puts "Creado PmeObjetivoSubDimension #{nuevo_obj_sub_dim.id} de objetivo ID:#{nuevo_objetivo.id}"
					end

					o.pme_estrategias.each do |e|
						nueva_estrategia = PmeEstrategia.create(
							:nombre => e.nombre, 
							:es_vigente => true,
							:pme_objetivo_id => o.id
						)
						puts "Creado Estrategia #{nueva_estrategia.id} de objetivo ID:#{nuevo_objetivo.id}"
						e.pme_accions.each do |a|
							nueva_accion = PmeAccion.create(
								:nombre => a.nombre,
								:descripcion => a.descripcion,
								:es_vigente => true,
								:pme_estrategia_id => nueva_estrategia.id,
								:descripcion => a.descripcion,
								:desde => "2020-01-01",
								:hasta => "2020-12-31",
								:responsable_nombre => a.responsable_nombre,
								:recursos => a.recursos,
								:plan => a.plan,
								:programa => a.programa,
								:presupuesto => a.presupuesto
							)
							puts "Creado accion #{nueva_accion.id} de estrategia ID:#{nueva_estrategia.id}"
						end
					end
				end
			end
		rescue Exception => e
			puts "Error: #{e.message}"	
		end

	end