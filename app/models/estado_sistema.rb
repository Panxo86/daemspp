class EstadoSistema

  include DataMapper::Resource

  property :id, Serial
  property :activo, Boolean, :default => true
  property :desde, DateTime
  property :hasta, DateTime

  has n, :estado_sistema_historials

end
