class PmeObjetivoSubDimension

  include DataMapper::Resource

  property :id, Serial

  belongs_to :pme_objetivo
  belongs_to :pme_sub_dimension

end
