#encoding: utf-8
class Contrato

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial

  property :tipo, String, :required => true
  property :cargo, String, :required => true
  property :funcion, String, :required => true
  property :fecha_desde, Date, :required => true
  property :fecha_hasta, Date, :required => true
  property :bienios, String, :required => true
  property :titulado, Boolean, :required => true, :default => false
  property :mencion, Boolean, :required => true, :default => false
  
  belongs_to :orden

  timestamps :at 


end
