#encoding: utf-8
class Orden

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial

  property :cod, String, :required => true
  property :titulo, String, :required => true, :length => 100
  property :justificacion, String, :required => true, :length => 500
  property :es_entrante, Boolean, :required => true,:default => true
  property :termino, DateTime, :required => false
  property :estado, String, :required => true
  property :es_editable, Boolean, :required => true,:default => false
  property :es_nula, Boolean, :required => true,:default => false
  property :lista_recepcion, Boolean, :required => true,:default => false
  property :enviada_personal, Boolean, :required => true,:default => false
  property :es_cometido, Boolean, :required => true,:default => false
  property :en_consulta, Boolean, :required => true,:default => false
  property :en_proceso, Boolean, :required => true,:default => false
  property :en_emisor, Boolean, :required => true,:default => false
  property :en_direccion, Boolean, :required => true,:default => false
  property :posicion, String, :required => false, :length => 1
  property :es_suministro, Boolean, :required => true,:default => false
  property :director, Integer

  belongs_to :subcategoria_gasto
  belongs_to :usuario
  belongs_to :periodo
  belongs_to :departamento 
  has 1, :ubicacion
  has 1, :cuenta_gasto_orden
  has n, :bien_servicios
  has 1, :recurso_humano
  has 1, :contrato
  has n, :movimientos
  has n, :documentos
  has n, :adquisicions
  has n, :licitacions
  has n, :recepcion
  has 1, :orden_tiene_accion
  has 1, :firma
  has 1, :consulta
  has n, :rutas
  has 1, :orden_tiene_pme_accion
  has n, :suministros
  has 1, :orden_suministro

  timestamps :at 

  def self.establecimiento id
	data = Orden.all(:establecimiento_id => id)
  end
end
