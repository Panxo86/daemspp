class DocenteSolicitudMovimiento

  	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :descripcion, String, :required => true, :length => 400
	property :estado, String, :required => true
	property :comentario, String, :required => true, :length => 800

	belongs_to :departamento
	belongs_to :docente_solicitud
	belongs_to :usuario

	timestamps :at 


end
