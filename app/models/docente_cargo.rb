class DocenteCargo

  include DataMapper::Resource

  property :id, Serial
  property :nombre, String, :required => true
  property :es_nulo, Boolean, :required => true, :default => false

  has n, :docentes
  has n, :docente_solicituds

  def self.vigentes
	data = DocenteCargo.all(:order => :nombre, :es_nulo => false)
  end

end
