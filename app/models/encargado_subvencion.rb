class EncargadoSubvencion

  include DataMapper::Resource

  property :id, Serial
  property :es_vigente, Boolean, :default => true

  belongs_to :subvencion	
  belongs_to :usuario

  timestamps :at 

end
