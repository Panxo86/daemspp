#encoding: utf-8
class Usuario

  include DataMapper::Resource

  property :id, Serial

  property :rut, String, :required => true, :unique => true, :messages => { :is_unique => "El RUT ya esta registrado.",}
  property :nombre, String, :required => true
  property :apaterno, String, :required => true
  property :amaterno, String, :required => true
  property :email, String, :required => true, :unique => true, :messages => { :is_unique => "El Email ya esta registrado.",}
  property :password, String, :required => true
  property :es_nulo, Boolean, :required => true, :default => false

  belongs_to :departamento
  has n, :ordens
  has 1, :sistema
  has n, :documentos
  has n, :historial_presupuesto
  has n, :recepcion
  has n, :asesor_subvencions
  has n, :encargado_subvencions
  has n, :consultas
  has n, :bandeja_usuarios
  has n, :pme_actividad_docs
  has n, :docente_solicituds
  has 1, :firma_usuario
  
  def self.todos
	data = Usuario.all(:order => :nombre, :es_nulo => false)
  end

  def self.bajas
  data = Usuario.all(:order => :nombre, :es_nulo => true)
  end
  def self.establecimiento
	data = Usuario.all(:es_nulo => false, :departamento => {:establecimiento_id => id}, :order => :nombre)
  end

  def self.daem
  data = Usuario.all(:es_nulo => false, :departamento => {:establecimiento_id => 1}, :order => :apaterno)
  end

end
