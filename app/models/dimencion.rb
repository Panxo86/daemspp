#encoding: utf-8
class Dimencion

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true
  property :es_vigente, Boolean, :required => true, :default => true

  belongs_to :area
  has n, :objetivos
end
