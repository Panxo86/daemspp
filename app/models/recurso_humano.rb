#encoding: utf-8
class RecursoHumano

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial

  property :rut, String, :required => true
  property :nombre, String, :required => true
  property :apaterno, String, :required => true
  property :amaterno, String, :required => true
  property :sexo, String, :required => true
  property :nacimiento, Date, :required => true
  property :estado_civil, String, :required => true
  property :nacionalidad, String, :required => true
  property :comuna, String, :required => true
  property :calle, String, :required => true
  property :numero, String, :required => true
  property :dpto, String
  property :email, String, :required => true
  property :telefono, String, :required => true
  property :afp, String, :required => true
  property :salud, String, :required => true
  property :profesion, String
  property :nivel_educacional, String, :required => true

  belongs_to :orden

  timestamps :at 


  def self.todos
	data = RecursoHumano.all(:order => :nombre)
  end
end
