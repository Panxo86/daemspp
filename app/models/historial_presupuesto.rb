#encoding: utf-8
class HistorialPresupuesto
	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :descripcion, String, :required => true, :length => 450
	property :entrada, String, :required => true
	property :salida, String, :required => true
	property :total, String, :required => true

	belongs_to :subvencion
	belongs_to :periodo
	belongs_to :usuario
	belongs_to :establecimiento

	timestamps :at 
end