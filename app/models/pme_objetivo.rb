class PmeObjetivo

  include DataMapper::Resource

  property :id, Serial
  property :nombre, Text, :required => true
  property :es_vigente, Boolean, :required => true, :default => true

  has n, :pme_estrategias
  has n, :pme_objetivo_sub_dimensions
  belongs_to :pme_dimension
  belongs_to :establecimiento
  belongs_to :periodo
  
  def self.dimension(id)
	data = PmeObjetivo.all(:pme_dimension_id => id, :es_vigente => true, :order => :nombre)
end

end
