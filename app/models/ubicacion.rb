#encoding: utf-8
class Ubicacion

  include DataMapper::Resource

  property :id, Serial

  belongs_to :orden  
  belongs_to :departamento  

  timestamps :at 

end
