class PmeDimension

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true, :length => 200
  property :es_vigente, Boolean, :required => true, :default => true

  has n, :pme_sub_dimensions
  has n, :pme_objetivos

	def self.todas(periodo)
		data = PmeDimension.all(:es_vigente => true, :order => :nombre)
	end

end
