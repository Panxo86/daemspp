#encoding: utf-8
class Proveedor
	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :rut, String, :required => true, :unique => true, :length => 15, :messages => {
														      :is_unique => "El rut ya esta registrado.",
	    													}
	property :nombre, String, :required => true, :length => 400
	property :giro, String, :length => 200
	property :contacto, String, :length => 200
	property :telefono, String
	property :direccion, String, :length => 200
	property :tipo, String

	has n, :recepcions
	
	timestamps :at 
end