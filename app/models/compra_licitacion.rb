#encoding: utf-8
class CompraLicitacion

  include DataMapper::Resource

  property :id, Serial

  belongs_to :adquisicion  
  belongs_to :licitacion  

end
