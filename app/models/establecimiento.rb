#encoding: utf-8
class Establecimiento

  include DataMapper::Resource

  property :id, Serial

  property :rbd, String, :required => true, :length => 10, :unique => true, :messages => { :is_unique => "El RBD ya esta registrado.",}
  property :nombre, String, :required => true, :length => 100
  property :direccion, String, :required => true, :length => 400
  property :es_dificil, Boolean, :required => true, :default => false
  property :es_admin, Boolean, :required => true, :default => false
  property :hex_url, String


  has n, :departamentos
  has n, :objetivos
  has n, :presupuesto_subvencions
  has n, :carga_directas
  has n, :pme_objetivos
  has n, :docente_solicituds
  
  def self.todos
	data = Establecimiento.all(:order => :nombre)
  end

  def self.colegios
  data = Establecimiento.all(:id.not => 1)
  end
end
