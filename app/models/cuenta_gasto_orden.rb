#encoding: utf-8
class CuentaGastoOrden

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial
  property :cuenta, String, :length => 500
  property :item, String
  property :subtitulo, String
  property :asignacion, String

  belongs_to :orden 
  belongs_to :cuenta_gasto 


end
