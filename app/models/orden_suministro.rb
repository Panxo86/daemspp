#encoding: utf-8
class OrdenSuministro

  include DataMapper::Resource

  property :id, Serial 
  property :monto, Integer 

  belongs_to :suministro
  belongs_to :orden
  

end
