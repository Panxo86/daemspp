#encoding: utf-8
class LiquidacionSubvencion
	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :mes, String, :required => true, :length => 2 
	property :anio, String, :required => true, :length => 4
	property :total, String, :required => true
	property :incremento_zona, String, :required => true
	property :desempeno_dificil, String, :required => true
	property :adicional_especial, String, :required => true
	property :no_docentes, String, :required => true
	property :sub_total_aportes, String, :required => true
	property :sub_total_pagos, String, :required => true
	property :asignado, String, :required => true
	property :porcentaje_daem, String, :required => true
	property :total_daem, String, :required => true
	property :sep, String, :required => true
	property :sep_porcentaje, String, :required => true
	property :sep_daem, String, :required => true
	property :sep_porcentaje_est, String, :required => true
	property :sep_est, String, :required => true
	property :es_nula, Boolean, :default => false


	belongs_to :establecimiento
	belongs_to :periodo
	belongs_to :usuario
	has n, :liq_bases, "LiqBase"
	has n, :liq_descuentos
	has n, :liq_retencions
	has n, :liq_bonos

	timestamps :at 
end