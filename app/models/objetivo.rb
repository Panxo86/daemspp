#encoding: utf-8
class Objetivo

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true, :length => 800
  property :es_vigente, Boolean, :required => true, :default => true

  belongs_to :dimencion
  belongs_to :periodo
  belongs_to :establecimiento
  has n, :accions

  def self.todos
	data = Objetivo.all(:order => :nombre)
  end
end
