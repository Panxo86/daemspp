#encoding: utf-8
class RecepcionItem
	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :descripcion, String, :length => 500, :required => true
	property :descripcion_comprador, Text
	property :descripcion_proveedor, Text
	property :cantidad, Integer, :required => true
	property :neto, Integer, :required => true, :min => 0
	property :total, Integer, :required => true, :min => 0

	belongs_to :recepcion

end