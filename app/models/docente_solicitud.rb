class DocenteSolicitud

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial
  property :horas, String, :required => true
  property :desde, Date, :required => true
  property :hasta, Date, :required => true
  property :estado, String, :required => true, :default => "Digitada"
  property :justificacion, Text, :required => true
  property :motivo, String
  property :es_nulo, Boolean, :required => true, :default => false

  has 1, :docente_sugerido
  has 1, :docente_contratado
  has 1, :docente_reemplazar
  has n, :docente_solicitud_movimientos
  has n, :docente_solicitud_archivos

  timestamps :at 

  belongs_to :establecimiento
  belongs_to :docente_tipo
  belongs_to :docente_cargo
  belongs_to :usuario

end
