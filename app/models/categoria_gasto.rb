#encoding: utf-8
class CategoriaGasto

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true
  property :codigo, String
  property :es_vigente, Boolean, :required => true, :default => true

  belongs_to :tipo_gasto
  has n, :subcategoria_gastos

  def self.todos
	data = CategoriaGasto.all(:es_vigente => true, :order => :nombre)
  end
end
