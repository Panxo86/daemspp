class Ruta

  include DataMapper::Resource
  include DataMapper::Timestamps
  

  property :id, Serial
  property :aprobada, Boolean, :default => false
  property :rechazada, Boolean, :default => false
  property :activa, Boolean, :default => false
  property :fecha_aprobada, DateTime
  property :comentario, String, :length => 800
  property :firma, Boolean, :default => false

  timestamps :at 

  belongs_to :bandeja
  belongs_to :orden
  belongs_to :usuario


end
