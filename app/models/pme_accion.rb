class PmeAccion

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial
  property :nombre, String, :required => true, :length => 200
  property :descripcion, Text, :required => true
  property :desde, Date
  property :hasta, Date
  property :responsable_nombre, String, :required => true, :length => 200
  property :recursos, Text
  property :plan, Text
  property :programa, Text
  property :presupuesto, String, :required => true
  property :es_vigente, Boolean, :required => true, :default => true
  property :evaluacion, Text
  property :evaluacion_inicio, Date
  property :evaluacion_fin, Date
  property :evaluacion_cuantitativo, Text
  property :evaluacion_cualitativo, Text

  timestamps :at 

  has n, :pme_actividads
  has n, :orden_tiene_pme_accions
  belongs_to :pme_estrategia

  

end
