class PmeEstrategia

  include DataMapper::Resource

  property :id, Serial
  property :nombre, Text, :required => true
  property :es_vigente, Boolean, :required => true, :default => true

  has n, :pme_accions
  belongs_to :pme_objetivo

  def self.objetivo(id)
	data = PmeEstrategia.all(:pme_objetivo_id => id, :es_vigente => true, :order => :nombre)
end
  
end
