#encoding: utf-8
class Suministro

  include DataMapper::Resource

  property :id, Serial

  property :titulo, String, :required => true
  property :descripcion, Text
  property :total, Integer
  property :es_vigente, Boolean, :required => true, :default => true

  has n, :orden_suministros
  belongs_to :orden
  belongs_to :periodo

  
  def self.vigentes(periodo)
	data = Suministro.all(:es_vigente => true, :periodo_id => periodo)
  end

end
