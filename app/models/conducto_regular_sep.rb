#encoding: utf-8
class ConductoRegularSep

  include DataMapper::Resource

  property :id, Serial

  property :posicion, Integer, :required => true

  belongs_to :departamento

  def self.todos
	data = ConductoRegularSep.all(:order => :posicion)
  end

end
