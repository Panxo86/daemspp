#encoding: utf-8
class Departamento

  include DataMapper::Resource

  property :id, Serial
  property :nombre, String, :required => true, :length => 100
  property :es_direccion, Boolean, :required => true, :default => false
  property :es_asesor, Boolean, :required => true, :default => false

  belongs_to :establecimiento
  has n, :usuarios
  has n, :ubicacions
  has n, :documentos
  has n, :ordens
  has n, :pme_actividad_docs

  def self.todos
	data = Departamento.all(:order => :nombre)
  end
  def self.daem
  data = Departamento.all(:order => :nombre, :establecimiento_id => 1)
  end
  def self.asesor
  data = Departamento.all(:order => :nombre, :es_asesor => true)
  end
end


