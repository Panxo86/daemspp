class PmeSubDimension

  include DataMapper::Resource

  property :id, Serial
  property :nombre, String, :required => true, :length => 200
  property :es_vigente, Boolean, :required => true, :default => true

  has n, :pme_objetivo_sub_dimensions
  belongs_to :pme_dimension
  

end
