#encoding: utf-8
class LiqDescuento

  include DataMapper::Resource

  property :id, Serial
  property :descripcion, String, :required => true, :length => 50
  property :monto, String, :required => true

  belongs_to :liquidacion_subvencion


  
end
