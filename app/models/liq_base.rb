#encoding: utf-8
class LiqBase

  include DataMapper::Resource

  property :id, Serial
  property :monto, String, :required => true
  property :porcentaje, String, :required => true
  property :asignado, String, :required => true

  belongs_to :liquidacion_subvencion
  belongs_to :subvencion_base


  
end
