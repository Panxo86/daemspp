class Conducto

  include DataMapper::Resource

  property :id, Serial
  property :cometido, Boolean, :default => false
  property :bien, Boolean, :default => false
  property :firma, Boolean, :required => true, :default => false

  timestamps :at 

  #belongs_to :subvencion
  belongs_to :bandeja

  def self.actual
	data = Conducto.all(:bien => true, :order => :id)
  end

end
