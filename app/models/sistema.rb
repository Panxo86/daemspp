#encoding: utf-8
class Sistema
	include DataMapper::Resource
	include DataMapper::Timestamps

	property :id, Serial
	property :orden, Integer, :required => true, :default => 0
	property :finanzas, Integer, :required => true, :default => 0
	property :adquisiciones, Integer, :required => true, :default => 0
	property :tesoreria, Integer, :required => true, :default => 0
	property :admin, Integer, :required => true, :default => 0
	property :pme, Integer, :required => true, :default => 0
	property :dotacion_admin, Integer, :required => true, :default => 0
	property :dotacion, Integer, :required => true, :default => 0
	property :validar_orden, Boolean, :required => true, :default => false

	belongs_to :usuario
		
	timestamps :at 
end 