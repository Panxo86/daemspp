#encoding: utf-8
require 'carrierwave/datamapper'
class PmeActividadDoc

  include DataMapper::Resource
  include DataMapper::Timestamps

  property :id, Serial
  property :name, String, :length => 256

	mount_uploader :archivo, ArchivoUploader

	belongs_to :pme_actividad, :require => false
	belongs_to :departamento
	belongs_to :usuario

	timestamps :at 


end
