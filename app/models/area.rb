#encoding: utf-8
class Area

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true
  property :es_vigente, Boolean, :required => true, :default => true

  has n, :dimencion 
  belongs_to :periodo
  
  def self.todos(periodo)
	data = Area.all(:es_vigente => true, :order => :nombre)
  end

end
