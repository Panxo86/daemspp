#encoding: utf-8
class Accion

  include DataMapper::Resource

  property :id, Serial

  property :nombre, String, :required => true, :length => 200
  property :descripcion, Text, :required => true
  property :desde, Date, :required => true
  property :hasta, Date, :required => true
  property :responsable_nombre, String, :required => true, :length => 200
  property :presupuesto, String, :required => true
  property :es_vigente, Boolean, :required => true, :default => true
  
  belongs_to :objetivo
  belongs_to :periodo

  has n, :orden_tiene_accions
 

end
