#encoding: utf-8
class Adquisicion

  include DataMapper::Resource

  property :id, Serial

  property :orden_compra, String, :required => true
  property :monto, String, :required => true
  property :fecha, Date, :required => true
  property :cod_licitacion, String
  property :fue_licitacion, Boolean, :required => true, :default => false
  property :es_nula, Boolean, :required => true, :default => false
  property :eliminada, Boolean, :required => true, :default => false
  property :motivo, String, :length => 255
  property :dolar, String
  property :monto_dolar, String
  property :tipo_moneda, String

  belongs_to :orden  
  has 1, :compra_licitacion
  has 1, :orden_compra_doc
  


end
