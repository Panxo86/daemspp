module ApplicationHelper
	def dinero num
		if !num.blank?
	    	number_to_currency(num, :precision => (num.round == num) ? 0 : 2, :delimiter => ".")
	    end
    end

    def avance num
        msj = ""
        case num
        when 0
            msj = "0%"
        when 20
            msj = "1% - 24%"
        when 40
             msj = "25% - 49%"
        when 70
             msj = "50% - 74%"
        when 90
             msj = "75% - 99%"
        when 100
             msj = "100%"
         end

         msj

        
    end

    def get_etapa_actual id
        etapa = Ruta.all(:orden_id => id, :activa => true).first
        if etapa.blank?
            data = ""
        else
            data = etapa.bandeja.nombre
        end
        data
    end

    def get_etapa_anterior id
        etapa = Ruta.all(:orden_id => id, :aprobada => true, :activa => false).last
        if etapa.blank?
            data = ""
        else
            data = etapa.bandeja.nombre
        end
        data
    end

    def get_siguiente_etapa id
        etapa = Ruta.all(:orden_id => id, :aprobada => false, :activa => false).first
        if etapa.blank?
            data = "Terminar OPI aprobada"
        else
            data = etapa.bandeja.nombre
        end
        data
    end

    def ubicacion_opi id
    orden = Orden.get id
    case orden.estado 
      when "Conducto Regular"
        ubicacion = orden.rutas(:activa => true).first.bandeja.nombre
      when "En consulta"
        if orden.consulta.blank?
          ubicacion = "#{orden.ubicacion.departamento.nombre}"
        else
          ubicacion = "#{orden.consulta.usuario.nombre} #{orden.consulta.usuario.apaterno}"
        end
      when "En proceso"
        ubicacion = "#{orden.ubicacion.departamento.nombre}"
      when "Digitada"
        ubicacion = "#{orden.departamento.establecimiento.nombre}"
      when "Rechazada"
        ubicacion = "Direccion - #{orden.departamento.establecimiento.nombre}"
      else
        ubicacion = "#{orden.departamento.nombre} - #{orden.departamento.establecimiento.nombre}"
      end
      ubicacion

  end

    def miles num
        if !num.blank?
            number_to_currency(num, :precision => (num.round == num) ? 0 : 2, :delimiter => ".", :unit => "")
        end
    end

    def fecha_hora fecha
        fecha.strftime("%d/%m/%Y a las %H:%M hrs.")
    rescue
        nil
    end

    def fecha fecha
        fecha.strftime("%d/%m/%Y")
    rescue
        nil
    end

    def filename url
        url.split("/").last
    end

    def mes mes

        m = ""
        case mes
        when "1"
            m = "Enero"
        when "2"
            m = "Febrero"
            when "3"
            m = "Marzo"
            when "4"
            m = "Abril"
            when "5"
            m = "Mayo"
            when "6"
            m = "Junio"
            when "7"
            m = "Julio"
            when "8"
            m = "Agosto"
            when "9"
            m = "Septiembre"
            when "10"
            m = "Octubre"
            when "11"
            m = "Noviembre"
            when "12"
            m = "Diciembre"
        end
        m


    end
end
