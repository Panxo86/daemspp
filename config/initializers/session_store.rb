#require 'dm-rails/session_store'
# Be sure to restart your server when you modify this file.

Educa::Application.config.session_store :cookie_store, key: '_educa_session'
#Educa::Application.config.session_store Rails::DataMapper::SessionStore

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# Educa::Application.config.session_store :active_record_store
