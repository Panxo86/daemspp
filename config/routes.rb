Educa::Application.routes.draw do



  root :to => "home#redirigir"

  get 'home/redirigir' => 'home#redirigir', as: :redirigir

  get 'suministros/' => 'suministro#index', as: :suministros
  get 'suministros/nuevo' => 'suministro#nuevo', as: :nuevo_suministro
  post 'suministros/guardar' => 'suministro#guardar', as: :guardar_suministro
  get 'suministros/:id/ver' => 'suministro#ver', as: :ver_suministro
  post 'suministros/:id/actualizar' => 'suministro#actualizar', as: :actualizar_suministro
  post 'suministros/:id/eliminar' => 'suministro#eliminar', as: :eliminar_suministro
  get 'suministros/:id/ver_orden' => 'suministro#ver_orden', as: :ver_orden_suministro
  post 'suministros/:id/asignar_orden' => 'suministro#asignar_orden', as: :asignar_orden
  post 'suministros/:id/quitar_orden' => 'suministro#quitar_orden', as: :quitar_orden

  get 'dotacion-docente/solicitudes' => 'dotacion_docente#solicitudes', as: :solicitudes_dotacion
  get 'dotacion-docente/solicitudes-enviadas' => 'dotacion_docente#solicitudes_enviadas', as: :solicitudes_enviadas
  get 'dotacion-docente/nueva-solicitud' => 'dotacion_docente#nueva', as: :nueva_solicitud_dotacion
  post 'dotacion-docente/crear-solicitud' => 'dotacion_docente#crea_solicitud', as: :crea_solicitud_dotacion
  get 'dotacion-docente/reemplazos-activos' => 'dotacion_docente#remplazos_activos', as: :remplazos_activos
  get 'dotacion-docente/reemplazos-terminados' => 'dotacion_docente#remplazos_terminados', as: :remplazos_terminados
  get 'dotacion-docente/:id/ver-solicitud' => 'dotacion_docente#ver_solicitud', as: :ver_solicitud_dotacion
  post 'dotacion-docente/:id/guarda-doc' => 'dotacion_docente#guarda_doc_solicitud', as: :guarda_doc_solicitud
  post 'dotacion-docente/:id/elimina-doc' => 'dotacion_docente#elimina_doc_solicitud', as: :elimina_doc_solicitud
  post 'dotacion-docente/:id/actualizar-solicitud' => 'dotacion_docente#actualizar_solicitud', as: :actualizar_solicitud_dotacion
 

  post 'firma_usuario/:id/sube' => 'firma_usuario#sube', as: :sube_firma
  post 'firma_usuario/:id/borra' => 'firma_usuario#borra', as: :borra_firma

  post 'dotacion-docente/actualizar-docente' => 'docente#actualizar_docente', as: :actualizar_docente
  get 'dotacion-docente/get-docente' => 'docente#get_docente', as: :get_docente
  get 'dotacion-docente/:id/ver-docente' => 'docente#ver_docente', as: :ver_docente



  get 'plan-mejoramiento-educativo/dimensiones' => 'pme_parametros#dimensiones', as: :pme_dimensiones
  post 'plan-mejoramiento-educativo/create_pme_dimension' => 'pme_parametros#create_pme_dimension', as: :create_pme_dimension
  post 'plan-mejoramiento-educativo/:id/destroy_pme_dimension' => 'pme_parametros#destroy_pme_dimension', as: :destroy_pme_dimension
  post 'plan-mejoramiento-educativo/jeditable_nombre_pme_dimension' => 'pme_parametros#jeditable_nombre_pme_dimension', as: :jeditable_nombre_pme_dimension
  get 'plan-mejoramiento-educativo/get_pme_dimension' => 'pme_parametros#get_pme_dimension', as: :get_pme_dimension

  get 'plan-mejoramiento-educativo/:id/subdimensiones' => 'pme_parametros#subdimensiones', as: :pme_subdimensiones
  post 'plan-mejoramiento-educativo/create_pme_subdimension' => 'pme_parametros#create_pme_subdimension', as: :create_pme_subdimension
  post 'plan-mejoramiento-educativo/:id/destroy_pme_subdimension' => 'pme_parametros#destroy_pme_subdimensiones', as: :destroy_pme_subdimensiones
  post 'plan-mejoramiento-educativo/jeditable_nombre_pme_subdimensiones' => 'pme_parametros#jeditable_nombre_pme_subdimensiones', as: :jeditable_nombre_pme_subdimensiones
  get 'plan-mejoramiento-educativo/get_pme_subdimension' => 'pme_parametros#get_pme_subdimension', as: :get_pme_subdimension


  get 'plan-mejoramiento-educativo/get_pme_objetivo' => 'pme_objetivo_estrategia#get_pme_objetivo', as: :get_pme_objetivo
  get 'plan-mejoramiento-educativo/:est/objetivos' => 'pme_objetivo_estrategia#objetivos', as: :pme_objetivos
  get 'plan-mejoramiento-educativo/:est/nuevo_objetivo' => 'pme_objetivo_estrategia#nuevo_objetivo', as: :pme_nuevo_objetivo
  post 'plan-mejoramiento-educativo/:est/crear_objetivo' => 'pme_objetivo_estrategia#crear_objetivo', as: :pme_crear_objetivo
  post 'plan-mejoramiento-educativo/jeditable_nombre_pme_objetivo' => 'pme_objetivo_estrategia#jeditable_nombre_pme_objetivo', as: :jeditable_nombre_pme_objetivo
  post 'plan-mejoramiento-educativo/:id/destroy_pme_objetivo' => 'pme_objetivo_estrategia#destroy_pme_objetivo', as: :destroy_pme_objetivo
 
  get 'plan-mejoramiento-educativo/get_pme_estrategia' => 'pme_objetivo_estrategia#get_pme_estrategia', as: :get_pme_estrategia
  get 'plan-mejoramiento-educativo/:est/objetivo/:id/estrategia' => 'pme_objetivo_estrategia#estrategia_objetivo', as: :pme_estrategias_objetivo
  post 'plan-mejoramiento-educativo/:est/editar/:id/crear_estrategia' => 'pme_objetivo_estrategia#crear_estrategia', as: :pme_crear_estrategia
  post 'plan-mejoramiento-educativo/:est/eliminar/:id/estrategia' => 'pme_objetivo_estrategia#eliminar_estrategia', as: :pme_eliminar_estrategia
  post 'plan-mejoramiento-educativo/jeditable_nombre_pme_estrategia' => 'pme_objetivo_estrategia#jeditable_nombre_pme_estrategia', as: :jeditable_nombre_pme_estrategia
  post 'plan-mejoramiento-educativo/:id/destroy_pme_estrategia' => 'pme_objetivo_estrategia#destroy_pme_estrategia', as: :destroy_pme_estrategia


  get 'plan-mejoramiento-educativo/get_pme_accion' => 'pme_accion#get_pme_accion', as: :get_pme_accion
  get 'plan-mejoramiento-educativo/sl_pme_accion' => 'pme_accion#sl_pme_accion', as: :sl_pme_accion
  get 'plan-mejoramiento-educativo/:est/acciones' => 'pme_accion#index', as: :pme_acciones
  get 'plan-mejoramiento-educativo/:est/nueva' => 'pme_accion#nueva', as: :pme_nueva_accion
  post 'plan-mejoramiento-educativo/:est/crear' => 'pme_accion#crear', as: :pme_crear_accion
  get 'plan-mejoramiento-educativo/:est/ver/:id/accion' => 'pme_accion#ver', as: :pme_ver_accion
  post 'plan-mejoramiento-educativo/:est/editar/:id/accion' => 'pme_accion#editar', as: :pme_editar_accion
  post 'plan-mejoramiento-educativo/:est/eliminar/:id/accion' => 'pme_accion#eliminar', as: :pme_eliminar_accion

  get 'plan-mejoramiento-educativo/:est/ver/:id/accion/:act/actividad' => 'pme_actividad#ver', as: :pme_ver_actividad
  get 'plan-mejoramiento-educativo/:est/ver/:id/pme_nueva_actividad' => 'pme_actividad#nueva', as: :pme_nueva_actividad
  post 'plan-mejoramiento-educativo/:id/crea_actividad' => 'pme_actividad#crear', as: :pme_crea_actividad
  post 'plan-mejoramiento-educativo/:id/actualiza_actividad' => 'pme_actividad#actualiza', as: :pme_actualiza_actividad
  post 'plan-mejoramiento-educativo/:id/subir_pme_actividad_doc' => 'pme_actividad#subir_pme_actividad_doc', as: :subir_pme_actividad_doc



  get 'opi/nueva' => 'opi#nueva', as: :nueva_opi
  post 'opi/crear' => 'opi#crear', as: :crear_opi
  get 'opi/validar' => 'opi#validar', as: :validar_opi
  get 'opi/:id/ver' => 'opi#ver', as: :ver_opi
  post 'opi/:id/actualizar' => 'opi#actualizar', as: :actualiza_opi


  get 'gestionar/:id/opi' => 'gestionar#opi', as: :gestionar_opi
  post 'gestionar/:id/validar' => 'gestionar#validar', as: :gestionar_validar_opi
  post 'gestionar/:id/sigue_conducto' => 'gestionar#sigue_conducto', as: :sigue_conducto
  post 'gestionar/:id/responde_consulta' => 'gestionar#responde_consulta', as: :responde_consulta
  post 'gestionar/:id/rechazada' => 'gestionar#rechazada', as: :gestionar_rechazada
  post 'gestionar/:id/adquisicion' => 'gestionar#adquisicion', as: :gestionar_adquisicion
  post 'gestionar/:id/termina_decreto' => 'gestionar#termina_decreto', as: :termina_decreto

  get 'xml/' => 'xml#index', as: :xml

  get 'bandejas/' => 'bandeja#index', as: :bandejas
  get 'bandejas/mi-bandeja' => 'bandeja#mi_bandeja', as: :mi_bandeja
  get 'bandejas/asesor_subvencion' => 'bandeja#asesor_subvencion', as: :entrantes_asesor_subvencion
  get 'bandejas/encargado_subvencion' => 'bandeja#encargado_subvencion', as: :entrantes_encargado_subvencion
  get 'bandejas/:id/entrantes' => 'bandeja#entrantes', as: :bandeja_entrantes
  post 'bandejas/guardar' => 'bandeja#guarda', as: :guarda_bandeja
  get 'bandejas/:id/ver' => 'bandeja#ver', as: :ver_bandeja
  post 'bandejas/:id/guarda_usuario' => 'bandeja#guarda_usuario', as: :guarda_usuario_bandeja
  post 'bandejas/:id/elimina_usuario' => 'bandeja#elimina_usuario', as: :elimina_usuario_bandeja

  get 'conducto/' => 'conducto#index', as: :conducto
  post 'conducto/guardar' => 'conducto#guardar', as: :guarda_conducto


  get 'encargados-subvencion/' => 'encargado#encargados', as: :encargados_subvencion
  get 'asesores-subvencion/' => 'encargado#asesores', as: :asesores_subvencion
  post 'encargados-subvencion/guarda_encargado' => 'encargado#guarda_encargado', as: :guarda_encargado
  post 'asesores-subvencion/guarda_asesor' => 'encargado#guarda_asesor', as: :guarda_asesor
  post 'encargados-subvencion/:id/borra_asesor' => 'encargado#borra_asesor', as: :borra_asesor
  post 'asesores-subvencion/:id/borra_encargado' => 'encargado#borra_encargado', as: :borra_encargado




  get 'cuentas_gastos/' => 'cuenta_gastos#index', as: :cuentas_gastos
  post 'cuentas_gastos/guardar' => 'cuenta_gastos#guardar', as: :guarda_cuenta_gasto
  post 'cuentas_gastos/eliminar' => 'cuenta_gastos#eliminar', as: :eliminar_cuenta_gasto

  get 'usuarios/' => 'usuarios#index', as: :usuarios
  get 'usuarios/nuevo' => 'usuarios#nuevo', as: :nuevo_usuario
  get 'usuarios/bajas' => 'usuarios#bajas', as: :bajas_usuario
  post 'usuarios/guardar' => 'usuarios#guardar', as: :guardar_usuario
  get 'usuarios/:id/editar' => 'usuarios#editar', as: :editar_usuario
  get 'usuarios/:id/perfil' => 'usuarios#perfil', as: :perfil
  post 'usuarios/:id/actualizar' => 'usuarios#actualizar', as: :actualizar_usuario
  post 'usuarios/:id/actualizar_perfil' => 'usuarios#actualizar_perfil', as: :actualizar_perfil
  post 'usuarios/:id/actualiza_password' => 'usuarios#actualiza_password', as: :actualiza_password
  post 'usuarios/:id/baja' => 'usuarios#baja', as: :baja
  get 'usuarios/get_usuario' => 'usuarios#get_usuario', as: :get_usuario


    get 'establecimiento/' => 'establecimientos#index', as: :establecimientos
    get 'establecimiento/nuevo' => 'establecimientos#new', as: :nuevo_establecimiento
    post 'establecimiento/guardar' => 'establecimientos#create', as: :guardar_establecimiento
    get 'establecimiento/get_dpto' => 'establecimientos#get_dpto', as: :get_dpto
    get 'establecimiento/get_dpto_usuario' => 'establecimientos#get_dpto_usuario', as: :get_dpto_usuario
    get 'establecimiento/:id/ver' => 'establecimientos#show', as: :ver_establecimiento
    post 'establecimiento/:id/actualizar' => 'establecimientos#update', as: :actualizar_establecimiento
    post 'establecimiento/:id/eliminar' => 'establecimientos#eliminar_establecimiento', as: :eliminar_establecimiento
    


    get 'departamento/:id/ver' => 'establecimientos#departamento', as: :ver_departamento
    post 'departamento/:id/actualizar' => 'establecimientos#actualizar_departamento', as: :actualizar_departamento
    post 'departamento/:id/guardar' => 'establecimientos#guardar_departamento', as: :guardar_departamento
    post 'departamento/:id/eliminar' => 'establecimientos#eliminar_departamento', as: :eliminar_departamento

   

  match(
    "home/general" => "home#general",
    :as => :general,
    :via => :get
  )



  match(
    "home/:id/dashboard" => "home#dashboard",
    :as => :dashboard,
    :via => :get
  )

  match(
    "pagar/index" => "pagar#index",
    :as => :pagar,
    :via => :get
  )
   match(
    "pagar/todas_recepciones" => "pagar#todas_recepciones",
    :as => :todas_recepciones,
    :via => :get
  ) 
   match(
    "pagar/:id/ver" => "pagar#ver",
    :as => :ver_pagar,
    :via => :get
  )  
  match(
    "pagar/:id/quitar_lista_pagar" => "pagar#quitar_lista_pagar",
    :as => :quitar_lista_pagar,
    :via => :post
  )
   match(
    "pagar/:id/guarda_cheque" => "pagar#guarda_cheque",
    :as => :guarda_cheque,
    :via => :post
  ) 
  match(
    "informe/gastos-categoria" => "informe#gastos_categoria",
    :as => :gastos_categoria,
    :via => :get
  )
  match(
    "informe/informe_gastos_categoria" => "informe#informe_gastos_categoria",
    :as => :informe_gastos_categoria,
    :via => :get  )

  match(
    "informe/gastos-subvencion" => "informe#gastos_subvencion",
    :as => :gastos_subvencion,
    :via => :get
  )
   match(
    "informe/informe_gastos_subvencion" => "informe#informe_gastos_subvencion",
    :as => :informe_gastos_subvencion,
    :via => :get  )
  match(
    "informe/:id/gastos-subvencion" => "informe#ver_gastos_subvencion",
    :as => :ver_gastos_subvencion,
    :via => :get,
    :format => "pdf"
  )

  match(
    "informe/ingresos-subvencion" => "informe#ingresos_subvencion",
    :as => :ingresos_subvencion,
    :via => :get
  )
  match(
    "informe/:id/ingresos-subvencion" => "informe#ver_ingresos_subvencion",
    :as => :ver_ingresos_subvencion,
    :via => :get,
    :format => "pdf"
  )

  match(
    "informe/compras" => "informe#compras",
    :as => :informe_compras,
    :via => :get
  )
  match(
    "informe/informe_compras" => "informe#informe_compras",
    :as => :genera_informe_compras,
    :via => :get  )
  match(
    "informe/:id/ver-compras" => "informe#ver_compras",
    :as => :ver_informe_compras,
    :via => :get,
    :format => "pdf"
  )

  match(
    "informe/supereduc" => "informe#supereduc",
    :as => :supereduc,
    :via => :get
  )
  match(
    "informe/informe_supereduc" => "informe#informe_supereduc",
    :as => :informe_supereduc,
    :via => :get  )
  match(
    "informe/:id/generar-supereduc" => "informe#generar_supereduc",
    :as => :generar_supereduc,
    :via => :get
  )
    #Recepcion
  match(
    "recepcionar" => "recepcionar#index",
    :as => :recepcionar,
    :via => :get
  )

  match(
    "recepcionar/crear" => "recepcionar#crear",
    :as => :crear_recepcion,
    :via => :post
  )
  match(
    "recepcionar/seleccionar" => "recepcionar#seleccionar",
    :as => :seleccionar_recepcion,
    :via => :post
  )
  match(
    "recepcionar/listado" => "recepcionar#listado",
    :as => :listado_recepcionadas,
    :via => :get
  )
  match(
    "recepcionar/para_firma" => "recepcionar#para_firma",
    :as => :para_firmar_recepcion,
    :via => :get
  )
  match(
    "recepcionar/visar_adq" => "recepcionar#visar_adq",
    :as => :visar_adq,
    :via => :get
  )
  match(
    "recepcionar/:id/firmar" => "recepcionar#firmar",
    :as => :firmar_recepcion,
    :via => :get
  )
  match(
    "recepcionar/:id/insertar_firma" => "recepcionar#insertar_firma",
    :as => :insertar_firma_recepcion,
    :via => :post
  )
    match(
    "recepcionar/:id/adq" => "recepcionar#adq",
    :as => :adq_recepcion,
    :via => :get
  )
      match(
    "recepcionar/:id/act_adq" => "recepcionar#act_adq",
    :as => :act_adq_recepcion,
    :via => :post
  )  
  match(
    "recepcionar/:id/ver" => "recepcionar#ver",
    :as => :ver_recepcion,
    :via => :get,
    :format => "pdf"
  )
    match(
    "recepcionar/:id/orden" => "recepcionar#nueva",
    :as => :nueva_recepcion,
    :via => :get
  )
  match(
    "recepcionar/:id/quitar" => "recepcionar#quitar",
    :as => :quitar_lista_recepcion,
    :via => :post
  )
  match(
    "recepcionar/:id/eliminar" => "recepcionar#eliminar",
    :as => :eliminar_recepcion,
    :via => :post
  )
  ######Proveedor

  match(
    "proveedor/get" => "proveedor#get",
    :as => :get_proveedor,
    :via => :get,
  )

  match(
    "proveedor/crear" => "proveedor#crear",
    :as => :guardar_proveedor,
    :via => :post,
  )

  #####################
  match(
    "presupuesto/:id/establecimiento" => "presupuesto#establecimiento",
    :as => :presupuesto_establecimiento,
    :via => :get
  )
  match(
    "presupuesto/:id/establecimiento/:sub/movimientos" => "presupuesto#movimientos",
    :as => :movimientos_presupuesto,
    :via => :get
  )

  #HOMEEEE
  match(
    "home" => "home#index",
    :as => :home,
    :via => :get
  )
  match(
    "home/perfil" => "home#perfil",
    :as => :perfil,
    :via => :get
  )
    match(
    "home/actualiza_perfil" => "home#actualiza_perfil",
    :as => :actualiza_perfil,
    :via => :post
  )
  match(
    "home/pass" => "home#pass",
    :as => :pass,
    :via => :get
  )
    match(
    "home/actualiza_pass" => "home#actualiza_pass",
    :as => :actualiza_pass,
    :via => :post
  )


#SUBVENCIONES ORDEN
  match(
      "subvenciones-orden/index" => "finanzas#subvenciones_orden",
      :as => :subvenciones_orden,
      :via => :get
    )
  match(
      "subvencion/guarda-subvencion" => "subvencions#guarda_subvencion",
      :as => :guarda_subvencion,
      :via => :post
    )
   match(
  "subvencions/get_subvencion" => "subvencions#get_subvencion",
  :as => :get_subvencion,
  :via => :get 
  )
#TIPO GASTO
  match(
      "subvenciones-orden/:id/tipo-gasto" => "finanzas#tipo_gasto_subvencion",
      :as => :tipo_gasto_subvencion,
      :via => :get
    )

  match(
      "tipo_gastos/guarda_tipo_gasto" => "tipo_gastos#guarda_tipo_gasto",
      :as => :guarda_tipo_gasto,
      :via => :post
    )
    match(
    "tipo_gastos/sl_tipo_gasto" => "tipo_gastos#sl_tipo_gasto",
    :as => :sl_tipo_gasto,
    :via => :get
  )

    match(
  "tipo_gastos/get_tipo_gasto" => "tipo_gastos#get_tipo_gasto",
  :as => :get_tipo_gasto,
  :via => :get 
  )
    match(
  "tipo_gastos/jeditable_nombre_tipo_gasto" => "tipo_gastos#jeditable_nombre_tipo_gasto",
  :as => :jeditable_nombre_tipo_gasto,
  :via => :post 
  )
    match(
  "tipo_gastos/:id/borrar_tipo_gasto" => "tipo_gastos#borrar_tipo_gasto",
  :as => :borrar_tipo_gasto,
  :via => :post 
  )

#CATEGORIA GASTO
  match(
      "subvenciones-orden/:id/categoria-gasto-subvencion" => "finanzas#categoria_gasto_subvencion",
      :as => :categoria_gasto_subvencion,
      :via => :get
    )
  match(
      "categoria_gastos/guardar" => "categoria_gastos#guarda_categoria_gasto",
      :as => :guarda_categoria_gasto,
      :via => :post
    )
   match(
    "categoria_gastos/sl_categoria_gasto" => "categoria_gastos#sl_categoria_gasto",
    :as => :sl_categoria_gasto,
    :via => :get
  )
    match(
  "categoria_gastos/jeditable_nombre_cat_gasto" => "categoria_gastos#jeditable_nombre_cat_gasto",
  :as => :jeditable_nombre_cat_gasto,
  :via => :post 
  )
      match(
  "categoria_gastos/jeditable_codigo_cat_gasto" => "categoria_gastos#jeditable_codigo_cat_gasto",
  :as => :jeditable_codigo_cat_gasto,
  :via => :post 
  )
    match(
  "categoria_gastos/:id/borrar_cat_gasto" => "categoria_gastos#borrar_cat_gasto",
  :as => :borrar_cat_gasto,
  :via => :post 
  )
   #SUBCATEGORIA GASTO
     match(
      "subvenciones-orden/:id/subcategoria-gasto-subvencion" => "finanzas#subcategoria_gasto_subvencion",
      :as => :subcategoria_gasto_subvencion,
      :via => :get
    )
       match(
      "subcategoria_gastos/guardar" => "subcategoria_gastos#guarda_subcategoria_gasto",
      :as => :guarda_subcategoria_gasto,
      :via => :post
    )
      match(
    "subcategoria_gastos/sl_subcategoria_gasto" => "subcategoria_gastos#sl_subcategoria_gasto",
    :as => :sl_subcategoria_gasto,
    :via => :get
  )
    match(
  "subcategoria_gastos/jeditable_nombre_subcat_gasto" => "subcategoria_gastos#jeditable_nombre_subcat_gasto",
  :as => :jeditable_nombre_subcat_gasto,
  :via => :post 
  )

        match(
  "subcategoria_gastos/jeditable_codigo_subcat_gasto" => "subcategoria_gastos#jeditable_codigo_subcat_gasto",
  :as => :jeditable_codigo_subcat_gasto,
  :via => :post 
  )
    match(
  "subcategoria_gastos/:id/borrar_subcat_gasto" => "subcategoria_gastos#borrar_subcat_gasto",
  :as => :borrar_subcat_gasto,
  :via => :post 
  )      

#PERIODO
  match(
    "periodo/index" => "periodo#index",
    :as => :periodo,
    :via => :get
  )

  match(
    "periodo/create" => "periodo#create",
    :as => :crear_periodo,
    :via => :post
  )

    match(
    "periodo/cambiar" => "periodo#cambiar",
    :as => :cambiar_periodo,
    :via => :post
  )
    match(
    "periodo/activar" => "periodo#activar",
    :as => :activar_periodo,
    :via => :post
  )
    match(
    "periodo/eliminar" => "periodo#eliminar",
    :as => :eliminar_periodo,
    :via => :post
  )
#############PME
  match(
    "pme/index" => "pme#index",
    :as => :pme,
    :via => :get
  )

  match(
    "pme/nueva-accion" => "pme#new",
    :as => :nuevo_pme,
    :via => :get
  )
  match(
    "pme/acciones" => "pme#acciones",
    :as => :acciones_pme,
    :via => :get
  )
  match(
    "pme/:id/ver-acciones" => "pme#ver",
    :as => :ver_acciones_plan,
    :via => :get
  )

  match(
    "pme/:id/ver-accion" => "pme#ver_accion",
    :as => :ver_accion_plan,
    :via => :get
  )
    match(
    "pme/:id/actualiza_accion_plan" => "pme#actualiza_accion_plan",
    :as => :actualiza_accion_plan,
    :via => :post
  )

##################LICITACIONES
match(
    "licitacion/:id/gestionar" => "licitacion#gestionar",
    :as => :gestionar_licitacion,
    :via => :get
  )
match(
    "licitacion/:id/enviar" => "licitacion#enviar",
    :as => :enviar_licitacion,
    :via => :post
  )
##################ORDENES
  match(
    "ordenes/licitacion" => "ordens#licitacion",
    :as => :ordenes_licitacion,
    :via => :get
  )
  match(
    "ordenes/compradas" => "ordens#compradas",
    :as => :ordenes_compradas,
    :via => :get
  )
  match(
    "ordenes/index" => "ordens#index",
    :as => :ordenes_entrantes,
    :via => :get
  )
 # match(
  #  "ordenes/nueva-orden" => "ordens#new",
  #  :as => :nueva_orden,
  #  :via => :get
  #)
  #match(
  #  "ordenes/create" => "ordens#create",
  #  :as => :ordens,
  #  :via => :post
  #)
    match(
    "ordenes/gestionadas" => "ordens#gestionadas",
    :as => :ordenes_gestionadas,
    :via => :get
  )
  match(
    "ordenes/enviadas" => "ordens#enviadas",
    :as => :ordenes_enviadas,
    :via => :get
  )
  match(
    "ordenes/:id/ver" => "ordens#show",
    :as => :ver_orden,
    :via => :get
  )
  match(
  "ordenes/:id/actualizar" => "ordens#actualizar",
    :as => :actualiza_orden,
    :via => :post
  )
  match(
  "ordenes/:id/enviar" => "ordens#enviar",
    :as => :enviar_orden,
    :via => :post
  )
  match(
  "ordenes/:id/gestionar" => "ordens#gestionar",
    :as => :gestionar_orden,
    :via => :get
  )
  match(
  "ordenes/:id/eliminar" => "ordens#eliminar",
    :as => :eliminar_orden,
    :via => :post
  )
  match(
  "ordenes/:id/imprimible" => "ordens#imprimible",
    :as => :imprimible_orden,
    :via => :get,
    :format => "pdf"
  )
  ###########################PARAMETROS GENERALES

  match(
  "parametros/conducto-regular" => "parametros#conducto_regular",
    :as => :conducto_regular,
    :via => :get
  )

  match(
  "parametros/ingresa-conducto" => "parametros#ingresa_conducto",
    :as => :ingresa_conducto,
    :via => :post
  )

  ########################LOGIN
  resources :login, :only => [:create, :new]  do
    get 'logout', :on => :collection
    post 'registrar', :on => :collection
    get 'departamentos', :on => :collection
  end

  ########################FINANZAS
  match(
    "finanzas/presupuesto-subvencion" => "finanzas#presupuesto_subvencion",
    :as => :presupuesto_subvencion,
    :via => :get
  )
  match(
    "finanzas/actualiza-presupuesto-subvencion" => "finanzas#actualiza_presupuesto_subvencion",
    :as => :actualiza_presupuesto_subvencion,
    :via => :post
  )
  match(
    "finanzas/liquidacion-subvenciones" => "finanzas#liquidacion_subvenciones",
    :as => :liquidacion,
    :via => :get
  )
  match(
    "finanzas/listado-liquidacion-subvenciones" => "finanzas#listado_liquidacion",
    :as => :listado_liquidacion,
    :via => :get
  )
    match(
    "finanzas/:id/ver-liquidacion-subvenciones" => "finanzas#ver_liquidacion",
    :as => :ver_liquidacion,
    :via => :get
  )

    match(
    "finanzas/get_liquidacion_est" => "finanzas#get_liquidacion_est",
    :as => :get_liquidacion_est,
    :via => :get
  )
  match(
    "finanzas/get_porcentaje_base" => "finanzas#get_porcentaje_base",
    :as => :get_porcentaje_base,
    :via => :get
  ) 
   match(
    "finanzas/crear-liquidacion" => "finanzas#crear_liquidacion",
    :as => :crear_liquidacion,
    :via => :post
  )
    match(
    "finanzas/:id/actualiza-liquidacion" => "finanzas#actualiza_liquidacion",
    :as => :actualiza_liquidacion,
    :via => :post
  )

        match(
    "finanzas/:id/elimina-liquidacion" => "finanzas#elimina_liquidacion",
    :as => :elimina_liquidacion,
    :via => :post
  )
    match(
    "finanzas/subvenciones-base" => "finanzas#subvenciones_base",
    :as => :subvenciones_base,
    :via => :get
  )

        match(
    "finanzas/eliminar_subvencion_base" => "finanzas#eliminar_subvencion_base",
    :as => :eliminar_subvencion_base,
    :via => :post
  )

        match(
    "finanzas/eliminar_subvencion_orden" => "finanzas#eliminar_subvencion_orden",
    :as => :eliminar_subvencion_orden,
    :via => :post
  )

        match(
    "finanzas/get_subvencion_base" => "finanzas#get_subvencion_base",
    :as => :get_subvencion_base,
    :via => :get
  )
        match(
    "finanzas/guarda-subvenciones-base" => "finanzas#guarda_subvencion_base",
    :as => :guarda_subvencion_base,
    :via => :post
  )
          match(
    "finanzas/carga_presupuesto" => "finanzas#carga_presupuesto",
    :as => :carga_presupuesto,
    :via => :get
  ) 

    match(
    "finanzas/guarda_presupuesto" => "finanzas#guarda_presupuesto",
    :as => :guarda_presupuesto,
    :via => :post
  ) 
      match(
    "finanzas/:id/anula_carga" => "finanzas#anula_carga",
    :as => :anula_carga,
    :via => :post
  )         
  ########################################ADQUISICIONES
  match(
    "adquisiciones/index" => "adquisiciones#index",
    :as => :adquisiciones,
    :via => :get
  )
  match(
    "adquisiciones/decreto" => "adquisiciones#decreto",
    :as => :espera_decreto,
    :via => :get
  )

  match(
    "adquisiciones/orden_compras" => "adquisiciones#orden_compras",
    :as => :orden_compras,
    :via => :get
  )
  match(
    "adquisiciones/elimina_orden" => "adquisiciones#elimina_orden",
    :as => :elimina_orden,
    :via => :post
  )
  match(
    "adquisiciones/:id/guarda_oc" => "adquisiciones#guarda_oc",
    :as => :guarda_oc,
    :via => :post
  )



  match(
    "documentos/crear" => "documentos#crear_documento",
    :as => :crear_documento,
    :via => :post
  )
  match(
    "documentos/adjuntar" => "documentos#adjuntar_documento",
    :as => :adjuntar_documento,
    :via => :post
  )
  match(
    "documentos/:id/borrar" => "documentos#borrar_documento",
    :as => :borrar_documento,
    :via => :post
  )




 









  

  

  


  


  #rutas accion

 
  match(
    "accion/create" => "accions#create", 
    :as => :crear_accion, 
    :via => :post
    )
            match(
    "accion/sl_accion" => "accions#sl_accion",
    :as => :sl_accion,
    :via => :get
  )
  match(
      "accion/presupuesto" => "accions#presupuesto",
    :as => :presupuesto_accion,
    :via => :get
  )
     match(
    "accion/:id/borrar" => "accions#borrar_accion",
    :as => :borrar_accion_plan,
    :via => :post
  )

    match(
    "dimencion/sl_dimencion" => "dimencions#sl_dimencion",
    :as => :sl_dimencion,
    :via => :get
  )



#OBJETIVO#########################################
        match(
    "objetivo/sl_objetivo" => "objetivo#sl_objetivo",
    :as => :sl_objetivo,
    :via => :get
  )

  match(
    "objetivo/nuevo" => "objetivo#new",
    :as => :nuevo_objetivo,
    :via => :get
  )
  match(
    "objetivo/create" => "objetivo#create",
    :as => :crear_objetivo,
    :via => :post
  )
  match(
    "objetivo/jeditable_nombre" => "objetivo#jeditable_nombre",
    :as => :jeditable_nombre_objetivo,
    :via => :post
  )
  match(
    "objetivo/:id/borrar" => "objetivo#borrar",
    :as => :borrar_objetivo,
    :via => :post
  )




# rutas area
  match(
    "areas" => "areas#index", 
    :as => :areas, 
    :via => :get
    )

  match(
    "areas/new" => "areas#new", 
    :as => :nueva_area, 
    :via => :get
    )
  match(
    "areas/create" => "areas#create", 
    :as => :crear_area, 
    :via => :post
    )

  match(
    "areas/:id/edit" => "areas#edit", 
    :as => :editar_area, 
    :via => :get
    )
  
   match(
    "areas/:id/update" => "areas#update", 
    :as => :update_area, 
    :via => :post
    )

   match(
    "areas/:id/borrar" => "areas#borrar", 
    :as => :borrar_area, 
    :via => :get
    )
  
  match(
    "areas/:id/destroy" => "areas#destroy", 
    :as => :destroy_area, 
    :via => :post
    )

  match(
    "areas/:id/show" => "areas#show", 
    :as => :ver_area_dim, 
    :via => :get
    )
  

  #rutas dimensiones
 match(
    "dimencions/:id" => "dimencions#index", 
    :as => :dimencions, 
    :via => :get
    )

  match(
    "dimencions/:id/new" => "dimencions#new", 
    :as => :nueva_dimencion, 
    :via => :get
    )
  match(
    "dimencions/:id/create" => "dimencions#create", 
    :as => :crear_dimencion, 
    :via => :post
    )

  match(
    "dimencions/:id/edit" => "dimencions#edit", 
    :as => :editar_dimencion, 
    :via => :get
    )
  
   match(
    "dimencions/:id/update" => "dimencions#update", 
    :as => :update_dimencion, 
    :via => :post
    )

   match(
    "dimencions/:id/borrar" => "dimencions#borrar", 
    :as => :borrar_dimencion, 
    :via => :get
    )
  
  match(
    "dimencions/:id/destroy" => "dimencions#destroy", 
    :as => :destroy_dimencion, 
    :via => :post
    )

    match(
    "dimencions/:id/show" => "dimencions#show", 
    :as => :ver_dim_obj, 
    :via => :get
    )
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
