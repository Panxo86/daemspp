require 'test_helper'

class TipoGastosControllerTest < ActionController::TestCase
  setup do
    @tipo_gasto = tipo_gastos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tipo_gastos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create tipo_gasto" do
    assert_difference('TipoGasto.count') do
      post :create, tipo_gasto: { nombre: @tipo_gasto.nombre }
    end

    assert_redirected_to tipo_gasto_path(assigns(:tipo_gasto))
  end

  test "should show tipo_gasto" do
    get :show, id: @tipo_gasto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @tipo_gasto
    assert_response :success
  end

  test "should update tipo_gasto" do
    put :update, id: @tipo_gasto, tipo_gasto: { nombre: @tipo_gasto.nombre }
    assert_redirected_to tipo_gasto_path(assigns(:tipo_gasto))
  end

  test "should destroy tipo_gasto" do
    assert_difference('TipoGasto.count', -1) do
      delete :destroy, id: @tipo_gasto
    end

    assert_redirected_to tipo_gastos_path
  end
end
