require 'test_helper'

class SubvencionsControllerTest < ActionController::TestCase
  setup do
    @subvencion = subvencions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:subvencions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create subvencion" do
    assert_difference('Subvencion.count') do
      post :create, subvencion: { nombre: @subvencion.nombre }
    end

    assert_redirected_to subvencion_path(assigns(:subvencion))
  end

  test "should show subvencion" do
    get :show, id: @subvencion
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @subvencion
    assert_response :success
  end

  test "should update subvencion" do
    put :update, id: @subvencion, subvencion: { nombre: @subvencion.nombre }
    assert_redirected_to subvencion_path(assigns(:subvencion))
  end

  test "should destroy subvencion" do
    assert_difference('Subvencion.count', -1) do
      delete :destroy, id: @subvencion
    end

    assert_redirected_to subvencions_path
  end
end
