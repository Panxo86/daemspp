require 'test_helper'

class SubcategoriaGastosControllerTest < ActionController::TestCase
  setup do
    @subcategoria_gasto = subcategoria_gastos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:subcategoria_gastos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create subcategoria_gasto" do
    assert_difference('SubcategoriaGasto.count') do
      post :create, subcategoria_gasto: { nombre: @subcategoria_gasto.nombre }
    end

    assert_redirected_to subcategoria_gasto_path(assigns(:subcategoria_gasto))
  end

  test "should show subcategoria_gasto" do
    get :show, id: @subcategoria_gasto
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @subcategoria_gasto
    assert_response :success
  end

  test "should update subcategoria_gasto" do
    put :update, id: @subcategoria_gasto, subcategoria_gasto: { nombre: @subcategoria_gasto.nombre }
    assert_redirected_to subcategoria_gasto_path(assigns(:subcategoria_gasto))
  end

  test "should destroy subcategoria_gasto" do
    assert_difference('SubcategoriaGasto.count', -1) do
      delete :destroy, id: @subcategoria_gasto
    end

    assert_redirected_to subcategoria_gastos_path
  end
end
