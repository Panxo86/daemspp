require 'test_helper'

class PmeAccionControllerTest < ActionController::TestCase
  test "should get index" do
    get :index
    assert_response :success
  end

  test "should get nueva" do
    get :nueva
    assert_response :success
  end

  test "should get ver" do
    get :ver
    assert_response :success
  end

end
