require 'test_helper'

class DimencionsControllerTest < ActionController::TestCase
  setup do
    @dimencion = dimencions(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:dimencions)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create dimencion" do
    assert_difference('Dimencion.count') do
      post :create, dimencion: { nombre: @dimencion.nombre }
    end

    assert_redirected_to dimencion_path(assigns(:dimencion))
  end

  test "should show dimencion" do
    get :show, id: @dimencion
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @dimencion
    assert_response :success
  end

  test "should update dimencion" do
    put :update, id: @dimencion, dimencion: { nombre: @dimencion.nombre }
    assert_redirected_to dimencion_path(assigns(:dimencion))
  end

  test "should destroy dimencion" do
    assert_difference('Dimencion.count', -1) do
      delete :destroy, id: @dimencion
    end

    assert_redirected_to dimencions_path
  end
end
