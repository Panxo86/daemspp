#encoding: utf-8
# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


puts "Creando establecimientos..."
hex = SecureRandom.hex(3)
daem = Establecimiento.create(nombre: 'DAEM', rbd: "1",hex_url: hex, direccion: "Los Escritores 595, Villa Acero, Hualpén")

puts "Creando Departamentos..."
info = Departamento.create(nombre: 'Informatica', establecimiento_id: daem.id)
finanzas = Departamento.create(nombre: 'Finanzas', establecimiento_id: daem.id)


puts "Creando usuarios..."
daniel = Usuario.create(
		rut: '11.111.111-1',
		nombre: 'Daniel',
		apaterno: 'Fuentealba',
		amaterno: 'Arias',
		email: "dfuentealba@ingenieriasd.cl",
		password: '123.123',
		departamento_id: info.id
		)
Sistema.create(
	admin: "1", 
	orden: "1",
	finanzas: "1",
	adquisiciones: "1",
	tesoreria: "1"
	admin: "1"
	pme: "1"
	dotacion_admin: "1"
	dotacion: "1"
	validar_orden: "1"
	usuario_id: daniel.id)

Bandeja.create(
	nombre: "Encargado Subvención",
	es_encargado: true
	)
Bandeja.create(
	nombre: "Asesor Subvención",
	es_asesor: true
	)
Bandeja.create(nombre: "Personal")
Bandeja.create(nombre: "Dirección DAEM")
Bandeja.create(nombre: "Tesorería")
Bandeja.create(nombre: "Adquisiciones")

periodo = Periodo.create(anio: "2019", es_vigente: true)
EstadoSistema.create(activo: true, desde: Time.now)



Departamento.create(nombre: 'DIRECCIÓN DAEM', establecimiento_id: daem.id, es_direccion: true)


TipoDocumento.create(nombre: "Acta Evaluación", es_vigente: true)
TipoDocumento.create(nombre: "Certificado Antecedentes", es_vigente: true)
TipoDocumento.create(nombre: "Certificado Estudios", es_vigente: true)
TipoDocumento.create(nombre: "Certificado Proveedor Unico", es_vigente: true)
TipoDocumento.create(nombre: "Certificado Título", es_vigente: true)
TipoDocumento.create(nombre: "Cotización", es_vigente: true)
TipoDocumento.create(nombre: "Curriculum Vitae", es_vigente: true)
TipoDocumento.create(nombre: "Decreto", es_vigente: true)
TipoDocumento.create(nombre: "Especificación Técnica", es_vigente: true)
TipoDocumento.create(nombre: "Guia de Estudio", es_vigente: true)
TipoDocumento.create(nombre: "Imagen Referencial", es_vigente: true)
TipoDocumento.create(nombre: "Perfeccionamiento", es_vigente: true)
TipoDocumento.create(nombre: "Programa", es_vigente: true)
TipoDocumento.create(nombre: "Terminos de Referencia", es_vigente: true)

sube = Subvencion.create(nombre: "NORMAL" )	
presupuesto = PresupuestoSubvencion.create(
              :monto => "0",
              :establecimiento_id => daem.id,
              :subvencion_id => sube.id,
              :periodo_id => periodo.id
            )
tip = TipoGasto.create(nombre: "BIENES Y SERVICIOS", subvencion_id: sube.id)
cat = CategoriaGasto.create(nombre: "GASTOS EN RECURSOS DE APRENDIZAJE", tipo_gasto_id: tip.id)
scat = SubcategoriaGasto.create(nombre: "OTROS GASTOS EN RECURSOS DE APRENDIZAJE", categoria_gasto_id: cat.id)

Orden.create(
  cod: "1",
  titulo: "1",
  justificacion: "1",
  es_entrante: true,
  estado: "1",
  es_editable: false,
  es_nula: true,
  lista_recepcion: false,
  enviada_personal: false,
  es_cometido: false,
  en_consulta: false,
  en_proceso: false,
  en_emisor: false,
  en_direccion: false,
  es_suministro: false,
  periodo_id: 1,
  usuario_id: 1,
  departamento_id: 1,
  subcategoria_gasto_id: scat.id
)

CuentaGasto.create(id: 1, numero: "1", descripcion: "1", es_nula: true)